package script.textosTunki;

import com.everis.Action;
import com.everis.EFA;

import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;
import scripts.helpers.Reutilizable;

public class ValidacionTexto {
	
	
	public static String textCollect() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_TOOLBAR_TITLE.getValue())).toString().toUpperCase().equals(Textos.COBRAR_A.toUpperCase())){
			msjFinal = "Texto: "+Textos.COBRAR_A+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_MAX_AMOUNT.getValue())).toString().substring(0,22).toUpperCase().equals(Textos.MONTO_MAXIMO_DE_COBRO.toUpperCase())){
			msjFinal = "Texto: "+Textos.MONTO_MAXIMO_DE_COBRO+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TIL_DETAIL.getValue())).toString().toUpperCase().equals(Textos.ASUNTO.toUpperCase())){
			msjFinal = "Texto: "+Textos.ASUNTO+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId())).toString().toUpperCase().equals(Textos.COBRAR.toUpperCase())){
			msjFinal = "Texto: "+Textos.COBRAR+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.REJECT.getId())).toString().toUpperCase().equals(Textos.CANCELAR.toUpperCase())){
			msjFinal = "Texto: "+Textos.CANCELAR+" diferente";
		}
		return msjFinal;
	}
	
	public static String textPay() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_TOOLBAR_TITLE.getValue())).toString().toUpperCase().equals(Textos.PAGAR_A.toUpperCase())){
			msjFinal = "Texto: "+Textos.PAGAR_A+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_MAX_AMOUNT.getValue())).toString().substring(0,21).toUpperCase().equals(Textos.MONTO_MAXIMO_DE_PAGO.toUpperCase())){
			msjFinal = "Texto: "+Textos.MONTO_MAXIMO_DE_PAGO+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TIL_DETAIL.getValue())).toString().toUpperCase().equals(Textos.ASUNTO.toUpperCase())){
			msjFinal = "Texto: "+Textos.ASUNTO+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId())).toString().toUpperCase().equals(Textos.PAGAR.toUpperCase())){
			msjFinal = "Texto: "+Textos.PAGAR+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.REJECT.getId())).toString().toUpperCase().equals(Textos.REGRESAR.toUpperCase())){
			msjFinal = "Texto: "+Textos.REGRESAR+" diferente";
		}
		return msjFinal;
	}
	
	public static String textPaymentMethod() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TIL_CARD_NUMBER.getValue())).toString().trim().toUpperCase().equals(Textos.NUMERO_DE_TARJETA.toUpperCase())){
			msjFinal = "Texto: "+Textos.NUMERO_DE_TARJETA+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TIL_EXPIRATION_DATE.getValue())).toString().trim().toUpperCase().equals(Textos.FECHA_DE_VENCIMIENTO.toUpperCase())){
			msjFinal = "Texto: "+Textos.FECHA_DE_VENCIMIENTO+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TIL_CVV.getValue())).toString().trim().toUpperCase().equals(Textos.CVV.toUpperCase())){
			msjFinal = "Texto: "+Textos.CVV+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_CVV_INFO.getValue())).toString().trim().toUpperCase().equals(Textos.QUE_ES_UN_CVV.toUpperCase())){
			msjFinal = "Texto: "+Textos.QUE_ES_UN_CVV+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId())).toString().trim().toUpperCase().equals(Textos.VINCULAR.toUpperCase())){
			msjFinal = "Texto: "+Textos.VINCULAR+" diferente";
		}
		return msjFinal;
	}
	
	public static String paymentMehodMore() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TARJETA_AGREGADA.toUpperCase())){
			msjFinal = "Texto: "+Textos.TARJETA_AGREGADA+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DESCRIPTION.getValue())).toString().trim().toUpperCase().equals(Textos.MENSAJE_TARJETA_AGREGADA.toUpperCase())){
			msjFinal = "Texto: "+Textos.MENSAJE_TARJETA_AGREGADA+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId())).toString().trim().toUpperCase().equals(Textos.ENTENDIDO.toUpperCase())){
			msjFinal = "Texto: "+Textos.ENTENDIDO+" diferente";
		}
	    return msjFinal;
	}
	
	public static String payConstancyP2C() throws Exception{
		String msjFinal="";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_GIVER_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_GIVER_TITLE.toUpperCase())){
			msjFinal = "Texto: "+Textos.TV_GIVER_TITLE+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_RECEIVER_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_RECEIVER_TITLE.toUpperCase())){
			msjFinal = "Texto: "+Textos.TV_RECEIVER_TITLE+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_PAYMENT_METHOD_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_PAYMENT_METHOD_TITLE.toUpperCase()) && !EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_PAYMENT_METHOD_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_PAYMENT_METHOD_TITLE_CARD.toUpperCase())){
			msjFinal = "Texto: "+Textos.TV_PAYMENT_METHOD_TITLE+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_OPERATION_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_OPERATION_TITLE.toUpperCase())){
			msjFinal = "Texto: "+Textos.TV_OPERATION_TITLE+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DATE_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_DATE_TITLE.toUpperCase())){
			msjFinal = "Texto: "+Textos.TV_DATE_TITLE+" diferente";
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_AMOUNT_TITLE.getValue())).toString())){
			if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_AMOUNT_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_AMOUNT_TITLE.toUpperCase())){
				msjFinal = "Texto: "+Textos.TV_AMOUNT_TITLE+" diferente";
			}
			if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_TIP_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_TIP_TITLE.toUpperCase())){
				msjFinal = "Texto: "+Textos.TV_TIP_TITLE+" diferente";
			}
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_TIME_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_TIME_TITLE.toUpperCase())){
			msjFinal = "Texto: "+Textos.TV_TIME_TITLE+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_TOTAL_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.TV_TOTAL_TITLE.toUpperCase())){
			msjFinal = "Texto: "+Textos.TV_TOTAL_TITLE+" diferente";
		}
		
		return msjFinal;
	}

}
