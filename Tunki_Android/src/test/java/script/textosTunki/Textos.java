package script.textosTunki;

public class Textos {
	public static final String INICIA_SESION = "Inicia sesi�n";
	public static final String CORREO_ELECTRONICO = "Correo electr�nico";
	public static final String CONTRASE�A= "Contrase�a";
	public static final String SESION_ACTIVA="Sesi�n activa";
	public static final String OLVIDASTE_CONTRASE�A= "�Olvidaste tu contrase�a?";
	public static final String INICIAR = "Iniciar";
	public static final String TAMBIEN_PUEDES_USAR="Tambi�n puedes usar:";
	public static final String AUN_NO_ESTAS_REGISTRADO="�A�n no est�s registrado?";
	public static final String COBRAR_A="Cobrar a";
	public static final String ERROR_INESPERADO="Ha ocurrido un error inesperado";//agregar punto
	public static final String PAGAR_A="Pagar a";
	public static final String MONTO_MAXIMO_DE_COBRO="Monto m�ximo de cobro:";
	public static final String MONTO_MAXIMO_DE_PAGO="Monto m�ximo de pago:";
	public static final String ASUNTO="Asunto";
	public static final String COBRAR="Cobrar";
	public static final String PAGAR="Pagar";
	public static final String CANCELAR="Cancelar";
	public static final String REGRESAR="Regresar";
	public static final String ENVIASTE_UN_COBRO="Enviaste un cobro";
	public static final String ENVIASTE_UN_PAGO="Enviaste un pago";
	public static final String RECIBISTE_UN_COBRO = "Recibiste un cobro";
	public static final String RECIBISTE_UN_PAGO = "Recibiste un pago";
	public static final String VINCULA_TARJETA= "Vincula tu tarjeta";
	public static final String TIPO_DE_TARJETA = "Tipo de tarjeta";
	public static final String NUMERO_DE_TARJETA="N�mero de tarjeta";
	public static final String FECHA_DE_VENCIMIENTO="Fecha de vencimiento";
	public static final String CVV="CVV";
	public static final String QUE_ES_UN_CVV="�Qu� es un CVV?";
	public static final String CLAVE_WEB_INTERBANK="Clave web Interbank";
	public static final String TEXTO_CLAVE_WEB_INTERBANK= "La clave que usas para tu app o banca por internet";
	public static final String VINCULAR="Vincular";
	public static final String TARJETA_AGREGADA="�Tarjeta agregada!";
	public static final String MENSAJE_TARJETA_AGREGADA="Est�s listo para pagarle a tus contactos, en tus comercios favoritos y hacer tus pagos de servicios y recargas!";
	public static final String ENTENDIDO="Entendido";
	public static final String TARJETA_YA_ESTA_VINCULADA="Esa cuenta ya est� vinculada.";
	public static final String CREDENCIALES_INVALIDAS_TARJETA="Tus credenciales no coinciden, por favor intenta nuevamente.";
	public static final String TV_GIVER_TITLE="De :";
	public static final String TV_RECEIVER_TITLE="Para :";
	public static final String TV_PAYMENT_METHOD_TITLE="Cuenta cargo :";
	public static final String TV_PAYMENT_METHOD_TITLE_CARD="Tarjeta cargo :";
	public static final String TV_OPERATION_TITLE="C�d. operaci�n :";
	public static final String TV_DATE_TITLE = "Fecha :";
	public static final String TV_TIME_TITLE ="Hora :";
	public static final String TV_AMOUNT_TITLE = "Monto:";
	public static final String TV_TIP_TITLE = "Propina:";
	public static final String TV_TOTAL_TITLE = "Total :";
	public static final String VALIDA_TU_CORREO ="Valida tu correo";
	public static final String DETALLE_VALIDA_TU_CORREO = "Ingresa el c�digo que recibiste en tu bandeja de entrada o correo no deseado:";
	public static final String CREA_TU_CONTRASE�A= "Crea tu contrase�a";
	public static final String INGRESA_UNA_CONTRASE�A_NUEVA= "Ingresa una contrase�a nueva";
	public static final String DEBE_TENER_6_DIGITOS= "Debe tener 6 d�gitos";
	public static final String COMUNICATE_CON_NOSOTROS_CAMBIO_CLAVE= "Si tienes problemas, comun�cate con nosotros a: tunki@intercorp.com.pe para ayudarte.";
	public static final String CONTRASENA_CREADA= "Contrase�a creada";
	public static final String CONTRASENA_CREADA_CORRECTAMENTE = "Tu contrase�a se guard� correctamente";
	public static final String RESTABLECE_TU_CONTRASE�A= "Restablece tu contrase�a";
	public static final String DESCRIPCIO_RESTABLECE_TU_CONTRASE�A = "Tu cuenta ha sido temporalmente bloqueada por intentos fallidos. Para acceder a Tunki debes crear una nueva contrase�a.";
	public static final String NO_TIENES_TICKECTS= "Por el momento no tienes tickets.";
	public static final String INGRESA_EL_CODIGO_TICKET= "Ingresa el c�digo";
	public static final String DETALLO_INGRESA_EL_CODIGO_TICKET= "Ingresa el c�digo alfanum�rico de 6 caracteres.";
	public static final String VERIFICAR = "Verificar";
	
}
