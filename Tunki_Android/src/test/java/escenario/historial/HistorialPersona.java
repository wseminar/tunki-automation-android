package escenario.historial;


import java.io.IOException;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.everis.ExecutionInfo;
import com.everis.GlobalData;
import com.everis.data.DataDictionary;

import scripts.helpers.Reutilizable;
import scripts.helpers.TestLinkConfig;
import scripts.persona.Historial;
import web.Automation;

import com.everis.EFA;

@RunWith(Parameterized.class)
public class HistorialPersona extends Automation
{
    /**
     * Script: Web simple form test
     *
     * @param executionName
     *            Test name
     * @param data
     *            Data for current interaction
     */
    public HistorialPersona(String executionName, DataDictionary data)
    {
        ExecutionInfo.setExecutionName(executionName);
        // Fill Global Dictionary
        GlobalData.load(data);
    }
    /**
     * Load data for all interactions
     *
     * @return List all interactions with data for the test
     * @throws Exception
     */
    @Parameters(name = "{0}")
    public static List<Object> loadTestData() throws Exception
    {
        // Call data loader for all interactions
        return loadData();
    }
    /**
     * Scenario - Precondition for the test
     *
     * @throws IOException
     * @throws IllegalArgumentException
     */
    @Before
    public void beforeTest() throws Exception
    {
    	TestLinkConfig.iniciarConfiguracion();
    	
    	DesiredCapabilities capabilities = Reutilizable.getCapabilities();
        EFA.setCapabilities(capabilities);
        EFA.loadExecutionInfo();
    }
    /**
     * Simple example
     */
    @Test
    public void script()
    {
    	String output = "";
        try {
			Historial.historial();
			output = Historial.getResult();
		} catch (Exception e) {
			output = "Error: " + e.getMessage();
		}
        finally {
			ExecutionInfo.setResult(output);
		}
    }
}
