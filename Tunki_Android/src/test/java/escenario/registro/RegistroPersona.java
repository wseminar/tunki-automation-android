package escenario.registro;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.everis.ExecutionInfo;
import com.everis.GlobalData;
import com.everis.data.DataDictionary;

import io.appium.java_client.ios.IOSDriver;
import scripts.helpers.Reutilizable;
import scripts.helpers.TestLinkConfig;
import scripts.persona.Registro_new;
import web.Automation;

import com.everis.EFA;

@RunWith(Parameterized.class)
public class RegistroPersona extends Automation
{
    /**
     * Script: Web simple form test
     *
     * @param executionName
     *            Test name
     * @param data
     *            Data for current interaction
     */
    public RegistroPersona(String executionName, DataDictionary data)
    {
        ExecutionInfo.setExecutionName(executionName);
        // Fill Global Dictionary
        GlobalData.load(data);
    }
    /**
     * Load data for all interactions
     *
     * @return List all interactions with data for the test
     * @throws Exception
     */
    @Parameters(name = "{0}")
    public static List<Object> loadTestData() throws Exception
    {
        // Call data loader for all interactions
        return loadData();
    }
    /**
     * Scenario - Precondition for the test
     *
     * @throws IOException
     * @throws IllegalArgumentException
     */
    @Before
    public void beforeTest() throws Exception
    {
    		TestLinkConfig.iniciarConfiguracion();
    	
    		DesiredCapabilities capabilities = Reutilizable.getCapabilities();
        EFA.setCapabilities(capabilities);
        //EFA.cv_driver = new IOSDriver(capabilities);
        //EFA.cv_driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        EFA.loadExecutionInfo();
    }
    /**
     * Simple example
     */
    @Test
    public void script()
    {
    	String output = "";
        try {
//			Registro.RegistroPersona();
//			output = Registro.getResult();
        	Registro_new.RegistroPersona();
        	output = Registro_new.getResult();
		} catch (Exception e) {
			output = "Error: " + e.getMessage();
		}
        finally {
			ExecutionInfo.setResult(output);
		}
    }
}