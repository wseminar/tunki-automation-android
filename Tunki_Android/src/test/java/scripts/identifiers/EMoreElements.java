package scripts.identifiers;

public enum EMoreElements {
	PROGRESS("//*[@class='android.widget.ProgressBar']"),
	TEXTOESPERA("//*[@class='android.widget.TextView'][1]"),
	lblTITULO("tv_title"),
	IMGFACE("iv_face"),
	IMGESPERA("iv_waiting"),
	IMGVIEW("image_view"),
	MOREINFO("iv_info"),
	IMGROCKET("lav_rocket"),
	lblSINTARJ("tv_info"),
	DIALOGICON("dialog_icon"),
	LOGOCUENTASOCIADA("iv_logo"),
	CHKREMEMBER("chk_remember"),
	TV_TOTAL("tv_total"),
	TXT_NAME_COMERCE("txt_name_commerce"),
	TXT_DATE_TIME("txt_date_time"),
	SPINNER_DOCUMENT("spinner_document"),
	IMG_CLOUD_SPEC("img_cloud_spec"),
	ITEM_ACCOUNT_MANAGMENT("item_account_managment"),
	RV_LIST_ACCOUNT("rv_list_account"),
	LISTADO_CUENTAS_DEBITO("rv_payment_method"),
	SW_SESSION_ALIVE("sw_session_alive"),
	CB_COND_TERMS_USE("cb_cond_terms_use"),
	IV_WELCOME("iv_welcome"),
	IV_CLOUD("iv_cloud"),
	SELECT_DIALOG_LISTVIEW("select_dialog_listview"),
	VIEW_PASSWORD("text_input_password_toggle"),
	IV_TUNKI("iv_tunki"),
	INVITAR_GMAIL("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/com.android.internal.widget.ResolverDrawerLayout/android.widget.ListView/android.widget.LinearLayout[1]"),
	SPLASH("splash"),
	IV_CHECK("iv_check"),
	IV_TICKET_GLOBE("iv_ticket_globe");
	
	private String value;
	
	EMoreElements(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
}
