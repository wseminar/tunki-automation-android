package scripts.identifiers;

public enum ETextArea {
	STORENAME("et_store_name"),
	ET_EMAIL("et_email"),
	TXTPASWORD("et_password"),
	TXTPROPINA("et_tip"),
	TXTMENSAJERROR("txt_message"),
	TXTP2PNAME("txt_p2p_name"),
	TXTP2PHONE("txt_p2p_phone_number"),
	TXTMONTO("et_amount"),
	TXTCARDNUMBER("et_card_number"),
	TXTDOCNUMBER("et_doc_number"),
	CVV("et_cvv"),
	EXPDATE("et_expiration_date"),
	PASSWORDBPI("et_password_bpi"),
	ET_DOCUMENT("et_document"),
	ET_NAME("et_name"),
	ET_LAST_NAME("et_last_name"),
	ET_CODE_1("et_code_1"),
	ET_CODE_2("et_code_2"),
	ET_CODE_3("et_code_3"),
	ET_CODE_4("et_code_4"),
	ET_CODE_5("et_code_5"),
	ET_CODE_6("et_code_6"),
	TXT_NAME_COMMERCE("txt_name_commerce"),
	TXT_NAME("txt_name"),
	ET_USER_NAME("et_user_name"),
	ET_COMMERCE_NAME("et_commerce_name"),
	ET_BUSINESS_NAME("et_business_name"),
	ET_RUC_NUMBER("et_ruc_number"),
	ET_ADMIN_NAME("et_admin_name"),
	ET_ADMIN_LAST_NAME("et_admin_last_name"),
	ET_ADMIN_EMAIL("et_admin_email"),
	ET_ADMIN_PHONE("et_admin_phone"),
	ET_ADMIN_DOC_NUMBER("et_admin_doc_number"),
	ET_CONFIRM_PASSWORD("et_confirm_password"),
	ET_STORE_ADDRESS("et_store_address"),
	ET_ACCOUNT_NUMBER("et_account_number"),
	TXT_TITLE("txt_title"),
	ET_PASSWORD_CONFIRMATION("et_password_confirmation"),
	ET_PHONE("et_phone"),
	ET_VALIDATION_CODE("et_validation_code"),
	ET_DETAIL("et_detail"),
	EDIT_TEXT("edit_text"),
	SNACKBAR_TEXT("snackbar_text"),
	ET_QR_CODE("et_qr_code"),
	ET_QUANTITY("et_quantity");
	
	private String id;
	
	ETextArea(String id){
		this.id = id;
	}
	
	public String getId(){
		return id;
	}
}
