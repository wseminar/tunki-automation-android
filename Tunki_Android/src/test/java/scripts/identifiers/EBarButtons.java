package scripts.identifiers;

public enum EBarButtons {
	CUSTOMER("menu_list_clients"),
	MORE("menu_more"),
	MOVE("menu_move"),
	MOVE_COMMERCE("menu_movements"),
	CONTACT("menu_list_p2p_contacts"),
	COMMERCE("menu_list_commerce"),
	SERVICES("menu_list_services");
	
	private String id;
	
	EBarButtons(String id){
		this.id = id;
	}
	
	public String getId(){
		return id;
	}
}
