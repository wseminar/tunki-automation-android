package scripts.identifiers;

public enum EComponents {
	ID("id"),
	XPATH("xpath");

	private String value;

	EComponents (String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}
}
