package scripts.identifiers;

public enum EText {
	
	TV_TITLE("tv_title"),
	TV_DETAIL("tv_detail"),
	TV_OTHER_SIGN_IN("tv_other_sign_in"),
	CB_ACTIVE_SESSION("cb_active_session"),
	TIL_EMAIL("til_email"),
	TIL_PASSWORD("til_password"),
	TV_TOOLBAR_TITLE("tv_toolbar_title"),
	TV_MAX_AMOUNT("tv_max_amount"),
	TIL_DETAIL("til_detail"),
	TIL_CARD_NUMBER("til_card_number"),
	TIL_EXPIRATION_DATE("til_expiration_date"),
	TIL_CVV("til_cvv"),
	TV_CVV_INFO("tv_cvv_info"),
	TIL_PASSWORD_BPI("til_password_bpi"),
	TV_INFO("tv_info"),
	TV_DESCRIPTION("tv_description"),
	TV_EMPTY_TEXT("tv_empty_text"),
	TV_NAME("tv_name"),
	TV_GIVER_TITLE("tv_giver_title"),
	TV_RECEIVER_TITLE("tv_receiver_title"),
	TV_PAYMENT_METHOD_TITLE("tv_payment_method_title"),
	TV_OPERATION_TITLE("tv_operation_title"),
	TV_DATE_TITLE("tv_date_title"),
	TV_TIME_TITLE("tv_time_title"),
	TV_AMOUNT_TITLE("tv_amount_title"),
	TV_TIP_TITLE("tv_tip_title"),
	TV_TOTAL_TITLE("tv_total_title"),
	BOTTOM_TEXT("bottom_text"),
	TV_PASSWORD_INSTRUCTION("tv_password_instruction"),
	TV_CODE("tv_code"),
	TV_EMPTY("tv_empty");
	
	private String value;
	
	EText(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
}
