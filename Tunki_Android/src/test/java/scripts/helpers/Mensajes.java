package scripts.helpers;

public class Mensajes {

	public static final String SUCCESS = "Ejecuci�n Correcta";
	public static final String DATANOTFOUND = "DataNotFound";
	public static final String PAGOREALIZADO = "�Pago Realizado!";
	public static final String CONSTANCIAPAGO = "Constancia de pago";
	public static final String COBROREALIZADO = "�Cobro Realizado!";
	public static final String ENTENDIDO = "Entendido";
	public static final String DEVOLVER = "Devolver";
	public static final String PAGORECHAZADO = "�Pago Rechazado!";
	public static final String COBRORECHAZADO = "�Cobro Rechazado!";
	public static final String CREDENCIALESINVALIDAS = "Tus credenciales no coinciden, por favor intenta nuevamente.";
	public static final String FORGOT_PASSWORD = "Restablecer contrase�a";
	public static final String UPS_GENEREAL = "�Ups! Hubo un error, por favor intenta de nuevo";
	public static final String ERROR_INESPERADO = "Ha ocurrido un error inesperado";
	public static final String AGREGAR_TARJETA_RECARGAS ="Necesitas agregar una cuenta de ahorros para realizar recargas";
	public static final String SESION_ACTIVA= "Expected Ejecuci�n Correcta but was �Quieres mantener tu sesi�n activa en este dispositivo?";
}
