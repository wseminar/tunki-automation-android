package scripts.helpers;

import java.io.File;
import java.net.Authenticator;
import java.net.PasswordAuthentication;

import com.everis.GlobalData;
import com.everis.PropertyReader;
import com.everis.integration.Testlink;

public class TestLinkConfig {

	private static PropertyReader reader = new PropertyReader();
	private static boolean proxyActive = false;
	
	public static void iniciarConfiguracion(){
		reader.getProperties(new File("data").getAbsolutePath() + "/testlink.properties");
		if(reader.getProperty("USETESTLINK").equals("TRUE"))
		{
			definirProxy();
			definirTestLink();
		}
	}
	
	public static void definirProxy(){
		if(reader.getProperty("PROXY_ACTIVE").equals("TRUE") && !proxyActive){
			final String authUser = reader.getProperty("PROXY_USER");
			final String authPassword = reader.getProperty("PROXY_PASS");
            System.setProperty("http.proxyHost", reader.getProperty("PROXY_HOST"));
            System.setProperty("http.proxyPort", reader.getProperty("PROXY_PORT"));
            System.setProperty("http.proxyUser", authUser);
            System.setProperty("http.proxyPassword", authPassword);
            
            if (authUser != null && authPassword != null) {
                    Authenticator.setDefault(
                      new Authenticator() {
                        public PasswordAuthentication getPasswordAuthentication() {
                          return new PasswordAuthentication(
                                  authUser, authPassword.toCharArray()
                          );
                        }
                      }
                    );
                }
            proxyActive = true;
		}
	}
	
	public static void definirTestLink(){
		Testlink.setEnable(true);
        Testlink.DEV_KEY = reader.getProperty("DEVKEY");
        Testlink.URL = reader.getProperty("URL");
        Testlink.TestProject = reader.getProperty("TESTPROJECT");
        Testlink.TestPlanName = reader.getProperty("TESTPLANNAME");
        Testlink.TestBuild = GlobalData.getData("viTestBuild");
        Testlink.TestCaseId = GlobalData.getData("viTestCaseId");
        Testlink.Platform = reader.getProperty("PLATFORM");
	}
}
