package scripts.helpers;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;

import org.omg.PortableServer.THREAD_POLICY_ID;
import org.openqa.selenium.Dimension;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import cucumber.api.java.fr.Et;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import script.textosTunki.Textos;
import script.textosTunki.ValidacionTexto;
import scripts.Util.UtilPaymentCommerce;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;
import scripts.persona.Firebase;
import scripts.persona.GestionCuenta;

public class ReusableRegistroPersona {
	
	public static String ingresarDatosRegistrate() throws Exception{
		String msjFinal = "";
		if(!GlobalData.getData("viEmail").isEmpty()){
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()),
					GlobalData.getData("viEmail"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viPassword").isEmpty()){
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()),
					GlobalData.getData("viPassword"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viConfirmPassword").isEmpty()){
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getId()),
					GlobalData.getData("viConfirmPassword"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONTINUE.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String ingresarDatos() throws Exception{
		String msjFinal = "";
		Element optTipoDoc = new Element("xpath",
				"//android.widget.CheckedTextView[@text='" + GlobalData.getData("viTipoDocumento") + "']");
		if(GlobalData.getData("viTipoFoto").toUpperCase().equals("SELFIE")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PICTURE.getId()));
			msjFinal=Reutilizable.selfiePhoto();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(GlobalData.getData("viTipoFoto").toUpperCase().equals("GALERIA")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PICTURE.getId()));
			msjFinal = Reutilizable.galleryPhoto();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(!GlobalData.getData("viNombre").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_NAME.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_NAME.getId()),
					GlobalData.getData("viNombre"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viApellido").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_LAST_NAME.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_LAST_NAME.getId()),
					GlobalData.getData("viApellido"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.SPINNER_DOCUMENT.getValue()));
		EFA.executeAction(Action.Click, optTipoDoc);
		if(!GlobalData.getData("viDocIdentidad").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DOCUMENT.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DOCUMENT.getId()),
					GlobalData.getData("viDocIdentidad"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viPhone").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PHONE.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PHONE.getId()),
					GlobalData.getData("viPhone"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONTINUE.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())
			return msjFinal;
		return msjFinal;
	}
	
	public static String ingresarCodigoSMS() throws Exception {
		String msjFinal = "";
		if (!Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()))
				.toString())) {
			return msjFinal = "No muestra pop up valida tu correo";
		}
		if (GlobalData.getData("viReenviarCodigoSMS").equals("SI")) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_RESEND.getId()));
			Thread.sleep(2000);
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			Thread.sleep(2000);
			if (!msjFinal.isEmpty())
				return msjFinal;
			if (!Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()))
					.toString())) {
				msjFinal = "No muestra pop up valida tu telefono";
			}
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()),
					GlobalData.getData("viCodigoSMS"));
			EFA.cs_getTestEvidence("Ingresar Codigo", 500);
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			Thread.sleep(2000);
			EFA.cs_getTestEvidence("Error Validar telefono", 500);
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty())
				return msjFinal;
		}
		if (GlobalData.getData("viReenviarCodigoSMS").equals("NO")) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()),
					GlobalData.getData("viCodigoSMS"));

			EFA.cs_getTestEvidence("Ingresar Codigo", 500);
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			Thread.sleep(2000);
			EFA.cs_getTestEvidence("Error Validar telefono", 500);
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty())
				return msjFinal;
		}
		if (GlobalData.getData("viReenviarCodigoSMS").toUpperCase().equals("REENVIO FORMULARIO")) {
			EFA.executeAction(Action.Back, null);
			msjFinal = reIngresarDatos();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		System.out.println("Ingresar a Firebase");
		if(!GlobalData.getData("viEmail2").isEmpty()){
			String phone = GlobalData.getData("viPhone");
			String email = GlobalData.getData("viEmail2");
			Firebase.smsFirebase(phone, email);	
		}
		if(!GlobalData.getData("viPhone2").isEmpty()){
			String phone = GlobalData.getData("viPhone2");
			String email = GlobalData.getData("viEmail");
			Firebase.smsFirebase(phone, email);
		}
		if(GlobalData.getData("viEmail2").isEmpty() && GlobalData.getData("viPhone2").isEmpty()){
			String phone = GlobalData.getData("viPhone");
			String email = GlobalData.getData("viEmail");
			Firebase.smsFirebase(phone, email);	
		}
		Thread.sleep(30000);
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()));
		EFA.executeAction(Action.SendKeys,
				Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()), "ABC123");
		EFA.cs_getTestEvidence("Ingresar Codigo", 500);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		Thread.sleep(1000);
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())
			return msjFinal;
		return msjFinal;

	}

	public static String ingresarCodigo() throws Exception {
		String msjFinal = "";
		if (!Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()))
				.toString())) {
			return msjFinal = "No muestra pop up valida tu correo";
		}
		if (GlobalData.getData("viReenviarCodigo").equals("SI")) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_RESEND.getId()));
			Thread.sleep(2000);
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			Thread.sleep(2000);
			if (!msjFinal.isEmpty())
				return msjFinal;
			if (!Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()))
					.toString())) {
				msjFinal = "No muestra pop up valida tu correo";
			}
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()),
					GlobalData.getData("viCodigo"));
			EFA.cs_getTestEvidence("Ingresar Codigo", 500);
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			Thread.sleep(2000);
			EFA.cs_getTestEvidence("Error Validar Correo", 500);
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty())
				return msjFinal;
		}
		if (!GlobalData.getData("viCodigo").isEmpty()) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()),
					GlobalData.getData("viCodigo"));
			EFA.cs_getTestEvidence("Ingresar Codigo", 500);
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			Thread.sleep(4000);
			EFA.cs_getTestEvidence("Error Validar Correo", 500);
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty())
				return msjFinal;
		}
		if (GlobalData.getData("viReenviarCodigo").toUpperCase().equals("REENVIO FORMULARIO")) {
			EFA.executeAction(Action.Back, null);
			msjFinal = reIngresarDatosRegistrate();
			if(!msjFinal.isEmpty())return msjFinal;	
		}
		Thread.sleep(30000);
		String codigo = obtenerCodigoVerificacionAlfa();
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()));
		EFA.executeAction(Action.SendKeys,
				Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()), codigo);
		EFA.cs_getTestEvidence("Ingresar Codigo", 500);
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		Thread.sleep(1000);
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())
			return msjFinal;
		return msjFinal;
	}

	public static String obtenerCodigoVerificacionAlfa() throws Exception {
		String codigo = "";
		String primerDato = "";

		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i < 1; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "(?=.*[0-9])(?=.*[a-zA-Z]).{6,}";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

	public static String AgregarTarjeta() throws Exception {
		String msjFinal = "";
		if(GlobalData.getData("viTipoTarjeta").toUpperCase().equals("DEBITO")){
			msjFinal = ingresarDatosTarjetaDebito();
			if(!msjFinal.isEmpty())return msjFinal;
			Thread.sleep(5000);
			if (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.LISTADO_CUENTAS_DEBITO.getValue())).toString())){
				if(GlobalData.getData("viReIngresoGestionCuenta").toUpperCase().equals("VARIAS CUENTAS".toUpperCase())){
					return msjFinal = "";
				}
				EFA.cs_getTestEvidence("Listado de Cuentas", 500);
				if(GlobalData.getData("viReIngresoGestionCuenta").toUpperCase().equals("DOS CUENTAS".toUpperCase())){
					GestionCuenta.CapturarTextoPorId("v_item", "AHORRO MILLONARIA MN");
					EFA.cs_getTestEvidence("Seleccion de Cuentas", 500);
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
					while (Boolean.parseBoolean(EFA
							.executeAction(Action.IsElementPresent,
									Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
							.toString()));
					msjFinal = Reutilizable.verificarMensajesError();
					if (!msjFinal.equals(""))return msjFinal;
					if(GlobalData.getData("viValidarAhora").toUpperCase().equals("SI")){
						msjFinal = UtilPaymentCommerce.validateTarjeta();
						if(!msjFinal.isEmpty())return msjFinal;
					}
					return msjFinal;
				}
				TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
				x.press(334,758).moveTo(6,-411).release().perform();
				Thread.sleep(1000);
				Element item = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.view.View[3]/android.widget.RadioButton");
				EFA.executeAction(Action.Click, item);
				EFA.cs_getTestEvidence("Seleccion de Cuentas", 500);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
				Thread.sleep(15000);
				while (Boolean.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
						.toString()));
				msjFinal = Reutilizable.verificarMensajesError();
				if (!msjFinal.equals(""))
					return msjFinal;
			}
			if (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId())).toString()) ||Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE_LATER.getId())).toString()) || Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_START.getId())).toString())) {
				Reutilizable.tomarCapturaPantalla(500, "Cuenta Asocida sin validar");
				
			} else {
				msjFinal = "No se muestra la pantalla Cuenta Asociada";
				return msjFinal;
			}
			if(GlobalData.getData("viValidarAhora").toUpperCase().equals("SI")){
				msjFinal = UtilPaymentCommerce.validateTarjeta();
				if(!msjFinal.isEmpty())return msjFinal;
			}
		}
		if(GlobalData.getData("viTipoTarjeta").toUpperCase().equals("CREDITO")){
			msjFinal = ingresarDatosTarjetaCredito();
			if(!msjFinal.isEmpty())return msjFinal;
			return msjFinal;
		}

		return msjFinal;
	}
	
	public static String ingresarDatosTarjetaDebito() throws Exception{	
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(),EButtons.BTN_TARJETA_DEBITO.getId()));
		EFA.evidenceEnabled = false;
		msjFinal = ValidacionTexto.textPaymentMethod();
		if(!msjFinal.isEmpty())return msjFinal;
		EFA.evidenceEnabled=true;
		if(!GlobalData.getData("viNroTarjeta").isEmpty()){
			if(GlobalData.getData("viReIngreso").equals("SI GCP")){
				if(!GlobalData.getData("viNroTarjeta_2").isEmpty()){
					EFA.executeAction(Action.Click,
							Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
					EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()), GlobalData.getData("viNroTarjeta_2"));
					EFA.executeAction(Action.Back, null);
				}
				if(GlobalData.getData("viBotonIntermitente").equals("SI")){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
				}
			}
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.Clear,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()),
					GlobalData.getData("viNroTarjeta"));
			EFA.executeAction(Action.Back, null);

		}
		if(!GlobalData.getData("viFechaVencimiento").isEmpty()){
			if(GlobalData.getData("viReIngreso").equals("SI GCP")){
				if(!GlobalData.getData("viFechaVencimiento_2").isEmpty()){
					EFA.executeAction(Action.Click,
							Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()));
					EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()), GlobalData.getData("viFechaVencimiento_2"));
					EFA.executeAction(Action.Back, null);
				}
				if(GlobalData.getData("viBotonIntermitente").equals("SI")){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
				}
			}
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.Clear,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()),
					GlobalData.getData("viFechaVencimiento"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viCVV").isEmpty()){
			if(GlobalData.getData("viReIngreso").equals("SI GCP")){
				if(!GlobalData.getData("viCVV_2").isEmpty()){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
					EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()), GlobalData.getData("viCVV_2"));
					EFA.executeAction(Action.Back, null);
				}
				if(GlobalData.getData("viBotonIntermitente").equals("SI")){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
				}
			}
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()),
					GlobalData.getData("viCVV"));
			EFA.executeAction(Action.Back, null);
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId())).toString())){
			if(!GlobalData.getData("viClaveWeb").isEmpty()){
				if(GlobalData.getData("viReIngreso").equals("SI GCP")){
					if(!GlobalData.getData("viClaveWeb_2").isEmpty()){
						EFA.executeAction(Action.Click,
								Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()));
						EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()), GlobalData.getData("viClaveWeb_2"));
						EFA.executeAction(Action.Back, null);
					}
					if(GlobalData.getData("viBotonIntermitente").equals("SI")){
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
					}
				}
				EFA.executeAction(Action.Click,
						Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()));
				EFA.executeAction(Action.Clear,
						Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()));
				EFA.executeAction(Action.SendKeys,
						Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()),
						GlobalData.getData("viClaveWeb"));
				EFA.executeAction(Action.Back, null);
			}
		}
		Thread.sleep(2000);
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
		Thread.sleep(25000);
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		if(GlobalData.getData("viReIngresoGestionCuenta").equals("FORMA")){
			msjFinal = reingresarDatosTarjeta();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId())).toString())){
			if(EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue())).toString().toUpperCase().equals(Textos.ERROR_INESPERADO.toUpperCase())){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
				Thread.sleep(1000);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
				while (Boolean.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
						.toString()))
					;
			}
		}
		msjFinal = Reutilizable.verificarMensajesError();
		Thread.sleep(5000);
		if(msjFinal.trim().toUpperCase().equals(Mensajes.CREDENCIALESINVALIDAS.trim().toUpperCase())){
			if(GlobalData.getData("viReIngresoGestionCuenta").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
				msjFinal = reingresarDatosTarjeta();
				if(!msjFinal.isEmpty())return msjFinal;
			}
		}
		if (!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String ingresarDatosTarjetaCredito() throws Exception{	
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(), EButtons.BTN_TARJETA_CREDITO.getId()));
		if(GlobalData.getData("viFlujoEspeciales").toUpperCase().equals("MANTENER DATOS")){
			mantenerDatosIngresados();
			return msjFinal;
		}
		if(GlobalData.getData("viFlujoEspeciales").toUpperCase().equals("DATOS INCOMPLETOS")){
			msjFinal = datosIncompletos();
			if(!msjFinal.isEmpty())return msjFinal;
			return msjFinal;
		}
		
		if(!GlobalData.getData("viNroTarjeta").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()),GlobalData.getData("viNroTarjeta"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viFechaVencimiento").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()),GlobalData.getData("viFechaVencimiento"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viCVV").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()),GlobalData.getData("viCVV"));
			EFA.executeAction(Action.Back, null);
		}
		Thread.sleep(4000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
		Thread.sleep(2000);
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId())).toString())){
			if(EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue())).toString().toUpperCase().equals(Textos.ERROR_INESPERADO.toUpperCase())){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
				Thread.sleep(1000);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
				while (Boolean.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
						.toString()))
					;
			}
		}
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE_LATER.getId())).toString())){
			return msjFinal="No muestra boton Validar Tarjeta de Credito";
		}
		if(GlobalData.getData("viValidarAhora").toUpperCase().equals("SI")){
			msjFinal = UtilPaymentCommerce.validateTarjeta();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		return msjFinal;
	}
	
	public static boolean pagoP2P() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CONTACT.getId()));
		Thread.sleep(3000);
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();	
		if(!msjFinal.isEmpty()) return false;
		Thread.sleep(5000);
		msjFinal = ReusablePxP.buscarPersonaP2P(GlobalData.getData("viPersonaPagar"));
		if(!msjFinal.isEmpty()) return false;
		msjFinal = ReusablePxP.EnvioPagoP2P();
		if(!msjFinal.isEmpty()) return false;
		EFA.executeAction(Action.Back, null);
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.equals("")) return false;	
		if(GlobalData.getData("viValidarMovimiento").equals("SI")){		
			Reutilizable.validarHistorialPersona(GlobalData.getData("viPersonaPagar").toUpperCase());
		}
		return true;
	}
	
		
	public static void swiptToBottom() throws Exception{
		Dimension dim = EFA.cv_driver.manage().window().getSize();
		int height = dim.getHeight();
		int width = dim.getWidth();
		int x = width/2;
		
		int top_y = (int)(height*0.80);
		int bottom_y = (int)(height*0.20);
		
		System.out.println(x+"   "+top_y+"  "+bottom_y);
		TouchAction s = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		s.press(x, top_y).waitAction(Duration.ofMillis(2500)).moveTo(x, bottom_y).release().perform();
		EFA.cs_getTestEvidence("Deslizar", 500);
	}
	
	public static String reingresarDatosTarjeta() throws Exception{
		String msjFinal = "";
		if(!GlobalData.getData("viNroTarjeta_2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()), GlobalData.getData("viNroTarjeta_2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viFechaVencimiento_2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()), GlobalData.getData("viFechaVencimiento_2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viCVV_2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()), GlobalData.getData("viCVV_2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viClaveWeb_2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()), GlobalData.getData("viClaveWeb_2"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
		Thread.sleep(25000);
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue())).toString().toUpperCase().equals(Textos.COBRAR_A.toUpperCase())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
			Thread.sleep(1000);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
		}
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String reIngresarDatos() throws Exception{
		String msjFinal = "";
		if(GlobalData.getData("viTipoFoto2").toUpperCase().equals("SELFIE")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PICTURE.getId()));
			msjFinal=Reutilizable.selfiePhoto();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(GlobalData.getData("viTipoFoto2").toUpperCase().equals("GALERIA")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PICTURE.getId()));
			msjFinal = Reutilizable.galleryPhoto();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(!GlobalData.getData("viClienteNombre2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_NAME.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_NAME.getId()),GlobalData.getData("viClienteNombre2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viClienteApellido2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_LAST_NAME.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_LAST_NAME.getId()),GlobalData.getData("viClienteApellido2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viDocIdentidad_2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DOCUMENT.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DOCUMENT.getId()),GlobalData.getData("viDocIdentidad_2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viPhone2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PHONE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PHONE.getId()),GlobalData.getData("viPhone2"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONTINUE.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return msjFinal;
		
		return msjFinal;
	}
	public static String reIngresarDatosRegistrate() throws Exception{
		String msjFinal = "";
		if(!GlobalData.getData("viEmail2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()),
					GlobalData.getData("viEmail2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viPassword_2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()),
					GlobalData.getData("viPassword_2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viConfirmPassword_2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getId()),
					GlobalData.getData("viConfirmPassword_2"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONTINUE.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static void mantenerDatosIngresados() throws Exception{
		if(!GlobalData.getData("viNroTarjeta").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()),GlobalData.getData("viNroTarjeta"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viFechaVencimiento").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()),GlobalData.getData("viFechaVencimiento"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viCVV").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()),GlobalData.getData("viCVV"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SNNIPER_CARD_TYPE.getId()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(),EButtons.BTN_TARJETA_DEBITO.getId()));
		if(!GlobalData.getData("viClaveWeb").isEmpty()){
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()));
			EFA.executeAction(Action.Clear,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.PASSWORDBPI.getId()),
					GlobalData.getData("viClaveWeb"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SNNIPER_CARD_TYPE.getId()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(),EButtons.BTN_TARJETA_CREDITO.getId()));	
	}
	
	public static String datosIncompletos() throws Exception{
		String msjFinal = "";
		if(!GlobalData.getData("viNroTarjeta_2").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()),GlobalData.getData("viNroTarjeta_2"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viFechaVencimiento").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()),GlobalData.getData("viFechaVencimiento"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viCVV").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()),GlobalData.getData("viCVV"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
		if(!GlobalData.getData("viNroTarjeta").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId()),GlobalData.getData("viNroTarjeta"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viFechaVencimiento_2").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()),GlobalData.getData("viFechaVencimiento_2"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
		if(!GlobalData.getData("viFechaVencimiento").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EXPDATE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EXPDATE.getId()),GlobalData.getData("viFechaVencimiento"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viCVV_2").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()),GlobalData.getData("viCVV_2"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
		if(!GlobalData.getData("viCVV").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.CVV.getId()),GlobalData.getData("viCVV"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK.getId()));
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_DETAIL.getValue())).toString().toUpperCase().equals(Textos.CREDENCIALES_INVALIDAS_TARJETA.toUpperCase())){
			return msjFinal="no muestra mensaje correcto";
		}
		return msjFinal;
	}
}
