package scripts.helpers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ValidacionBD {

	public static String persona_ValidarToken(String email) throws Exception {
		
		String query = "select nvl(device_token,'DataNotFound') from customer where upper(email) = upper('"+email+"')";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next()){
			return rs.getString(1);
		}
		
		return Mensajes.DATANOTFOUND;
		
	}
	
	public static String negocio_ValidarToken(String usuario) throws Exception {
		 
		String query = "select nvl(device_token,'DataNotFound') from commerce_user where upper(username) = upper('"+usuario+"')";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next()){
			return rs.getString(1);
		}
		
		return Mensajes.DATANOTFOUND;
		
	}
	
	public static String persona_ValidarTransaccion(String comercio, String monto, String persona) throws Exception{
        String query = "select t.status, t.description " +
        "from transaction t " +
        "inner join checkin ck on t.checkin_id=ck.id " +
        "inner join customer c on ck.customer_id=c.id " +
        "inner join commerce_user cu on ck.commerce_user_id=cu.id " +
        "inner join commerce co on cu.commerce_id=co.id " +
        "where co.brand_name = '" + comercio + "' and (t.amount + t.tip)="+ monto +" and c.email='" + persona + "' and create_date >= cast(sysdate-1 as timestamp) " +
        "order by create_date desc";
        
        Connection cn = Conector.getOracleConnection();
        Statement st = cn.createStatement();
        ResultSet rs = st.executeQuery(query);
        if(rs.next()){
            return rs.getString(1) + ": " + rs.getString(2);
        }
        
        return Mensajes.DATANOTFOUND;
	}
	
	public static String persona_obtenerGlosa(String email, String nroTarjeta) throws Exception{
		String query = "select vc.CODE_VALIDATED " +
		"from tbl_validate_code vc join tbl_credit_card cc on vc.credit_card_id=cc.credit_card_id " +
		"WHERE VC.CREATED_USER='"+email+"' AND TRUNC (VC.CREATED_DATE)=TRUNC (SYSDATE) " + 
		"order by vc.created_Date desc";
		
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		
		if(rs.next())
			return rs.getString(1);
		
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerPersonId(String dni) throws Exception{
		String query = "select person_id from tbl_person where doc_number='"+dni+"'";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerDnixPersonId(String email) throws Exception{
		String query = "select doc_number from tbl_person where upper(email) = upper('"+email+"')";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerPersonIdxEmail(String email) throws Exception{
		String query = "select person_id from tbl_person where upper(email) = upper('"+email+"')";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String modificarDatosTblUser(String personId) throws Exception{
		String query = "UPDATE tbl_user set created_user='hola1@gmail.com',last_modified_user='hola1@gmail.com',user_name='hola1@gmail.com' \n" + 
				"WHERE person_id='"+personId+"'";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		
		return Mensajes.DATANOTFOUND;
	}
	
	public static String modificarDatosTblPerson(String personId) throws Exception{
		String query = "UPDATE tbl_person SET phone='921231',email='hi3@gmail.com',doc_number='3232',created_user='hola1@gmail.com',LAST_MODIFIED_USER='hola1@gmail.com' "
				+ "WHERE person_id="+personId;
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerCodigoValidacionTC(String email) throws Exception{
		String query = "select CODE_VALIDATED from tbl_validate_code where upper(created_user) = '"+email+"' order by created_date desc";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String eliminarTarjetaTblPMethod(String personId) throws Exception{
		String query = "update TBL_PAYMENT_METHOD set status=1 where PERSON_ID="+personId+" and status=0";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String eliminarTarjetaTblPUser(String personId) throws Exception{
		String query = "update tbl_user set is_able_to_pay = 0 , is_our_client=0 where PERSON_ID ="+personId;
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String verificarIsAbleToTransfer(String dni) throws Exception{
		String query = "select IS_ABLE_TO_TRANSFER from tbl_person where doc_number = '"+dni+"'";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String createTicketToday() throws Exception{
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		 Date date = new Date();
		String xfechaInicio = "'"+formateador.format(date)+" 08:00:00'";
		String xfechaFinal = "'"+formateador.format(date)+" 22:00:00'";
		System.out.println(xfechaInicio);
		String query = "insert into TBL_PRODUCT values"+
		"(TBL_PRODUCT_SEQ.NEXTVAL,'Ticket Diario','Acercate a los establecimientos para canjear tu codigo',5000001279,null,3,100,1000,1,"+
		"TO_TIMESTAMP("+xfechaInicio+", 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP("+xfechaFinal+", 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP("+xfechaInicio+", 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP("+xfechaFinal+", 'YYYY-MM-DD HH24:MI:SS'),0,0,'BACKOFFICE_TUNKI',sysdate,'BACKOFFICE_TUNKI',sysdate)";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String verificacionCreacionTicket(String commerce_id ,String status) throws Exception{
		String query = "select status from tbl_product where commerce_id ='"+commerce_id+"'"+" and status = '"+status+"'";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerUserIdComercio(String usuario) throws Exception{
		String query = "select user_id from tbl_user where Upper(user_name) like Upper('%"+usuario+"%')";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerStoreIdComercio(String userId) throws Exception{
		String query = "SELECT store_id FROM TBL_COMMERCE_USER_STORE where user_id ="+userId;
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerCommerceIdComercio(String storeId) throws Exception{
		String query = "SELECT commerce_id FROM tbl_store where store_id ="+storeId;
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerProductIdComercio(String commerceId) throws Exception{
		String query = "select product_id from tbl_product where commerce_id="+commerceId+" and status ='0'";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerCodigoTicket(String productId,String email) throws Exception{
		String query = "select code from tbl_product_code where product_id = "+productId+" and status = 0 and upper(CREATED_USER) = upper('"+email+"') order by product_id desc";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String cambiarStatusTblProduct(String productId) throws Exception{
		String query = "update tbl_product set status = 1 where PRODUCT_ID ="+productId;
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerPaymentMethodId(String personId) throws Exception{
		String query = "select PAYMENT_METHOD_ID from TBL_PAYMENT_METHOD where PERSON_ID="+personId+" and status = 0";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String obtenerStatusTblPca(String paymentMethodId) throws Exception{
		String query = "select STATUS from tbl_pca where payment_method_id = "+paymentMethodId;
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		if(rs.next())return rs.getString(1);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String cambiarStatusTblPcaTD(String paymentMethodId) throws Exception{
		String query = "update tbl_pca set status=0 where payment_method_id ="+paymentMethodId;
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String cambiarStatusTblPcaMillonaria(String dni) throws Exception{
		String query = "UPDATE TBL_PCA SET STATUS = 0 where pca_id in "
				+ "(select pca.PCA_ID from tbl_person pe "
				+ "INNER JOIN TBL_PAYMENT_METHOD pm "
				+ "ON pe.person_id = pm.person_id "
				+ "INNER JOIN TBL_ACCOUNT_PERSON ap "
				+ "ON pm.PAYMENT_METHOD_ID = ap.PAYMENT_METHOD_ID "
				+ "INNER JOIN TBL_PCA pca "
				+ "ON pm.PAYMENT_METHOD_ID = pca.PAYMENT_METHOD_ID "
				+ "where pe.doc_number='"+dni+"' " 
				+ "and pm.status = 0 "
				+ "and pm.PAYMENT_METHOD_TYPE_ID = 4 "
				+ "and ap.ACCOUNT_DESCRIPTION='AHORRO MILLONARIA MN')";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String eliminarTarjeteaMillonaria(String dni) throws Exception{
		String query = "update TBL_PAYMENT_METHOD set status = 1 where PAYMENT_METHOD_ID in "
				+ "(select pca.PAYMENT_METHOD_ID from tbl_person pe "
				+ "INNER JOIN TBL_PAYMENT_METHOD pm "
				+ "ON pe.PERSON_ID = pm.PERSON_ID "
				+ "INNER JOIN TBL_ACCOUNT_PERSON ap "
				+ "on pm.PAYMENT_METHOD_ID = ap.PAYMENT_METHOD_ID "
				+ "INNER JOIN TBL_PCA pca  "
				+ "on pm.PAYMENT_METHOD_ID = pca.PAYMENT_METHOD_ID "
				+ "where pe.doc_number='"+dni+"' " 
				+ "and pm.status= 0 "
				+ "and ap.ACCOUNT_DESCRIPTION='AHORRO MILLONARIA MN') ";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String cambiarStatusValidarPca(String email, int status) throws Exception{
		String query = "update TBL_PCA set status = "+status
				+ "where upper(created_user) = upper('"+email+"') and status= 1 ";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String cambiarDefaultMillonaria(String dni) throws Exception{
		String query = "update TBL_PAYMENT_METHOD set BY_DEFAULT = 0,BY_DEFAULT_P2P= 0,BY_DEFAULT_RECHARGE= 0 where PAYMENT_METHOD_ID in "
				+ "(select pm.PAYMENT_METHOD_ID from tbl_person pe "
				+ "INNER JOIN TBL_PAYMENT_METHOD pm "
				+ "ON pe.PERSON_ID = pm.PERSON_ID "
				+ "INNER JOIN TBL_ACCOUNT_PERSON ap "
				+ "on pm.PAYMENT_METHOD_ID = ap.PAYMENT_METHOD_ID "
				+ "INNER JOIN TBL_PCA pca "
				+ "on pm.PAYMENT_METHOD_ID = pca.PAYMENT_METHOD_ID "
				+ "where pe.doc_number='"+dni+"' " 
				+ "and pm.status=0 "
				+ "and pm.by_default = 1 "
				+ "and ap.ACCOUNT_DESCRIPTION='AHORRO MILLONARIA MN') ";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String cambiarDefaultAhorro(String dni) throws Exception{
		String query = "update TBL_PAYMENT_METHOD set BY_DEFAULT = 1,BY_DEFAULT_P2P= 1,BY_DEFAULT_RECHARGE= 1 where PAYMENT_METHOD_ID in "
				+ "(select pm.PAYMENT_METHOD_ID from tbl_person pe "
				+ "INNER JOIN TBL_PAYMENT_METHOD pm "
				+ "ON pe.PERSON_ID = pm.PERSON_ID "
				+ "INNER JOIN TBL_ACCOUNT_PERSON ap "
				+ "on pm.PAYMENT_METHOD_ID = ap.PAYMENT_METHOD_ID "
				+ "INNER JOIN TBL_PCA pca "
				+ "on pm.PAYMENT_METHOD_ID = pca.PAYMENT_METHOD_ID "
				+ "where pe.doc_number='"+dni+"' "
				+ "and pm.status=0 "
				+ "and pm.by_default = 0 "
				+ "and ap.ACCOUNT_DESCRIPTION='AHORROS MN') ";
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
	
	public static String desbloquearUsuario(String personId) throws Exception{
		String query = "update tbl_user set status = 0 where person_id ="+personId;
		Connection cn = Conector.getOracleConnection();
		Statement st = cn.createStatement();
		ResultSet rs = st.executeQuery(query);
		return Mensajes.DATANOTFOUND;
	}
}
