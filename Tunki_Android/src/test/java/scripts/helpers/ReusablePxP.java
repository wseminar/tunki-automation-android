package scripts.helpers;

import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import script.textosTunki.Textos;
import script.textosTunki.ValidacionTexto;
import scripts.Util.UtilPaymentCommerce;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class ReusablePxP {
	
	public static String synchronizeContacts() throws Exception{
		String msjFinal = "";
		EFA.evidenceEnabled = false;
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty()) return msjFinal;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SYNC_P2P_CONTACTS.getId()));
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();	
		if(!msjFinal.isEmpty()) return msjFinal;
		Thread.sleep(5000);
		String codigoVerificacion = GetCodeGmail.getVerificationCode();
		System.out.println("El codigo es "+codigoVerificacion);
		AndroidElement el1 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_1"));
		el1.setValue(codigoVerificacion.substring(0,1));
		AndroidElement el2 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_2"));
		el2.setValue(codigoVerificacion.substring(1,2));
		AndroidElement el3 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_3"));
		el3.setValue(codigoVerificacion.substring(2, 3));
		AndroidElement el4 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_4"));
		el4.setValue(codigoVerificacion.substring(3, 4));
		AndroidElement el5 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_5"));
		el5.setValue(codigoVerificacion.substring(4, 5));
		AndroidElement el6 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_6"));
		el6.setValue(codigoVerificacion.substring(5, 6));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VALIDATE_CODE_BUTTON.getId()));
		EFA.evidenceEnabled = true;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString())){
			return msjFinal = "No se completo la sincronizacion";
		}
		return msjFinal;
	}
	
	public static String buscarPersonaP2P(String cliente) throws Exception{
		String msjFinal = "";
		Element txtBuscar = new Element("id", "search_view");
		Element rvcontaclist = new Element("id", "rv_channel");
		if(GlobalData.getData("viEnviarInvitacion").equals("SWIPE")){
			while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IV_INVITE.getId())).toString())){
				TouchAction a = new TouchAction(((AppiumDriver)EFA.cv_driver));
				a.press(589,352);
				a.moveTo(0, -35);
				a.waitAction(Duration.ofMillis(300));
				a.release();
				a.perform();	
			}
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IV_INVITE.getId()));
			EFA.cv_driver.findElement(By.xpath(EMoreElements.INVITAR_GMAIL.getValue())).click();
			Thread.sleep(5000);
			EFA.executeAction(Action.Back, null);
			EFA.cs_getTestEvidence("Envio Invitacion Gmail", 500);
			return msjFinal = "Envio Invitacion";
		}
		if(GlobalData.getData("viEnviarInvitacion").equals("VARIAS BUSQUEDAS")){
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IV_INVITE.getId()));
			EFA.cv_driver.findElement(By.xpath(EMoreElements.INVITAR_GMAIL.getValue())).click();
			Thread.sleep(3000);
			EFA.executeAction(Action.Back, null);
			EFA.cs_getTestEvidence("Envio Invitacion Gmail", 500);
			return msjFinal = "Envio Invitacion";
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_EMPTY_TEXT.getValue())).toString())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FAB_CONTACT.getId()));
			while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty())return msjFinal;
			EFA.executeAction(Action.Click, txtBuscar);
			Thread.sleep(2000);
			EFA.executeAction(Action.SendKeys,txtBuscar,cliente);
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTP2PNAME.getId()));
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, rvcontaclist).toString())){
			for(int i=0 ;i<5;i++){
				EFA.executeAction(Action.Click, txtBuscar);
				Thread.sleep(2000);
				EFA.executeAction(Action.SendKeys,txtBuscar,cliente);
					if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTP2PNAME.getId())).toString())){
						if(GlobalData.getData("viEnviarInvitacion").equals("BUSQUEDA")){
							EFA.executeAction(Action.Back, null);
							EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IV_INVITE.getId()));
							while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
							EFA.cv_driver.findElement(By.xpath(EMoreElements.INVITAR_GMAIL.getValue())).click();
							Thread.sleep(5000);
							EFA.executeAction(Action.Back, null);
							EFA.cs_getTestEvidence("Envio Invitacion Gmail", 500);
							return msjFinal = "Envio Invitacion";
						}
						String nombre = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTP2PNAME.getId())).toString();
						if(nombre.toUpperCase().equals(cliente.toUpperCase())){
							if(!GlobalData.getData("viVecesBusqueda").isEmpty()){
								EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTP2PNAME.getId()));
								EFA.cs_getTestEvidence("Pantalla Enviar Pago", 500);
								EFA.executeAction(Action.Back, null);
								while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
								msjFinal = Reutilizable.verificarMensajesError();
								EFA.cs_getTestEvidence("Pantalla Busqueda", 500);
							}
						EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTP2PNAME.getId()));
						return msjFinal;
						}
					}
			}
	}
		return msjFinal;
}

	public static String EnvioPagoP2P() throws Exception{
		String msjFinal = "";
		String montomax= "Has superado el monto m�ximo diario de transacciones.";
		if(!msjFinal.isEmpty()) return msjFinal;
		Thread.sleep(3000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PAY.getId()));
		if(GlobalData.getData("viPagoSinTarjeta").toUpperCase().equals("SI")){
			msjFinal = UtilPaymentCommerce.payNoCard();
			if(!msjFinal.isEmpty())return msjFinal;	
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()));
		EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()), GlobalData.getData("viMontoCobro"));
		EFA.executeAction(Action.Back, null);
		Element xTarjeta = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.widget.Spinner/android.widget.TextView");
		String Tarjeta = EFA.executeAction(Action.GetText,xTarjeta).toString().trim();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SPINNER_PAYMENT_METHOD.getId()));
		if(!GlobalData.getData("viElegirMetodoPago").isEmpty()){
			EFA.cs_getTestEvidence("Eleccion metodo de pago", 800);
			switch (GlobalData.getData("viElegirMetodoPago").toUpperCase()) {
			case "CREDITO":
				if(!UtilPaymentCommerce.EleccionTarjetaCredito()){
					return msjFinal = "no se realizo el cambio";
				}
				break;
			case "DEBITO":
				if(!UtilPaymentCommerce.EleccionTarjetaDebito(Tarjeta)){
					return msjFinal = "no se realizo el cambio";
				}
				break;
			default:
				break;
			}
		}else{
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.SELECT_DIALOG_LISTVIEW.getValue())).toString())){
				EFA.executeAction(Action.Back, null);
			}
		}
		EFA.cs_getTestEvidence("Panta Pago Persona", 800);
		if(GlobalData.getData("viAgregarDescripcion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DETAIL.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DETAIL.getId()),GlobalData.getData("viDescripcionPago"));
			EFA.executeAction(Action.Back, null);
		}
		Thread.sleep(6000);
		EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		if(!GlobalData.getData("viMontoCobro2").isEmpty() ){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()), GlobalData.getData("viMontoCobro2"));
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));	
			msjFinal = Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty()) return msjFinal;
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			return msjFinal;
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGVIEW.getValue())).toString())){
			msjFinal = Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		msjFinal = Reutilizable.verificarMensajesError();
		System.out.println(msjFinal);
		if(msjFinal.equals(montomax)){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.REJECT.getId()));
			return msjFinal;
		}
		if(msjFinal.isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE.getId())).toString())){
			if(GlobalData.getData("viValidarAhora").toUpperCase().equals("PAGO")){
				msjFinal = UtilPaymentCommerce.validateTarjeta();
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId())).toString())){
					msjFinal = "";
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId()));
					EFA.cs_getTestEvidence("pantalla pago p2p", 1500);
					EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
					msjFinal = Reutilizable.verificarMensajesError();
					System.out.println(msjFinal);
					if(!msjFinal.isEmpty())return msjFinal;
					EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
					return msjFinal;
				}
			}else{
				return msjFinal;
			}
		}
		if(!msjFinal.equals(Mensajes.UPS_GENEREAL)){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.REJECT.getId()));
			return msjFinal="";
		}else{
			return msjFinal;
		}
	}
	
	public static String envioCobroP2P(String monto) throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_COLLECT.getId()));
		EFA.executeAction(Action.Back, null);
		msjFinal = ValidacionTexto.textCollect();
		if(!msjFinal.isEmpty())return msjFinal;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXTMONTO.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXTMONTO.getId()),monto);
		EFA.executeAction(Action.Back, null);
		if(GlobalData.getData("viAgregarDescripcion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DETAIL.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DETAIL.getId()),GlobalData.getData("viDescripcionPago"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}

	public static String capturarIdChat(String montoCobro) throws Exception{ 
		String msjFinal = "";
			for(int i=1;i<4;i++){
			List<WebElement> lstMovimientosChat = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View"));															   
			for(WebElement webElement : lstMovimientosChat){
				Element btnPagar = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.Button");
				Element texto = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.TextView");
				Element texto1 = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.TextView[1]");
				Element texto2 = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.TextView[2]");
				Element texto3 = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.TextView[3]");
				boolean a = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, texto).toString());
				System.out.println(a);
				boolean b = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, texto1).toString());
				System.out.println(b);
				boolean c = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, texto2).toString());
				System.out.println(c);
				boolean d = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, texto3).toString());
				System.out.println(d);
				boolean present =validarIdChat(a,b,c,d);
				if(present){
				WebElement title = webElement.findElement(By.id("tv_title"));
				String xtitle = title.getText().trim();
				WebElement monto = webElement.findElement(By.id("tv_amount"));
				String xmonto = monto.getText().trim();
				System.out.println(xtitle);
				System.out.println(xmonto);
				if(xmonto.equals("S/ "+montoCobro) && Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnPagar).toString())){
					EFA.executeAction(Action.Click, btnPagar);
				}
			}
			}
			}
	return msjFinal ;
	}
	
	public static boolean validarIdChat(boolean a,boolean b,boolean c,boolean d) throws Exception{
		if(!a || !b || !c || !d){
			return false;
		}
		return true;
	}
	
	public static void hacerSwipeChat(){
		TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		  x.press(611,232).moveTo(0,778).release().perform();
	}
	
	public static String irAPagar() throws Exception{
		String msjFinal = "";
		EFA.evidenceEnabled = false;
		msjFinal=ValidacionTexto.textPay();
		if(!msjFinal.isEmpty())return msjFinal;
		EFA.evidenceEnabled = true;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()));
		if(!GlobalData.getData("viTransferPago2").isEmpty()){
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()),GlobalData.getData("viTransferPago2"));
		}
		EFA.executeAction(Action.Back, null);
		if(GlobalData.getData("viAgregarDescripcion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DETAIL.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DETAIL.getId()),GlobalData.getData("viDescripcionPago"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return msjFinal;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		return msjFinal;
	}
	
	public static boolean CapturarTextoChat(String Id) {
		String tempTexto = "";
		List<WebElement> listaIdTituloChat = EFA.cv_driver.findElementsById(Id);
		for (WebElement webElement : listaIdTituloChat) {
			tempTexto=webElement.getText().trim();
			if(GlobalData.getData("viCobrarP2P").equals("SI") && tempTexto.equals(Textos.ENVIASTE_UN_COBRO)){
				System.out.println(tempTexto);
				return true;
			}
			if(GlobalData.getData("viPagarP2P").equals("SI") && tempTexto.equals(Textos.ENVIASTE_UN_PAGO)){
				System.out.println(tempTexto);
				return true;
			}
			
		}
		return false;
	}
	
	public static boolean CapturarMontoChat(String monto){
		String tempMont= "";
		List<WebElement> listaIdTituloChat = EFA.cv_driver.findElementsById("tv_amount");
		for (WebElement webElement : listaIdTituloChat) {
			tempMont=webElement.getText().trim();
			if(GlobalData.getData("viCobrarP2P").equals("SI") && tempMont.equals("S/ "+monto)){
				System.out.println(tempMont);
				return true;
			}
			if(GlobalData.getData("viPagarP2P").equals("SI") && tempMont.equals("- S/ "+monto)){
				System.out.println(tempMont);
				return true;
			}
		}
		return false;
	}
	
	public static boolean CapturarHistorialMovimiento(String Nombre,String Monto){
		for(int i=1;i<7;i++){
		List<WebElement> lstMovimientosChat = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.view.View["+i+"]"));															   
		for(WebElement webElement : lstMovimientosChat){
			
		 }
		}
		return false;
	}
	
	public static boolean CapturarMensajeMonto(String Id,String Texto){
		List<WebElement> listaMensaje = EFA.cv_driver.findElementsById(Id);
		for (WebElement webElement : listaMensaje){
			String monto = webElement.getText().trim();
			System.out.println(monto);
			if(monto.equals(Texto)){
				return true;
			}
		}
		return false;
	}

	public static boolean CapturarMensajeTexto(String Id,String Texto){
		List<WebElement> listaMensaje = EFA.cv_driver.findElementsById(Id);
		for (WebElement webElement : listaMensaje){
			String xtexto = webElement.getText().trim();
			System.out.println(xtexto);
			if(xtexto.equals(Texto)){
				return true;
			}
		}
		return false;
	}
	
	public static boolean capturarRespuestaChat(String textoChat,String montoCobro) throws Exception{ 
			for(int i=1;i<6;i++){
			List<WebElement> lstMovimientosChat = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View"));															   
			for(WebElement webElement : lstMovimientosChat){
				Element texto = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.TextView");
				Element texto1 = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.TextView[1]");
				Element texto2 = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.TextView[2]");
				Element texto3 = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.LinearLayout["+i+"]/android.view.View/android.widget.TextView[3]");
				boolean a = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, texto).toString());
				boolean b = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, texto1).toString());
				boolean c = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, texto2).toString());
				boolean d = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, texto3).toString());
				boolean present =validarIdChat(a,b,c,d);
				if(present){
				WebElement title = webElement.findElement(By.id("tv_title"));
				String xtitle = title.getText().trim();
				WebElement monto = webElement.findElement(By.id("tv_amount"));
				String xmonto = monto.getText().trim();
				System.out.println(xtitle);
				System.out.println(xmonto);
				System.out.println(textoChat);
				System.out.println(montoCobro);
				System.out.println("--------------------");
				if(xmonto.equals(montoCobro) && xtitle.equals(textoChat)){
					return true;
				}
			}
			}
			}
	return false ;
	}
	
	public static boolean validarContacto(String persona,String numero) throws Exception{
		List<WebElement> lstContactos = EFA.cv_driver.findElementsById("rv_channel");
		for(WebElement ycontacto : lstContactos){
				if(!GlobalData.getData("viPersonaNumero").isEmpty()){
					List<WebElement> webElement = ycontacto.findElements(By.id("txt_p2p_phone_number"));
					for(WebElement xcontacto : webElement){
						String contacto = xcontacto.getText().trim().toUpperCase();
						System.out.println(contacto);
						if(contacto.equals(numero.toUpperCase())){
							xcontacto.click();
							return true;
						}
					}
				}else{
					List<WebElement> webElement = ycontacto.findElements(By.id("txt_p2p_name"));
					for(WebElement xcontacto : webElement){
						String contacto = xcontacto.getText().trim().toUpperCase();
						System.out.println(contacto);
						if(contacto.equals(persona.toUpperCase())){
							xcontacto.click();
							return true;
						}
					}
				}

		}
		return false;
	}
}
