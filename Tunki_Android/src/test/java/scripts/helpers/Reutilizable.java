package scripts.helpers;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;
import com.everis.PropertyReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.persona.GestionCuenta;


public class Reutilizable {
	
	public static DesiredCapabilities getCapabilities() {
		PropertyReader reader = new PropertyReader();
		reader.getProperties(new File("data").getAbsolutePath() + "/capabilities.properties");
		
		DesiredCapabilities capabilities = DesiredCapabilities.android();
    	capabilities.setCapability("platformName", GlobalData.getData("PlatformName"));
        capabilities.setCapability("device", GlobalData.getData("Device"));
        capabilities.setCapability("deviceName", GlobalData.getData("DeviceName"));
        capabilities.setCapability("udid", GlobalData.getData("DeviceID"));
        capabilities.setCapability("platformVersion", GlobalData.getData("PlatformVersion"));
        capabilities.setCapability("appPackage", GlobalData.getData("AppPackage"));
        capabilities.setCapability("appActivity", GlobalData.getData("AppActivity"));
        capabilities.setCapability("autoGrantPermissions", true);
        capabilities.setCapability("--session-override", true);
        capabilities.setCapability("noReset", true);
        if(!reader.getProperty("newCommandTimeout").equals("0"))
        capabilities.setCapability("newCommandTimeout", Integer.parseInt(reader.getProperty("newCommandTimeout")));
        return capabilities;
	}
	
	public static String verificarMensajesError() throws Exception{
		String msjFinal = "";
		Element lblmessage = new Element("id","txt_message");
		Element lblinputError = new Element("id","textinput_error");
		Element lbldetail = new Element("id","tv_detail");
		Element btnAceptar = new Element("id","btn_accept");
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lblinputError).toString())){
			msjFinal = EFA.executeAction(Action.GetText, lblinputError).toString();
			return msjFinal;
		}
			
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lblmessage).toString())){
			msjFinal = EFA.executeAction(Action.GetText, lblmessage).toString();
			msjFinal = msjFinal.replace("\n", " ");
			EFA.executeAction(Action.Click, btnAceptar);
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Retorno Pantalla", 0);
			return msjFinal;
		}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lbldetail).toString())){
			msjFinal = EFA.executeAction(Action.GetText, lbldetail).toString();
			msjFinal = msjFinal.replace("\n", " ");
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Pop Up", 0);
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId())).toString())){
				msjFinal = "";
				return msjFinal;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.POR_AHORA_NO.getId())).toString())){
				msjFinal = "";
				return msjFinal;
			}
			return msjFinal;
		}
		return msjFinal;
	}
	
	public static Element newElement(String identifier, String element){
		return new Element(identifier,element);
	}
	
	public static void tomarCapturaPantalla(long milisegundos, String nombreCaptura) throws InterruptedException {
		Thread.sleep(milisegundos);
		EFA.cs_getTestEvidence(nombreCaptura, 0);
	}

	public static boolean agregarImagen() throws Exception {
		String msjFinal = "";
		Element btnImagenes = new Element("xpath", "//android.widget.TextView[@text='Im�genes']");
		Element crop = new Element("id", "crop_image_menu_crop");
		Element foto = new Element("xpath", "//android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.GridView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView");
		Element img = new Element("xpath","	//android.widget.ImageButton[@content-desc='M�s opciones']");

		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnImagenes).toString())){
			EFA.executeAction(Action.Click, img);
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, foto).toString())){
				EFA.executeAction(Action.Click, foto);
		}else{
			msjFinal = "No hay ninguna foto";
			return false;
		}

		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, crop).toString())){
			EFA.executeAction(Action.Click, crop);
		}
		else{
			msjFinal = "No se encontro el boton aceptar";
			return false;
		}
		return true;
	}
	
	public static boolean NavegacionGestionTarjetas() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click,newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
		tomarCapturaPantalla(300, "Menu Mas");
		EFA.executeAction(Action.Click,newElement(EComponents.ID.getValue(), EButtons.MISTARJETAS.getId()));
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = verificarMensajesError();	
		if(!msjFinal.isEmpty()) return false;
		return true;
	}
	
	public static boolean CerrarSesion() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click,newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
		tomarCapturaPantalla(300, "Menu Mas");
		EFA.executeAction(Action.Click,newElement(EComponents.ID.getValue(), EButtons.SIGNOUT.getId()));
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = verificarMensajesError();	
		if(!msjFinal.isEmpty()) return false;
		tomarCapturaPantalla(300, "Login");
		EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
		EFA.executeAction(Action.SendKey, newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()),GlobalData.getData("viPassword"));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getId()));		
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = verificarMensajesError();
		tomarCapturaPantalla(300, "Pop Up");
		if(!msjFinal.isEmpty()) return false;
		return true;
	}
		
	public static boolean validarHistorialPersona(String persona) throws Exception{
		String msjFinal = "";
		for (int i=0;i<7;i++){
			List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.view.View["+i+"]"));															   
		if(lstMovimientos.size() > 0){
			for (WebElement ymovimiento : lstMovimientos){
			WebElement nombre = ymovimiento.findElement(By.id("tv_full_name"));
			String xcliente = nombre.getText().trim().toUpperCase();
			System.out.println(xcliente);
			if(xcliente.equals(persona)){
				ymovimiento.click();
			}
		}			
		Element back = new Element("xpath", "//*[@class='android.widget.ImageButton'][1]");
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, back).toString())){
			Element lblTiempoPago = new Element ("id","txt_date_time");
			Element lblMontoPago = new Element ("id","tv_total");
			tomarCapturaPantalla(500, "Movimiento");
			return true;
		}
		else{
			msjFinal = "No se muestra el movimiento";
			tomarCapturaPantalla(500, "No se muestra el movimiento");
			return false;
		}
	  }
	}
		return false;
	}
	
	public static String obtenerCodigoVerificacionAlfa() throws Exception {
		String codigo = "";
		String primerDato = "";

		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i < 1; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "(?=.*[0-9])(?=.*[a-zA-Z]).{6,}";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
		
	public static boolean CapturarTextoPorId(String Id, String Texto) {
		List<WebElement> listaElementosporId = EFA.cv_driver.findElementsById(Id);
		for (WebElement webElement : listaElementosporId) {
			String tempTexto = webElement.getText();
			System.out.println(tempTexto);
			if(tempTexto.equals(Texto)) {
				webElement.click();
				return true;
			}
		}
		return false;
	}
	
	public static boolean CapturarTextoGaleria(String Id, String Texto) {
		List<WebElement> listaElementosporId = EFA.cv_driver.findElementsById(Id);
		for (WebElement webElement : listaElementosporId) {
			String tempTexto = webElement.getText();
			System.out.println(tempTexto);
			if(tempTexto.equals(Texto)) {
				webElement.click();
				return true;
			}
		}
		return false;
	}
	
	
	public static String selfiePhoto() throws Exception{
		String msjFinal = "";
		Element review_cancel = new Element("id","com.motorola.camera:id/review_cancel");
		Element aprove = new Element("id","com.motorola.camera:id/review_approve");
		Element crop = new Element("id","crop_image_menu_crop");
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_TAKE_PICTURE.getId())).toString())){
			EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.BTN_TAKE_PICTURE.getId()));
		}
		TouchAction x = new TouchAction((PerformsTouchActions)EFA.cv_driver);
		x.tap(343,608).perform();
		Thread.sleep(5000);
		if(GlobalData.getData("viFlujoEditar").equals("CANCELAR FOTO")){
			EFA.executeAction(Action.Click, review_cancel);
			EFA.executeAction(Action.Back, null);
			return msjFinal;
		}
		EFA.executeAction(Action.Click, aprove);
		Thread.sleep(2000);
		EFA.executeAction(Action.Click, crop);
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String galleryPhoto() throws Exception{
		String msjFinal = "";
		Element crop = new Element("id", "crop_image_menu_crop");
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OPEN_GALLERY.getId())).toString())){
			EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.BTN_OPEN_GALLERY.getId()));	
		}
		Thread.sleep(4000);
		for (int i = 0; i <2; i++) {
			if(!CapturarTextoGaleria("title",GlobalData.getData("viNombreFoto2"))){
				EFA.cs_getTestEvidence("Deslizar", 500);
				swiptToBottom();
				Thread.sleep(2000);
			}else{
			    break;
			}
		}
		for (int i = 0; i <2; i++) {
			if(!CapturarTextoGaleria("title",GlobalData.getData("viNombreFoto"))){
				EFA.cs_getTestEvidence("Deslizar", 500);
				swiptToBottom();
				Thread.sleep(2000);
			}else{
			    break;
			}
		}
		if(GlobalData.getData("viFlujoEditar").equals("CANCELAR FOTO")){
			Thread.sleep(4000);
			EFA.cs_getTestEvidence("Galeria", 1000);
			EFA.executeAction(Action.Back, null);
			Thread.sleep(800);
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, crop).toString())){
		EFA.executeAction(Action.Click, crop);
		EFA.cs_getTestEvidence("Cambio foto", 500);
		}else{
		return msjFinal = "No se encontro el boton aceptar";
		}
		return msjFinal="";	
	}
	
	public static void swiptToBottom() throws Exception{
		Dimension dim = EFA.cv_driver.manage().window().getSize();
		int height = dim.getHeight();
		int width = dim.getWidth();
		int x = width/2;
		
		int top_y = (int)(height*0.80);
		int bottom_y = (int)(height*0.30);
		
		//System.out.println(x+"   "+top_y+"  "+bottom_y);
		TouchAction s = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		s.press(x, top_y).waitAction(Duration.ofMillis(4000)).moveTo(x, bottom_y).release().perform();
		EFA.cs_getTestEvidence("Deslizar", 500);
	}
	
	public static String navegarMenuGestionCuentas() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
		Reutilizable.tomarCapturaPantalla(500, "Men� M�s");
		EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.ITEM_ACCOUNT_MANAGMENT.getValue()));
		Reutilizable.tomarCapturaPantalla(500, "Gesti�n de Cuentas");
		
		return msjFinal;
	}
	
	public static void swipeDown(int a,int b,int c,int d){
		TouchAction swipe = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		swipe.press(a,b).moveTo(c,d).release().perform();
	}

}
