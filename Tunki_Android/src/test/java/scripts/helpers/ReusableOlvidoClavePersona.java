package scripts.helpers;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import script.textosTunki.Textos;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class ReusableOlvidoClavePersona {
	
	public static String validarPopUpValidaCorreo() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.VALIDA_TU_CORREO.toUpperCase())){
			return msjFinal = "Texto: "+Textos.VALIDA_TU_CORREO+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_DETAIL.getValue())).toString().trim().toUpperCase().equals(Textos.DETALLE_VALIDA_TU_CORREO.toUpperCase())){
			return msjFinal = "Texto: "+Textos.DETALLE_VALIDA_TU_CORREO+" diferente";
		}
		return msjFinal;
	}
	
	public static String validarPopUpRestablecerPassword() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.RESTABLECE_TU_CONTRASEŅA.toUpperCase())){
			return msjFinal = "Texto: "+Textos.RESTABLECE_TU_CONTRASEŅA+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_DETAIL.getValue())).toString().trim().toUpperCase().equals(Textos.DESCRIPCIO_RESTABLECE_TU_CONTRASEŅA.toUpperCase())){
			return msjFinal = "Texto: "+Textos.DESCRIPCIO_RESTABLECE_TU_CONTRASEŅA+" diferente";
		}
		return msjFinal;
	}
	
	public static String validarPantallaCreaPassword() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_TOOLBAR_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.CREA_TU_CONTRASEŅA.toUpperCase())){
			return msjFinal = "Texto: "+Textos.CREA_TU_CONTRASEŅA+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_PASSWORD_INSTRUCTION.getValue())).toString().trim().toUpperCase().equals(Textos.DEBE_TENER_6_DIGITOS.toUpperCase())){
			return msjFinal = "Texto: "+Textos.DEBE_TENER_6_DIGITOS+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.BOTTOM_TEXT.getValue())).toString().trim().toUpperCase().equals(Textos.COMUNICATE_CON_NOSOTROS_CAMBIO_CLAVE.toUpperCase())){
			return msjFinal = "Texto: "+Textos.COMUNICATE_CON_NOSOTROS_CAMBIO_CLAVE+" diferente";
		}
		return msjFinal;
	}
	
	public static String validarPantallaPassCreada() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.CONTRASENA_CREADA.toUpperCase())){
			return msjFinal = "Texto: "+Textos.CONTRASENA_CREADA+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_DESCRIPTION.getValue())).toString().trim().toUpperCase().equals(Textos.CONTRASENA_CREADA_CORRECTAMENTE.toUpperCase())){
			return msjFinal = "Texto: "+Textos.CONTRASENA_CREADA_CORRECTAMENTE+" diferente";
		}
		return msjFinal;
	}
	
	public static String ingresarCodigo() throws Exception {
		String msjFinal = "";
		if(!GlobalData.getData("viCodigoVerificacion_2").isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()),GlobalData.getData("viCodigoVerificacion_2"));
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty())return msjFinal;	
		}
		if(GlobalData.getData("viFlujosRecuperarPass").toUpperCase().equals("BACK VALIDA CORREO")){
			EFA.executeAction(Action.Back, null);
			Thread.sleep(800);
			EFA.executeAction(Action.Back, null);
			Thread.sleep(800);
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Element botonLogin = new Element ("id","button_0");
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,botonLogin).toString())){
				return msjFinal = "No muestra pantalla de login";
			} 
			return msjFinal = "Flujo Correcto";
		}
		String codigoVerificacion = obtenerCodigoVerificacion();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()),codigoVerificacion);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return msjFinal;		
		return msjFinal;
	}
	
	public static String obtenerCodigoVerificacion() throws Exception {
		String codigo = "";
		String primerDato = "";

		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i <1; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "[0-9][0-9][0-9][0-9][0-9][0-9]";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
	
}