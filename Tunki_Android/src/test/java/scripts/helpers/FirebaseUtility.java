package scripts.helpers;

import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.BasicConfigurator;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseUtility {
	private FirebaseUtility() {
		throw new UnsupportedOperationException();
	}

	public static void updateCode(String phone) {
		BasicConfigurator.configure();
		try (FileInputStream serviceAccount = new FileInputStream("tunki-dev-firebase-adminsdk.json")) {

			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://tunki-dev.firebaseio.com").build();

			FirebaseApp.initializeApp(options);
			final FirebaseDatabase database = FirebaseDatabase.getInstance();
			final DatabaseReference ref2 = database.getReference("automation/phone-validate-code");
			ChildEventListener childEventListener = new ChildEventListener() {

				@Override
				public void onChildRemoved(DataSnapshot dataSnapshot) {
					System.out.println("Se elimino el nodo.");
				}

				@Override
				public void onChildMoved(DataSnapshot dataSnapshot, String arg1) {
					System.out.println("Se movio el nodo.");
				}

				@Override
				public void onChildChanged(DataSnapshot dataSnapshot, String previousChild) {
					System.out.println("Se modifico el nodo.");
				}

				@Override
				public void onChildAdded(DataSnapshot dataSnapshot, String previousChild) {
					System.out.println("Se hizo la actualizacion de codigo.");
				}

				@Override
				public void onCancelled(DatabaseError e) {
					System.err.println("Ocurrio un error durante la ejecucion:" + e.getMessage());
				}
			};
			DatabaseReference ref = database.getReference("automation/phone-validate-code/" + phone);
			ApiFuture<Void> apiFuture = ref.setValueAsync("STARTED");
			apiFuture.get(10, TimeUnit.SECONDS);
			ref2.orderByValue().equalTo("UPDATED").addChildEventListener(childEventListener);
			Thread.sleep(5000);
			ref2.orderByValue().equalTo("UPDATED").removeEventListener(childEventListener);
		} catch (Exception e) {
			System.err.println("Ocurrio un error durante la ejecucion:" + e.getMessage());
		}
	}

	public static void stopListener(DatabaseReference ref, ChildEventListener listener) {
		ref.removeEventListener(listener);
	}

}
