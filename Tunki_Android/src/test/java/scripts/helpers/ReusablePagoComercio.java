package scripts.helpers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import script.textosTunki.ValidacionTexto;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class ReusablePagoComercio {
	
	public static String pagoComercio() throws Exception{
		String msjFinal = "";
		Element btnPorcentajePropina = null;
		int tries = 0;
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGESPERA.getValue())).toString())){
			if(tries>6){
				return msjFinal = "No se obtuvo ninguna solicitud de cobro";
			}
			Thread.sleep(15000);
			tries++;
		}
		String propina = GlobalData.getData("viPropina");
		boolean bTxtPropina = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId())).toString());
		
		if(!propina.isEmpty()){
			if(!bTxtPropina){
				return msjFinal = "La propina no esta habilitada para este comercio";
			}
			if(propina.contains("%")){
				propina = propina.replace("%", "");
				btnPorcentajePropina = new Element("id", "btn_pct_" + propina);
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnPorcentajePropina).toString())){
					EFA.executeAction(Action.Click, btnPorcentajePropina);
				}
				else{
					return msjFinal = "El porcentaje de propina indicado no es v�lido";
				}
			}
			else{				
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId()), propina);
				EFA.executeAction(Action.Back, null);
			}
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGVIEW.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.lblTITULO.getValue())).toString();
			if(!msjFinal.equals(Mensajes.PAGOREALIZADO))return msjFinal;				
		}
		EFA.evidenceEnabled = false;
		msjFinal = ValidacionTexto.payConstancyP2C();
		if(!msjFinal.isEmpty())return msjFinal;
		EFA.evidenceEnabled = true;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		if(GlobalData.getData("viValidarMovimiento").toUpperCase().equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.verificarMensajesError();
			if(!msjFinal.equals("")) return msjFinal;
			validarComercioHistorial(GlobalData.getData("viComercio").toUpperCase());
			EFA.evidenceEnabled = false;
			msjFinal = ValidacionTexto.payConstancyP2C();
			if(!msjFinal.isEmpty())return msjFinal;
			EFA.evidenceEnabled = true;
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		}
		return msjFinal;
	}
	
	public static String validarComercioHistorial(String comercio) throws Exception{
		String msjFinal = "";
		for (int i=0;i<7;i++){
			List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.view.View["+i+"]"));															   
		if(lstMovimientos.size() > 0){
			for (WebElement ymovimiento : lstMovimientos){
			WebElement nombre = ymovimiento.findElement(By.id("tv_full_name"));
			String xcliente = nombre.getText().trim().toUpperCase();
			System.out.println(xcliente);
			if(xcliente.equals(comercio)){
				lstMovimientos.get(0).click();
			}
		}
		Element back = new Element("xpath", "//*[@class='android.widget.ImageButton'][1]");
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, back).toString())){
			Element lblTiempoPago = new Element ("id","txt_date_time");
			Element lblMontoPago = new Element ("id","tv_total");
			GlobalData.setData("vOutTiempoPago",EFA.executeAction(Action.GetText, lblTiempoPago).toString());
			GlobalData.setData("vOutMontoPago",EFA.executeAction(Action.GetText, lblMontoPago).toString());
			Reutilizable.tomarCapturaPantalla(500, "Movimiento");
			return msjFinal;
		}else{
			Reutilizable.tomarCapturaPantalla(500, "No se muestra el movimiento");
			return msjFinal = "No se muestra el movimiento";
		}
		}
		}	
		return msjFinal;
	}
	
	public static String pagoComercioSinTarjeta() throws Exception{
		String msjFinal = "";
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD.getId()));
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId())).toString())){
				msjFinal = "No muestra pantalla Vincula tu tarjeta de debito";
			}
			msjFinal = ReusableRegistroPersona.AgregarTarjeta();
			if(!msjFinal.isEmpty())return msjFinal;
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NAME.getId()));
			msjFinal = ReusablePagoComercio.pagoComercio();
			if(!msjFinal.isEmpty())return msjFinal;
		return msjFinal = "";
	}
}
