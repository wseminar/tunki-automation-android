package scripts.helpers;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.Util.UtilPaymentCommerce;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class ReusablePagoServicios {
	public static String pagoRecargaCelulares() throws Exception{

		String msjFinal = "";
		Element optTipoOperador = new Element("xpath", "//android.widget.TextView[@text='" + GlobalData.getData("viOperador") + "']");
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.SERVICES.getId()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_RECHARGE.getId()));
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.SELECT_DIALOG_LISTVIEW.getValue())).toString())){
			return msjFinal = "No muestra listado de operadores";
		}
		EFA.executeAction(Action.Click, optTipoOperador);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PHONE.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PHONE.getId()),GlobalData.getData("viCelular"));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()));
		Thread.sleep(2000);
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMONTO.getId()),GlobalData.getData("viMontoRecargar"));
		EFA.executeAction(Action.Back, null);
		Element xTarjeta = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.widget.Spinner[2]/android.widget.TextView");
		String Tarjeta = EFA.executeAction(Action.GetText,xTarjeta).toString().trim();
		System.out.println(Tarjeta);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SPINNER_PAYMENT_METHOD.getId()));
		if(!GlobalData.getData("viElegirMetodoPago").isEmpty()){
			EFA.cs_getTestEvidence("Eleccion metodo de pago", 800);
			switch (GlobalData.getData("viElegirMetodoPago").toUpperCase()) {
			case "CREDITO":
				if(!UtilPaymentCommerce.EleccionTarjetaCredito()){
					return msjFinal = "no se realizo el cambio";
				}
				break;
			case "DEBITO":
				if(!UtilPaymentCommerce.EleccionTarjetaDebito(Tarjeta)){
					return msjFinal = "no se realizo el cambio";
				}
				break;
			case "NO ESCOGER":
				EFA.executeAction(Action.Back, null);
				break;
			default:
				break;
			}
		}
		EFA.cs_getTestEvidence("Pantalla Pago Recargas", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		msjFinal = Reutilizable.verificarMensajesError();
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));	
		msjFinal = Reutilizable.verificarMensajesError();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		return msjFinal;
	}
}
