package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class Logout {
	private static String msjFinal = "";
	
	private static Element progress = new Element("xpath", "//*[@class='android.widget.ProgressBar']");	
	
	public static void logout() throws Exception{

		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.SIGNOUT.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, progress).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		
	}
	
	public static String getResult() throws Exception{
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}

}
