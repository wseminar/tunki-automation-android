package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import script.textosTunki.Textos;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePxP;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class CobroChat {
	public static String msjFinal = "";
	public static void cobroChat() throws Exception{
		msjFinal = "";
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return;
		EFA.cs_getTestEvidence("Lista de Comercio", 500);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CONTACT.getId()));
		Thread.sleep(3000);
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SYNC_P2P_CONTACTS.getId())).toString())){
			 msjFinal = ReusablePxP.synchronizeContacts();
			 if(!msjFinal.isEmpty())return;
		}
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();	
		if(!msjFinal.isEmpty()) return;
		Thread.sleep(1000);
		msjFinal = ReusablePxP.buscarPersonaP2P(GlobalData.getData("viPersonaCobrar"));
		if(!msjFinal.isEmpty()) return;
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = ReusablePxP.envioCobroP2P(GlobalData.getData("viMontoCobro"));
		if(!msjFinal.isEmpty()) return;
		boolean texto = ReusablePxP.CapturarTextoChat("tv_title");
		boolean monto = ReusablePxP.CapturarMontoChat(GlobalData.getData("viMontoCobro"));
		if(!texto || !monto){
			 msjFinal = "no se encontro la ultima operacion";
			 return;
		}
		if(!GlobalData.getData("viMontoCobro2").isEmpty()){
			msjFinal = ReusablePxP.envioCobroP2P(GlobalData.getData("viMontoCobro2"));
			if(!msjFinal.isEmpty()) return;
			boolean xtexto = ReusablePxP.CapturarTextoChat("tv_title");
			boolean xmonto = ReusablePxP.CapturarMontoChat(GlobalData.getData("viMontoCobro2"));
			if(!xtexto || !xmonto){
				msjFinal = "no se encontro la ultima operacion";
				return;
			}
			int tries = 0;	   
			while(!ReusablePxP.capturarRespuestaChat(Textos.RECIBISTE_UN_PAGO,"S/ "+GlobalData.getData("viMontoCobro2"))){
				if(GlobalData.getData("viValidarMovimiento").equals("NO")){		
					break;
				}
				if(tries>6)
				{
					msjFinal = "No se obtuvo ningun pago";
					return;
				}
				Thread.sleep(10000);
				tries++;
			}
			
		}
		String amount = GlobalData.getData("viTransferPago");
		if(!GlobalData.getData("viTransferPago2").isEmpty()){
			amount = GlobalData.getData("viTransferPago2");
		}
		int tries = 0;
		while(!ReusablePxP.capturarRespuestaChat(Textos.RECIBISTE_UN_PAGO,"S/ "+amount)){
			if(GlobalData.getData("viValidarMovimiento").equals("NO")){		
				break;
			}
			if(tries>6)
			{
				msjFinal = "No se obtuvo ningun pago";
				return;
			}
			Thread.sleep(10000);
			tries++;
		}
		EFA.executeAction(Action.Back, null);
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.equals("")) return;	
		if(GlobalData.getData("viValidarMovimiento").equals("SI")){		
			Reutilizable.validarHistorialPersona(GlobalData.getData("viPersonaCobrar").toUpperCase());
		}

	}

	public static String getResult() throws Exception {
		String mensajeEsperado = GlobalData.getData("viMensajeErrorCobro");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}else{
				return "El mensaje no es igual: " + msjFinal;
			}
		}
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}
}
