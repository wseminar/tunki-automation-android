package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.Util.UtilLoginCustomer;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusableOlvidoClavePersona;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class OlvidoPasswordPersona {
	private static String msjFinal = "";
	private static Element optTipoDoc = new Element("xpath", "//android.widget.CheckedTextView[@text='"+GlobalData.getData("viTipoDocumento")+"']");
	
	public static void PassPersona() throws Exception{
		msjFinal = "";
		optTipoDoc = new Element("xpath", "//android.widget.CheckedTextView[@text='"+GlobalData.getData("viTipoDocumento")+"']");
		
		msjFinal = UtilLoginCustomer.loadingScreenLogin();
		if(!msjFinal.isEmpty())return;
		msjFinal = UtilLoginCustomer.enterYourUserName();
		if(!msjFinal.isEmpty())return;
		
		if(GlobalData.getData("viRealizarLogin").equals("SI")){
			if(GlobalData.getData("viBloquearUsuario").equals("SI")){
				for(int i = 0 ; i<4;i++){
					int n = 0;
					for(int j = 0 ; j<GlobalData.getData("viPassword_2").length();j++){
						UtilLoginCustomer.captureTextLogin(GlobalData.getData("viPassword_2").substring(n,n+1));
						n++;
						EFA.cs_getTestEvidence("Login", 500);
					}
					if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BTN_RIGHT.getId())).toString())){
						msjFinal = ReusableOlvidoClavePersona.validarPopUpRestablecerPassword();
						if(!msjFinal.isEmpty())return;
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_RIGHT.getId()));
						break;
					}
				}
			}
			if(GlobalData.getData("viPassword").length()<6){
				int n = 0;
				for(int i = 0 ; i<GlobalData.getData("viPassword").length();i++){
					UtilLoginCustomer.captureTextLogin(GlobalData.getData("viPassword").substring(n,n+1));
					n++;
					EFA.cs_getTestEvidence("Login", 500);
				}
			}
		}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RECOVER_PASSWORD.getId())).toString())){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RECOVER_PASSWORD.getId()));;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.SPINNER_DOCUMENT.getValue()));
		EFA.executeAction(Action.Click, optTipoDoc);
		if(!GlobalData.getData("viDocIdentidad_2").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_DOCUMENT.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DOCUMENT.getId()),GlobalData.getData("viDocIdentidad_2"));
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SEND.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.verificarMensajesError();
			if(msjFinal.equals(Mensajes.UPS_GENEREAL)){
				return;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId())).toString())){
				EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
			}
		}
		if(!GlobalData.getData("viDocIdentidad").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DOCUMENT.getId()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DOCUMENT.getId()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_DOCUMENT.getId()),GlobalData.getData("viDocIdentidad"));			
		}
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SEND.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_RESEND.getId())).toString())){
			msjFinal = "No muestra pop up Valida tu Correo";
			return;
		}
		msjFinal = ReusableOlvidoClavePersona.validarPopUpValidaCorreo();
		if(!msjFinal.isEmpty())return;
		Thread.sleep(80000);
		msjFinal = ReusableOlvidoClavePersona.ingresarCodigo();
		if(!msjFinal.isEmpty())return;
		
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getId())).toString())){
			msjFinal = "No muestra pantalla Crea tu Conntraseņa";
		}
		msjFinal = ReusableOlvidoClavePersona.validarPantallaCreaPassword();
		if(!msjFinal.isEmpty())return;
		if(!GlobalData.getData("viPass1").isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()),GlobalData.getData("viPass1"));
			EFA.executeAction(Action.Back, null);
		}
		if(!GlobalData.getData("viPass2").isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getId()),GlobalData.getData("viPass2"));
			EFA.executeAction(Action.Back, null);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONFIRM.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId())).toString())){
			msjFinal = "No muestra pantalla Contraseņa Creada";
			return;
		}
		msjFinal = ReusableOlvidoClavePersona.validarPantallaPassCreada();
		if(!msjFinal.isEmpty())return;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.COMMERCE.getId())).toString())){
			msjFinal = "No muestra listado de Comercio";
			return;
		}
		EFA.cs_getTestEvidence("Listado de Comercio", 800);
		if(GlobalData.getData("viCerrarSesion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SIGNOUT.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty())return;
			EFA.cs_getTestEvidence("Pantalla Login",800);
		}
		if(GlobalData.getData("viFlujosRecuperarPass").toUpperCase().equals("CREDENCIALES INVALIDAS")){
			int n = 0;
			for(int i = 0 ; i<GlobalData.getData("viPassword_2").length();i++){
				UtilLoginCustomer.captureTextLogin(GlobalData.getData("viPassword_2").substring(n,n+1));
				EFA.cs_getTestEvidence("Login", 500);
				n++;
			}
			Thread.sleep(1000);
			EFA.cs_getTestEvidence("Error", 800);
			while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BUTTON_0.getId())).toString()));	
		}
		if(GlobalData.getData("viReLogin").equals("SI")){
			int n = 0;
			for(int i = 0 ; i<GlobalData.getData("viPassword").length();i++){
				UtilLoginCustomer.captureTextLogin(GlobalData.getData("viPassword").substring(n,n+1));
				EFA.cs_getTestEvidence("Login", 500);
				n++;
			}
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty()) return;
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CONTACT.getId())).toString())){
				msjFinal = "No muestra listado de comercio";
				return;
			}
		}
		
	}
	
	public static String getResult(){
		String mensajeEsperado = GlobalData.getData("viMensajeErrorPass");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}else{
				return "El mensaje no es igual: " + msjFinal;
			}
		}
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}	
}
