package scripts.persona;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePagoServicios;
import scripts.helpers.Reutilizable;

public class RecargaCelulares {
	private static String msjFinal = "";
	private static Element optTipoOperador = new Element("xpath", "//android.widget.CheckedTextView[@text='"+ GlobalData.getData("viOperador")+"']");
	
	public static void recarga() throws Exception{
		optTipoOperador = new Element("xpath", "//android.widget.CheckedTextView[@text='" + GlobalData.getData("viOperador") + "']");
		msjFinal = "";
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMENSAJERROR.getId())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMENSAJERROR.getId())).toString();
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			return;
		}
		msjFinal = ReusablePagoServicios.pagoRecargaCelulares();
		if(!msjFinal.isEmpty()) return;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty()) return;
		EFA.cs_getTestEvidence("Lista de Movimientos", 800);
		Element lista = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.view.View[1]");
		EFA.executeAction(Action.Click, lista);
		EFA.cs_getTestEvidence("Constancia", 800);
	}
	
	public static String getResult() throws Exception{
		
		if(!GlobalData.getData("viMensajeErrorRecargas").isEmpty()){
			if(GlobalData.getData("viMensajeErrorRecargas").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
				
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
	}
	
	
}
