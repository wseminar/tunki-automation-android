package scripts.persona;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class Cancelacion {
	private static String msjFinal = "";
	private static Element btnPorcentajePropina = null;
	public static void cancelacion() throws Exception{
				
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty()) return;
		
		if(!Pago.buscarComercio(GlobalData.getData("viComercio"))){
			msjFinal = "No se encontro el comercio.";
			return;
		}
				
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGFACE.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.TEXTOESPERA.getValue())).toString();
			return;
		}	
				
		//Wait ==> Notificacion  de Envio de cobro por parte de Cajero	
		
		
		if(GlobalData.getData("viComercioRechaza").equals("SI")){
			
			int nroVeces = Integer.parseInt(GlobalData.getData("viVecesCancelar"));		
			
			for (int i = 0; i < nroVeces; i++) {
				
				if(!GlobalData.getData("viTiempoEspera").isEmpty()){
					int segundosEspera = Integer.parseInt(GlobalData.getData("viTiempoEspera"));
					Thread.sleep(segundosEspera * 1000);
				}
				
				int tries = 0;
				while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGVIEW.getValue())).toString())){				
					if(tries>60)
					{
						msjFinal = "No se obtuvo ninguna solicitud de cobro";
						return;
					}
					Thread.sleep(5000);
					tries++;
				}
				String msj = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.lblTITULO.getValue())).toString();
				if(!msj.equals(Mensajes.PAGORECHAZADO))
				{
					msjFinal = msj;
					Reutilizable.tomarCapturaPantalla(500, "Error Cancelar Pago");
					return;
				}
				Reutilizable.tomarCapturaPantalla(500, "Cobro Cancelado");
				
				new Element("id", "btn_got_it");
				
				EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
				
				tries = 0;
				
				if((i+1) == nroVeces){
					if(GlobalData.getData("viContinuarFlujo").equals("SI")){
						while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGESPERA.getValue())).toString())){				
							if(tries>60)
							{
								msjFinal = "No se obtuvo ninguna solicitud de cobro";
								return;
							}
							Thread.sleep(5000);
							tries++;
						}
					}
					else{
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CHANGECOMERCE.getId()));
						while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MOVE.getId()));
						while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
						Element lblMovimientos = new Element("xpath", "//*[@text='Historial de movimientos']");
						if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lblMovimientos).toString())){
							Reutilizable.tomarCapturaPantalla(500, "Movimientos");
						}
						else{
							Reutilizable.tomarCapturaPantalla(500, "Error Movimientos");
							msjFinal = "Error al entrar al historial";
						}
						return;
					}
				}
				else{
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGESPERA.getValue())).toString())){				
						if(tries>60)
						{
							msjFinal = "No se obtuvo ninguna solicitud de cobro";
							return;
						}
						Thread.sleep(5000);
						tries++;
					}
				}
			}
			
		}
		
		String propina = GlobalData.getData("viPropina");
		boolean bTxtPropina = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId())).toString());
		
		if(!propina.isEmpty()){
			if(!bTxtPropina)
			{
				msjFinal = "La propina no esta habilitada para este comercio";
				return;
			}
			if(propina.contains("%")){
				propina = propina.replace("%", "").replace(".", "").replace("0", "").replace(",", "");
				btnPorcentajePropina = new Element("id", "btn_pct_" + propina);
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnPorcentajePropina).toString())){
					EFA.executeAction(Action.Click, btnPorcentajePropina);
				}
				else{
					msjFinal = "El porcentaje de propina indicado no es v�lido";
					return;
				}
			}
			else{				
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId()), propina);
				EFA.executeAction(Action.Back, null);
			}
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGVIEW.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.lblTITULO.getValue())).toString();
			if(!msjFinal.equals(Mensajes.PAGOREALIZADO))
				return;
			else{
				msjFinal = "";
				Element lblmontocontrol = new Element("id","tv_total");
				String lblMontocadena = EFA.executeAction(Action.GetText, lblmontocontrol).toString().substring(2).trim();
			}				
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		
		//if(GlobalData.getData("viValidarMovimiento").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MOVE.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			if(!validarComercioHistorial(GlobalData.getData("viComercio"))){
				msjFinal = "No se visualiza el �ltimo movimiento realizado";
			}
		//}		
	}	
	public static String getResult() throws Exception {
		String mensajeEsperado = GlobalData.getData("viMensajeErrorPago");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}
			return "El mensaje no es igual: " + msjFinal;
		}
		
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;			
	    	return msjFinal;
		
	}
	public static boolean validarComercioHistorial(String comercio) throws Exception{
		List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.id("tv_fullname"));
		if(lstMovimientos.size() > 0){
			String comercioPrimero = lstMovimientos.get(0).getText().trim();
			if(comercioPrimero.equals(comercio))
			{
				lstMovimientos.get(0).click();
				Reutilizable.tomarCapturaPantalla(500, "Detalle Pago");
				EFA.executeAction(Action.Back, null);
				Reutilizable.tomarCapturaPantalla(500, "Movimientos");
				return true;
			}
		}		
		return false;
	}
	
	public static boolean buscarComercio(String comercio) throws Exception{
		Element btnLupa = new Element("id", "search_button");
		Element txtBuscar = new Element("id", "search_src_text");
		
		EFA.executeAction(Action.Click, btnLupa);
		EFA.executeAction(Action.Clear, txtBuscar);
		EFA.executeAction(Action.SendKeys, txtBuscar, comercio);
		
		List<WebElement> lista = EFA.cv_driver.findElements(By.id("txt_name"));
		for (WebElement item : lista) {
			if(item.getText().equals(comercio)){
				item.click();
				return true;
			}
		}
		return false;
	}
	

}
