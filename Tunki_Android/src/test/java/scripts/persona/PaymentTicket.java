package scripts.persona;

import com.everis.GlobalData;

import scripts.Util.UtilBD;
import scripts.Util.UtilPaymentCommerce;
import scripts.helpers.Mensajes;
import scripts.helpers.ValidacionBD;

public class PaymentTicket {
	private static String msjFinal = "";
	private static String productId;
	private static String userId;
	private static String storeId;
	private static String commerceId;
	
	public static void payTicket() throws Exception{
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("SI")){
			UtilBD.borrarTarjeta();
		}
		if(GlobalData.getData("viQuitarValidacionTD").equals("SI")){
			UtilBD.quitarValidacionTD();
		}
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		if(GlobalData.getData("viBdTicket").toUpperCase().equals("CREAR TICKET")){
			userId = ValidacionBD.obtenerUserIdComercio(GlobalData.getData("viUsuario"));
			if(userId.equals("DataNotFound")){
				msjFinal = "No hubo respuesta de BD";
			}
			storeId = ValidacionBD.obtenerStoreIdComercio(userId);
			if(storeId.equals("DataNotFound")){
				msjFinal = "No hubo respuesta de BD";
			}
			commerceId = ValidacionBD.obtenerCommerceIdComercio(storeId);
			if(commerceId.equals("DataNotFound")){
				msjFinal = "No hubo respuesta de BD";
			}
			productId = ValidacionBD.obtenerProductIdComercio(commerceId);
			System.out.println(productId);
			if(!productId.equals("DataNotFound")){
				ValidacionBD.cambiarStatusTblProduct(productId);
			}
			ValidacionBD.createTicketToday();
			String dato = ValidacionBD.verificacionCreacionTicket("5000001279", "0");
			if(dato.equals("DataNotFound")){
				msjFinal = "no se creo el ticket";
				return;
			}
		}
		msjFinal = "";
		if(GlobalData.getData("viComercio").isEmpty()){
			msjFinal = UtilPaymentCommerce.menuTicket();
			return;
		}
		if(!UtilPaymentCommerce.buscarComercio(GlobalData.getData("viComercio"))){
			msjFinal = "No se encontr� el comercio.";
			return;
		}
		if(GlobalData.getData("viPagoSinTarjeta").toUpperCase().equals("SI")){
			msjFinal = UtilPaymentCommerce.payNoCard();
			if(!msjFinal.isEmpty())return;	
		}
		msjFinal = UtilPaymentCommerce.paymentCommerceTicket();
		if(!msjFinal.isEmpty())return;	
		msjFinal = UtilPaymentCommerce.menuTicket();
		if(!msjFinal.isEmpty())return;
		if(GlobalData.getData("viBdTicket").toUpperCase().equals("CREAR TICKET")){
			productId = ValidacionBD.obtenerProductIdComercio(commerceId);
			ValidacionBD.cambiarStatusTblProduct(productId);
		}
		msjFinal = UtilPaymentCommerce.menuMove();
		if(!msjFinal.isEmpty())return;
	}
	
	public static String getResult(){
		if (!GlobalData.getData("viMensajeErrorPago").isEmpty()) {
			if (GlobalData.getData("viMensajeErrorPago").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
	
	}
	
}
