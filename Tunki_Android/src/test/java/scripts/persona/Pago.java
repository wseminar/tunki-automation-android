package scripts.persona;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import script.textosTunki.ValidacionTexto;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;
import scripts.Util.UtilBD;
import scripts.Util.UtilPaymentCommerce;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;

public class Pago {

	private static String msjFinal = "";
	private static Element btnPorcentajePropina = null;
	
	public static void pagar() throws Exception{
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("SI")){
			UtilBD.borrarTarjeta();
		}
		if(GlobalData.getData("viQuitarValidacionTD").equals("SI")){
			UtilBD.quitarValidacionTD();
		}
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXTMENSAJERROR.getId())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXTMENSAJERROR.getId())).toString();
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			return;
		}
		if(!UtilPaymentCommerce.buscarComercio(GlobalData.getData("viComercio"))){
			msjFinal = "No se encontr� el comercio.";
			return;
		}
		if(GlobalData.getData("viPagoSinTarjeta").toUpperCase().equals("SI")){
			msjFinal = UtilPaymentCommerce.payNoCard();
			if(!msjFinal.isEmpty())return;	
		}
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DESCRIPTION.getValue())).toString())){
			msjFinal = "no muestra pantalla de cobro";
			return;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.lblSINTARJ.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.lblSINTARJ.getValue())).toString();
			return;	
		}
		
		if(GlobalData.getData("viCheckOut").equals("SI")){
			Element btn_change_commerce = new Element("id","btn_change_commerce");
			EFA.executeAction(Action.Click, btn_change_commerce);
			Logout.logout();
			msjFinal = Logout.getResult();
			
			return;
		}
		
		if(GlobalData.getData("viSalirComercio").equals("SI")){
			while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGESPERA.getValue())).toString()));
			EFA.executeAction(Action.Back, null);
			validarListaComercios();
			return;
		}	
		
		if(GlobalData.getData("viCambiarComercio").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CHANGECOMERCE.getId()));
			validarListaComercios();
			return;
		}		
		int tries = 0;
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DESCRIPTION.getValue())).toString())){
			if(tries>6)
			{
				msjFinal = "No se obtuvo ninguna solicitud de cobro";
				return;
			}
			Thread.sleep(20000);
			tries++;
		}
		Element xTarjeta = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.widget.ScrollView/android.view.View/android.widget.Spinner/android.widget.TextView");
		String Tarjeta = EFA.executeAction(Action.GetText,xTarjeta).toString().trim();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SPINNER_PAYMENT_METHOD.getId()));
		if(!GlobalData.getData("viElegirMetodoPago").isEmpty()){
			EFA.cs_getTestEvidence("Eleccion metodo de pago", 800);
			switch (GlobalData.getData("viElegirMetodoPago").toUpperCase()) {
			case "CREDITO":
				if(!UtilPaymentCommerce.EleccionTarjetaCredito()){
					msjFinal = "no se realizo el cambio";
					return;
				}
				break;
			case "DEBITO":
				if(!UtilPaymentCommerce.EleccionTarjetaDebito(Tarjeta)){
					msjFinal = "no se realizo el cambio";
					return;
				}
				break;
			default:
				break;
			}
		}else{
			EFA.executeAction(Action.Back, null);
		}
		String propina = GlobalData.getData("viPropina");
		boolean bTxtPropina = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId())).toString());
		
		if(!propina.isEmpty()){
			if(!bTxtPropina)
			{
				msjFinal = "La propina no esta habilitada para este comercio";
				return;
			}
			if(propina.contains("%")){
				propina = propina.replace("%", "");
				btnPorcentajePropina = new Element("id", "btn_pct_" + propina);
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnPorcentajePropina).toString())){
					EFA.executeAction(Action.Click, btnPorcentajePropina);
				}
				else{
					msjFinal = "El porcentaje de propina indicado no es v�lido";
					return;
				}
			}
			else{				
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPROPINA.getId()), propina);
				EFA.executeAction(Action.Back, null);
			}
		}
		Thread.sleep(5000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		Thread.sleep(5000);
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE.getId())).toString())){
			if(GlobalData.getData("viValidarAhora").toUpperCase().equals("PAGO")){
				msjFinal = UtilPaymentCommerce.validateTarjeta();
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId())).toString())){
					msjFinal = "";
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId()));
					EFA.cs_getTestEvidence("pantalla pago ticket", 1500);
					EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
					msjFinal = Reutilizable.verificarMensajesError();
					System.out.println(msjFinal);
					if(!msjFinal.isEmpty())return ;
				}
			}
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGVIEW.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.lblTITULO.getValue())).toString();
			if(!msjFinal.equals(Mensajes.PAGOREALIZADO))return;				
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId())).toString())){
			EFA.evidenceEnabled = false;
			msjFinal = ValidacionTexto.payConstancyP2C();
			if(!msjFinal.isEmpty())return;
			EFA.evidenceEnabled = true;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.verificarMensajesError();
			if(!msjFinal.equals("")) return;
			if(GlobalData.getData("viValidarMovimiento").equals("SI")){
			validarComercioHistorial(GlobalData.getData("viComercio").toUpperCase());
			EFA.evidenceEnabled = false;
			msjFinal = ValidacionTexto.payConstancyP2C();
			if(!msjFinal.isEmpty())return;
			EFA.evidenceEnabled = true;
		}
	}
	
	public static String getResult() throws Exception{
		String mensajeEsperado = GlobalData.getData("viMensajeErrorPago");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}
			return "El mensaje no es igual: " + msjFinal;
		}
		
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}
	
	public static boolean buscarComercio(String comercio) throws Exception{
		Element btnLupa = new Element("id", "search_view");
		Element txtBuscar = new Element("id", "search_view");
				
		EFA.executeAction(Action.Click, btnLupa);
		EFA.executeAction(Action.SendKeys, txtBuscar, comercio);
		Thread.sleep(3000);
		List<WebElement> lista = EFA.cv_driver.findElements(By.id("txt_name"));
		for (WebElement item : lista) {
			if(item.getText().equals(comercio)){
				if(GlobalData.getData("viDetalleComercio").equals("SI")){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.MOREINFO.getValue()));
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PAGARAQUI.getId()));
					Thread.sleep(8000);
					return true;
				}
				item.click();
				Thread.sleep(5000);
				return true;
			}
		}
		return false;
	}
	
	public static boolean validarComercioHistorial(String comercio) throws Exception{
		for (int i=0;i<7;i++){
			List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.view.View["+i+"]"));															   
		if(lstMovimientos.size() > 0){
			for (WebElement ymovimiento : lstMovimientos){
			WebElement nombre = ymovimiento.findElement(By.id("tv_full_name"));
			String xcliente = nombre.getText().trim().toUpperCase();
			System.out.println(xcliente);
			if(xcliente.equals(comercio)){
				lstMovimientos.get(0).click();
			}
		}
		Element back = new Element("xpath", "//*[@class='android.widget.ImageButton'][1]");
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, back).toString())){
			Element lblTiempoPago = new Element ("id","txt_date_time");
			Element lblMontoPago = new Element ("id","tv_total");
			GlobalData.setData("vOutTiempoPago",EFA.executeAction(Action.GetText, lblTiempoPago).toString());
			GlobalData.setData("vOutMontoPago",EFA.executeAction(Action.GetText, lblMontoPago).toString());
			Reutilizable.tomarCapturaPantalla(500, "Movimiento");
			return true;
		}
		else{
			msjFinal = "No se muestra el movimiento";
			Reutilizable.tomarCapturaPantalla(500, "No se muestra el movimiento");
			return false;
		}
		}
		}	
		return false;
	}
	
	public static void validarListaComercios() throws Exception{
		if(GlobalData.getData("viValidarComercio").equals("SI")){
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SEARCHCLOSE.getId()));
			int nroComercios = EFA.cv_driver.findElements(By.id("txt_name")).size();
			if(nroComercios <= 0){
				msjFinal = "No se ha mostrado ning�n comercio";
			}
		}
	}
	
	private static Boolean validarTransaccionBD(String comercio, String monto, String email){
		try {
			String token = ValidacionBD.persona_ValidarTransaccion(comercio, monto, email);
			if(token.equals(Mensajes.DATANOTFOUND)) 
			{
				//msjFinal = "El resultado es nulo";
				return false;
			}
			GlobalData.setData("vOutEstadoTransaccion", token);
			System.out.println(token);
		} catch (Exception e) {
			//msjFinal = "Error en la conexi�n a la base de datos";
			GlobalData.setData("vOutEstadoTransaccion", e.getMessage());
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	

}
