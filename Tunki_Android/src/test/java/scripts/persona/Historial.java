package scripts.persona;

import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class Historial {
	private static String msjFinal = "";
	private static float montoCobrado = -1;

	public static void historial() throws Exception {
		msjFinal = "";
		
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MOVE.getId())).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MOVE.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				
		Date fechaHoy = new Date();
		@SuppressWarnings("deprecation")
		String cadenaFechaHoy =  fechaHoy.getDate() + "/" + (fechaHoy.getMonth()+1) + "/" + (fechaHoy.getYear() +1900);
		if(!validarComercioHistorial(GlobalData.getData("viComercio"),false,false,false,cadenaFechaHoy)){
			msjFinal = "Error: No se visualiza el movimiento realizado";
			return;
		}
		
		if(!validarComercioHistorial(GlobalData.getData("viComercio"),false,true,true,cadenaFechaHoy)){
			msjFinal = "Error: No se visualizan correctamente movimientos del mes";
			return;
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		//Entrando al primer movimiento
		if(!validarComercioHistorial(GlobalData.getData("viComercio"),true,false,false,cadenaFechaHoy)){
			msjFinal = "Error: No se puede entrar a movimiento";
			return;
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(!(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId())).toString()) &&
				Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.TV_TOTAL.getValue())).toString()) &&
				Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.TXT_DATE_TIME.getValue())).toString()) &&
				Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.TXT_NAME_COMERCE.getValue())).toString()))){
			msjFinal = "Error: No se pudo ingresar a detalle de movimiento";
			return;
		}
		
		String fechaelemento = EFA.executeAction(Action.GetText,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.TXT_DATE_TIME.getValue())).toString().substring(0,10).trim();
		if(cadenaFechaHoy.equals(fechaelemento)){
			Element imgRegresar = new Element("xpath", "//*[@class='android.widget.ImageButton'][1]");
			EFA.executeAction(Action.Click, imgRegresar);
		}
		else{
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		}
		
		if(!validarComercioHistorial(GlobalData.getData("viComercio"),false,false,false,cadenaFechaHoy)){
			msjFinal = "Error: No se visualizan correctamente movimientos";
			return;
		}
		
	}

	public static String getResult() throws Exception {
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}
	
		
	public static boolean validarComercioHistorial(String comercio, boolean click,boolean validaFecha,boolean validaMes, String fechaValidar) throws Exception{
		List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.id("recycler_view"));
		if(lstMovimientos.size() > 0){
			for (WebElement ymovimiento : lstMovimientos) {
				List<WebElement> lstlayuouts = ymovimiento.findElements(By.className("android.widget.RelativeLayout"));
				if(lstlayuouts.size()>0){
					boolean finalrecorrido=true ;
					while(finalrecorrido){
						for (WebElement movimiento : lstlayuouts) {
							String tempFecha=movimiento.findElement(By.id("tv_date")).getText();
							String fechaCobradox = tempFecha.substring(0, 10);
							
							if(validaMes){
								String Mesfv = fechaValidar.substring(3);
								String Mesfc = fechaCobradox.substring(3);

								if(Mesfv.equals(Mesfc)){
									continue;
								}
								else{
									return false;
								}
							}
							
							if(validaFecha){
								
								if(fechaValidar.equals(fechaCobradox)){
									if(!GlobalData.getData("viMontoCobro").equals("")){
										WebElement xxmontoCobrado = null;
										xxmontoCobrado = movimiento.findElement(By.id("tv_amount"));
										montoCobrado = Float.parseFloat(xxmontoCobrado.getText().substring(5).trim());
										if(!GlobalData.getData("viMontoCobro").equals(montoCobrado)){
											continue;
										}
									}								
									
								}
								else{
									finalrecorrido = false;
									break;
								}								
							}
							else{
								finalrecorrido = false;
							}
							
							WebElement nombre = movimiento.findElement(By.id("tv_fullname"));
							String xcomercio = nombre.getText().trim();
											
							if(xcomercio.equals(comercio)){										
								if(click)
									movimiento.click();
								return true;
							}
							
						}
						
						if(validaMes){
							finalrecorrido = false;
						}
						
						if(finalrecorrido)
							DeslizarPantalla();
					}
					return true;
				}					
			}
		}		
		return false;
		
	}
	
	public static void DeslizarPantalla() throws Exception {
		
		String Arreglo[];	
		Dimension dimensions = EFA.cv_driver.manage().window().getSize();
		double dblStart,dblEnd;
		dblStart=0.9;
		dblEnd=0.3;
		Double screenHeightStart = dimensions.getHeight() *dblStart;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * dblEnd;
		int scrollEnd = screenHeightEnd.intValue();
		Double tempStart = scrollStart*dblStart;
		Double tempEnd = scrollEnd*dblEnd;
		Arreglo = new String[] {"0",tempStart.intValue()+"","0",tempEnd.intValue()+"","2200"};
		
		EFA.executeAction(Action.Swipe, Arreglo);
	}

}
