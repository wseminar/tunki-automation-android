package scripts.persona;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import script.textosTunki.Textos;
import script.textosTunki.ValidacionTexto;
import scripts.Util.UtilBD;
import scripts.Util.UtilPaymentCommerce;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePagoComercio;
import scripts.helpers.ReusableRegistroPersona;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;

public class GestionCuenta {

	private static String msjFinal = "";
	private static String tarjeta = "";
	private static Element optTipoDoc;

	public static void gestionCuenta() throws Exception{
		msjFinal = "";
		tarjeta = "";
		if(!GlobalData.getData("viStatusPca").isEmpty()){
			String dni = ValidacionBD.obtenerDnixPersonId(GlobalData.getData("viEmail"));
			System.out.println(dni);
			ValidacionBD.cambiarDefaultAhorro(dni);
			ValidacionBD.cambiarDefaultMillonaria(dni);
		}
		if(GlobalData.getData("viStatusPca").toUpperCase().equals("QUITAR VALIDACION")){
			ValidacionBD.cambiarStatusValidarPca(GlobalData.getData("viEmail"),0);
		}
		if(GlobalData.getData("viStatusPca").toUpperCase().equals("INSERTAR VALIDACION")){
			ValidacionBD.cambiarStatusValidarPca(GlobalData.getData("viEmail"),1);
		}
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("SI")){
			UtilBD.borrarTarjeta();
		}
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("TARJETA MILLONARIA")){
			String dni = ValidacionBD.obtenerDnixPersonId(GlobalData.getData("viEmail"));
			System.out.println(dni);
			ValidacionBD.eliminarTarjeteaMillonaria(dni);
		}
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal="";
		optTipoDoc = new Element("xpath", "//android.widget.CheckedTextView[@text='" + GlobalData.getData("viTipoDocumento") + "']");
		if(GlobalData.getData("viP2C").equals("SI")){
			if(!UtilPaymentCommerce.buscarComercio(GlobalData.getData("viComercio")))return;
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_TUNKI.getValue())).toString())){
				msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_TUNKI.getValue())).toString();
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NOT_YET.getId()));
			}
			msjFinal = Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty())return;
			msjFinal = ReusablePagoComercio.pagoComercio();
			if(!msjFinal.isEmpty())return;
		}
		msjFinal = Reutilizable.navegarMenuGestionCuentas();
		if(!msjFinal.isEmpty())return;
		if(GlobalData.getData("viNavegacion").toUpperCase().equals("BACK")){
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD_CARD.getId()));
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_TITLE.getValue())).toString().equals(Textos.TIPO_DE_TARJETA)){
			msjFinal = "No muestra pantalla Vincula tu tarjeta";
			return;
		}
			msjFinal = ReusableRegistroPersona.AgregarTarjeta();
			System.out.println(msjFinal);
			if(GlobalData.getData("viTipoTarjeta").toUpperCase().equals("DEBITO")){
				if (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.LISTADO_CUENTAS_DEBITO.getValue())).toString())){
					if(GlobalData.getData("viReIngresoGestionCuenta").toUpperCase().equals("VARIAS CUENTAS")){
						CapturarTextoPorId("v_item",tarjeta);
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
						while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
						msjFinal = Reutilizable.verificarMensajesError();
						if(msjFinal.trim().toUpperCase().equals(Textos.TARJETA_YA_ESTA_VINCULADA.trim().toUpperCase())){
							EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
							msjFinal = ReusableRegistroPersona.AgregarTarjeta();
							if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.LISTADO_CUENTAS_DEBITO.getValue())).toString())){
								CapturarTexto("v_item", tarjeta);
								EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));;
								while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
								msjFinal = Reutilizable.verificarMensajesError();
								if(!msjFinal.isEmpty())return;
								EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
								while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
								msjFinal = Reutilizable.verificarMensajesError();
								if(!msjFinal.isEmpty())return;
								if(GlobalData.getData("viPagoP2P").equals("SI")){
									if (!ReusableRegistroPersona.pagoP2P())return;	
								}
							}
							return;
						}
						if(!msjFinal.isEmpty())return;
					}
					
				}
				if(msjFinal.trim().toUpperCase().equals(Textos.TARJETA_YA_ESTA_VINCULADA.trim().toUpperCase())){
					msjFinal = "";
					if(GlobalData.getData("viReIngresoGestionCuenta").toUpperCase().equals("CUENTA UNICA")){
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
						EFA.executeAction(Action.Back, null);
						EFA.executeAction(Action.Back, null);
						if(GlobalData.getData("viPagoP2P").equals("SI")){
							if (!ReusableRegistroPersona.pagoP2P())return;	
						}
						return;
					}
				}
				if(!msjFinal.isEmpty())return;
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId())).toString())){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
					while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					msjFinal = Reutilizable.verificarMensajesError();
					if(!msjFinal.isEmpty())return;
				}
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_START.getId())).toString())){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_START.getId()));
				}
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE_LATER.getId())).toString())){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE_LATER.getId()));
				}
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId())).toString())){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId()));
				}
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MORE.getId())).toString())){
					msjFinal = "No muestra menu mas";
				}
				EFA.cs_getTestEvidence("Menu mas", 800);
				if(GlobalData.getData("viPagoP2P").equals("SI")){
					if (!ReusableRegistroPersona.pagoP2P())return;	
				}
				if(!msjFinal.isEmpty())return;
			}
				
	}
	
	public static boolean CapturarTextoPorId(String Id, String Texto) {
		List<WebElement> listaElementosporId = EFA.cv_driver.findElementsById(Id);
		for (WebElement webElement : listaElementosporId) {
			WebElement btnRadio = webElement.findElement(By.id("rb_select"));
			WebElement tarjeta = webElement.findElement(By.id("tv_name"));
			String tempTexto = tarjeta.getText().trim().toUpperCase();
			System.out.println(tempTexto);
			if(tempTexto.equals(Texto.trim().toUpperCase())) {
				btnRadio.click();
				return true;
			}
		}
		return false;
	}
	
	public static boolean CapturarTexto(String Id, String Texto) {
		List<WebElement> listaElementosporId = EFA.cv_driver.findElementsById(Id);
		for (WebElement webElement : listaElementosporId) {
			WebElement btnRadio = webElement.findElement(By.id("rb_select"));
			WebElement tarjeta = webElement.findElement(By.id("tv_name"));
			String tempTexto = tarjeta.getText().trim().toUpperCase();
			if(!tempTexto.equals(Texto.trim().toUpperCase())) {
				btnRadio.click();
				return true;
			}
		}
		return false;
	}
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeErrorGestionCuenta").isEmpty()) {
			if(GlobalData.getData("viMensajeErrorGestionCuenta").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		
		return msjFinal.isEmpty() ? Mensajes.SUCCESS:msjFinal;
	}

}
