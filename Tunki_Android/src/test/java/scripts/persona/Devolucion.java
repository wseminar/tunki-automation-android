package scripts.persona;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class Devolucion {

	private static String msjFinal = "";
	private static String colormostrado = "";
	private static float montoCobrado = -1;
	
	public static void devolucion() throws Exception{
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId())).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				
		if(!validarComercioHistorial(GlobalData.getData("viComercio"),false,true)){
			msjFinal = "Error: No se visualiza el movimiento realizado";
			return;
		}		
				
		boolean validacionColor = (colormostrado.equals("red"))? false : true;
		
		if(!validacionColor){
			msjFinal = "Error: No se realizó correctamente el proceso de Devolución";
			return;
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId())).toString())){
			msjFinal = "Error: Validación incorrecta de movimiento";
			return;
		}
			
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(!validarComercioHistorial(GlobalData.getData("viComercio"),false,false)){
			msjFinal = "Error: No se visualiza el movimiento realizado";
			return;
		}
	}
	
	public static String getResult() throws Exception{
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}
	
	public static boolean validarComercioHistorial(String cliente, boolean click, boolean valores) throws Exception{
		List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.id("recycler_view"));
		if(lstMovimientos.size() > 0){			
			for (WebElement movimiento : lstMovimientos) {
				WebElement nombre = movimiento.findElement(By.id("tv_fullname"));
				String xcliente = nombre.getText().trim();
				if(xcliente.equals(cliente)){
					if(!GlobalData.getData("viMontoCobro").equals("")){
						WebElement xxmontoCobrado = null;
						xxmontoCobrado = movimiento.findElement(By.id("tv_amount"));
						montoCobrado = Float.parseFloat(xxmontoCobrado.getText().substring(5).trim());
						if(!GlobalData.getData("viMontoCobro").equals(montoCobrado)){
							return false;
						}
					}
					
					if(valores){
						WebElement xmontoCobrado = null;
						xmontoCobrado = movimiento.findElement(By.id("tv_amount"));
						//verificar lo del color
						try{
							colormostrado = xmontoCobrado.getCssValue("color").toLowerCase();
						}
						catch(Exception ex){
							colormostrado = "red";
							System.out.println(ex.getMessage());
						}
						//verificar lo del color
					}					
					if(click)
						movimiento.click();
					return true;
				}
			}
		}		
		return false;	
	}
}