package scripts.persona;

import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.BasicConfigurator;

import com.everis.GlobalData;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseCheckin {
	public static void smsFirebase(String phone,String email) throws Exception {
					BasicConfigurator.configure();
					FileInputStream serviceAccount =
							 new FileInputStream("tunki-uat-firebase-adminsdk.json"); //SE CAMBIA
							
							FirebaseOptions options = new FirebaseOptions.Builder()
							 .setCredentials(GoogleCredentials.fromStream(serviceAccount))
							 .setDatabaseUrl("https://tunki-uat.firebaseio.com") // SE CAMBIA
							 .build();

							FirebaseApp.initializeApp(options);///
				final FirebaseDatabase database = FirebaseDatabase.getInstance();
				final DatabaseReference ref2 = database.getReference("automation/phone-validate-code");
				DatabaseReference ref = database.getReference("automation/phone-validate-code/"+phone);
				ApiFuture<Void> apiFuture = ref.setValueAsync(email);
				System.out.println("esperando firebase");
				apiFuture.get(20,TimeUnit.SECONDS);
				
	}
	
	public static void stopListener(DatabaseReference ref,ChildEventListener listener){
		ref.removeEventListener(listener);
	}
	
}
