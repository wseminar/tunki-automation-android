package scripts.persona;

import com.everis.GlobalData;

import scripts.helpers.Mensajes;

public class CommerceList {
	private static String msjFinal = "";
	public static void searchCommerce() throws Exception{
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		
		
		
		
	}
	
	public static String getResult(){
		if (!GlobalData.getData("viMensajeErrorBusqueda").isEmpty()) {
			if (GlobalData.getData("viMensajeErrorBusqueda").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
	
	}
}
