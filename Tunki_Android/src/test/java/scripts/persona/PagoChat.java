package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import script.textosTunki.Textos;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePxP;
import scripts.helpers.Reutilizable;

public class PagoChat {
	public static String msjFinal = "";
	
	public static void pagoChat() throws Exception{
		msjFinal = "";
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return;
		EFA.cs_getTestEvidence("Lista de Comercio", 500);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CONTACT.getId()));
		Thread.sleep(3000);
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SYNC_P2P_CONTACTS.getId())).toString())){
			 msjFinal = ReusablePxP.synchronizeContacts();
			 if(!msjFinal.isEmpty())return;
		}
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();	
		if(!msjFinal.isEmpty()) return;
		Thread.sleep(1000);
		msjFinal = ReusablePxP.buscarPersonaP2P(GlobalData.getData("viPersonaCobrar"));
		if(!msjFinal.isEmpty()) return;
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		int tries = 0;
		if(!GlobalData.getData("viMontoCobro2").isEmpty()){
			while(!ReusablePxP.capturarRespuestaChat(Textos.RECIBISTE_UN_COBRO,"S/ "+GlobalData.getData("viMontoCobro2"))){
				if(tries>6)
				{
					msjFinal = "No se obtuvo ninguna solicitud de cobro";
					return;
				}
				Thread.sleep(15000);
				tries++;
			}
		}
		while(!ReusablePxP.capturarRespuestaChat(Textos.RECIBISTE_UN_COBRO,"S/ "+GlobalData.getData("viTransferPago"))){
			if(tries>6)
			{
				msjFinal = "No se obtuvo ninguna solicitud de cobro";
				return;
			}
			Thread.sleep(15000);
			tries++;
		}
		if(!GlobalData.getData("viMontoCobro2").isEmpty()){
			msjFinal =ReusablePxP.capturarIdChat(GlobalData.getData("viMontoCobro2"));
			if(!msjFinal.isEmpty())return;
			msjFinal = ReusablePxP.irAPagar();
			if(msjFinal.equals("No se pudo realizar el pago.")){
				msjFinal = "";
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			}
			if(!msjFinal.isEmpty())return;
		}
		msjFinal =ReusablePxP.capturarIdChat(GlobalData.getData("viTransferPago"));
		if(!msjFinal.isEmpty())return;
		msjFinal = ReusablePxP.irAPagar();
		if(msjFinal.equals("No se pudo realizar el pago.")){
			msjFinal = "";
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		}
		if(!msjFinal.isEmpty())return;
		EFA.executeAction(Action.Back, null);
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.equals("")) return;	
		if(GlobalData.getData("viValidarMovimiento").equals("SI")){		
			Reutilizable.validarHistorialPersona(GlobalData.getData("viPersonaCobrar").toUpperCase());
		}
		
	}
	
	public static String getResult() throws Exception {
		String mensajeEsperado = GlobalData.getData("viMensajeErrorCobro");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}else{
				return "El mensaje no es igual: " + msjFinal;
			}
		}
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}
	
}
