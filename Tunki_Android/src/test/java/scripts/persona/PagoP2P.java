package scripts.persona;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.Util.UtilBD;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePxP;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;

public class PagoP2P {
	
	private static String msjFinal = "";
	
	public static void p2p() throws Exception{
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("SI")){
			UtilBD.borrarTarjeta();
		}
		if(GlobalData.getData("viQuitarValidacionTD").equals("SI")){
			UtilBD.quitarValidacionTD();
		}
		if(GlobalData.getData("viQuitarValidacionTD").equals("AHORRO MILLONARIA")){
			String dni = ValidacionBD.obtenerDnixPersonId(GlobalData.getData("viEmail"));
			System.out.println(dni);
			ValidacionBD.cambiarStatusTblPcaMillonaria(dni);
		}
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMENSAJERROR.getId())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMENSAJERROR.getId())).toString();
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			return;
		}
		if(GlobalData.getData("viValidarMovimientoReceptor").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CONTACT.getId()));
			EFA.cs_getTestEvidence("Contactos Notificacion", 1000);
			if(GlobalData.getData("viPantallaBusqueda").toUpperCase().equals("FAVORITOS")){
				while(!ReusablePxP.validarContacto(GlobalData.getData("viPersonaPagar"),GlobalData.getData("viPersonaNumero"))){
					EFA.cs_getTestEvidence("Deslizar", 500);
					Reutilizable.swiptToBottom();
					Thread.sleep(500);
				}
			}
			if(GlobalData.getData("viPantallaBusqueda").toUpperCase().equals("CONTACTOS")){
				msjFinal = ReusablePxP.buscarPersonaP2P(GlobalData.getData("viPersonaPagar"));
				if(!msjFinal.isEmpty())return;
			}
			Thread.sleep(3000);
			EFA.cs_getTestEvidence("pantalla chat", 800);
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			Reutilizable.validarHistorialPersona(GlobalData.getData("viPersonaPagar").toUpperCase());
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CONTACT.getId()));
		Thread.sleep(3000);
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SYNC_P2P_CONTACTS.getId())).toString())){
			while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty()) return;
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SYNC_P2P_CONTACTS.getId()));
			while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.verificarMensajesError();	
			if(!msjFinal.isEmpty()) return;
			String codigoVerificacion = obtenerCodigoVerificacionAlfa();
			System.out.println("El codigo es "+codigoVerificacion);
			AndroidElement el1 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_1"));
			el1.setValue(codigoVerificacion.substring(0,1));
			AndroidElement el2 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_2"));
			el2.setValue(codigoVerificacion.substring(1,2));
			AndroidElement el3 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_3"));
			el3.setValue(codigoVerificacion.substring(2, 3));
			AndroidElement el4 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_4"));
			el4.setValue(codigoVerificacion.substring(3, 4));
			AndroidElement el5 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_5"));
			el5.setValue(codigoVerificacion.substring(4, 5));
			AndroidElement el6 = (AndroidElement) EFA.cv_driver.findElement(By.id("et_code_6"));
			el6.setValue(codigoVerificacion.substring(5, 6));
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VALIDATE_CODE_BUTTON.getId()));
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString())){
				msjFinal = "No se completo la sincronizacion";
				return;
			}
		}
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();	
		if(!msjFinal.isEmpty()) return;
		Thread.sleep(5000);
		if(GlobalData.getData("viTipoBusqueda").equals("BUSCADOR")){
			msjFinal = ReusablePxP.buscarPersonaP2P(GlobalData.getData("viPersonaPagar"));
			if(!msjFinal.isEmpty()) return;
		}
		if(GlobalData.getData("viTipoBusqueda").equals("LISTA")){
			for (int i = 0; i <7; i++) {
				if(!Reutilizable.CapturarTextoPorId("txt_p2p_name",GlobalData.getData("viPersonaPagar"))){
					EFA.cs_getTestEvidence("Deslizar", 500);
					swiptToBottom();
					Thread.sleep(1000);
				}else{
				    break;
				}
			}
			if(!GlobalData.getData("viVecesBusqueda").isEmpty()){
				EFA.executeAction(Action.Back, null);
				while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				msjFinal = Reutilizable.verificarMensajesError();
				EFA.cs_getTestEvidence("Pantalla Busqueda", 500);
				for (int i = 0; i <7; i++) {
					if(!Reutilizable.CapturarTextoPorId("txt_p2p_name",GlobalData.getData("viPersonaPagar"))){
						EFA.cs_getTestEvidence("Deslizar", 500);
						swiptToBottom();
						Thread.sleep(1000);
					}else{
					    break;
					}
				}
			}
		}
		msjFinal = ReusablePxP.EnvioPagoP2P();
		if(!msjFinal.isEmpty()) return;
		EFA.executeAction(Action.Back, null);
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.equals("")) return;	
		if(GlobalData.getData("viValidarMovimiento").equals("SI")){		
			Reutilizable.validarHistorialPersona(GlobalData.getData("viPersonaPagar").toUpperCase());
		}
	}
	
	public static String getResult() throws Exception{
		String mensajeEsperado = GlobalData.getData("viMensajeErrorCobro");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}else{
				return "El mensaje no es igual: " + msjFinal;
			}
		}
		
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}
	
	public static String obtenerCodigoVerificacionAlfa() throws Exception {
		String codigo = "";
		String primerDato = "";

		int ciclos = 2;
		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i < ciclos; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "(?=.*[0-9])(?=.*[a-zA-Z]).{6,}";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
	
	public static void swiptToBottom() throws Exception{
		Dimension dim = EFA.cv_driver.manage().window().getSize();
		int height = dim.getHeight();
		int width = dim.getWidth();
		int x = width/2;
		
		int top_y = (int)(height*0.80);
		int bottom_y = (int)(height*0.20);
		
		System.out.println(x+"   "+top_y+"  "+bottom_y);
		TouchAction s = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		s.press(x, top_y).waitAction(Duration.ofMillis(2500)).moveTo(x, bottom_y).release().perform();
		EFA.cs_getTestEvidence("Deslizar", 500);
	}

	
}