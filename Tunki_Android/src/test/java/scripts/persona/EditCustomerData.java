package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class EditCustomerData {
	private static String msjFinal = "";
	private static Element crop = new Element("id", "crop_image_menu_crop");
	public static void customerData() throws Exception{
		Login_new.login();
		msjFinal = Login_new.getResult();
		if (!msjFinal.equals(Mensajes.SUCCESS))
			return;
		msjFinal = "";
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMENSAJERROR.getId())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTMENSAJERROR.getId())).toString();
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
		EFA.cs_getTestEvidence("Menu mas", 1000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_EDIT.getId()));
		EFA.cs_getTestEvidence("Editar Persona", 1000);
		if(GlobalData.getData("viTipoFoto").toUpperCase().equals("SELFIE")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SELFIE.getId()));
			msjFinal=Reutilizable.selfiePhoto();
			if(!msjFinal.isEmpty())return;
		}
		if(GlobalData.getData("viTipoFoto").toUpperCase().equals("GALERIA")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PICTURE.getId()));
			msjFinal = Reutilizable.galleryPhoto();
			if(!msjFinal.isEmpty())return;
		}
		Thread.sleep(5000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONFIRM.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return;
		EFA.cs_getTestEvidence("Menu Mas", 1000);
	}
	
	public static String getResult() throws Exception{
		if (!GlobalData.getData("viMensajeErrorEditar").isEmpty()) {
			if (GlobalData.getData("viMensajeErrorEditar").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		return msjFinal.isEmpty() ? Mensajes.SUCCESS : msjFinal;
	}
	
}
