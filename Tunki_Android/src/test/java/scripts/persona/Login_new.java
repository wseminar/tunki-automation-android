package scripts.persona;
import com.everis.GlobalData;
import scripts.Util.UtilLoginCustomer;
import scripts.helpers.Mensajes;
import scripts.helpers.ValidacionBD;

public class Login_new {
	public static String msjFinal = "";
	public static int n = 0;
	
	public static void login() throws Exception {
		msjFinal = UtilLoginCustomer.loadingScreenLogin();
		if(!msjFinal.isEmpty())return;
		msjFinal = UtilLoginCustomer.enterYourUserName();
		if(!msjFinal.isEmpty())return;
		msjFinal = UtilLoginCustomer.loginPersona();
		if(!msjFinal.isEmpty())return;		
	}

	public static String getResult() throws Exception {
		if (!GlobalData.getData("viMensajeError").isEmpty()) {
			if (GlobalData.getData("viMensajeError").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
	}
	
}
