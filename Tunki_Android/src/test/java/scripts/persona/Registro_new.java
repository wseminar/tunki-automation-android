package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import script.textosTunki.Textos;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;
import scripts.Util.UtilPaymentCommerce;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePagoComercio;
import scripts.helpers.ReusableRegistroPersona;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;

public class Registro_new {
	private static String msjFinal = "";
	private static Element optTipoDoc = new Element("xpath",
			"//android.widget.CheckedTextView[@text='" + GlobalData.getData("viTipoDocumento") + "']");

	public static void RegistroPersona() throws Exception {
		msjFinal = "";
		String dato = ValidacionBD.obtenerPersonId(GlobalData.getData("viDocIdentidad"));
		System.out.println(dato);
		if(!dato.equals("DataNotFound")){
			ValidacionBD.modificarDatosTblPerson(dato);
			ValidacionBD.modificarDatosTblUser(dato);
			System.out.println("se borro usuario");
		}
		optTipoDoc = new Element("xpath",
				"//android.widget.CheckedTextView[@text='" + GlobalData.getData("viTipoDocumento") + "']");
		while (!Boolean
				.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_UP.getId()))
						.toString()))
			;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_UP.getId()));
		msjFinal = ReusableRegistroPersona.ingresarDatosRegistrate();//Ingreso Datos Primera pantalla
		if(!msjFinal.isEmpty()) return;
		if (GlobalData.getData("viNavegacion").equals("SI")) {
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_VALIDATION_CODE.getId()),"BF23EA");
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_RESEND.getId()));
			while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			String codigoVerificacion = ReusableRegistroPersona.ingresarCodigo();
			if (!codigoVerificacion.isEmpty())return;
			msjFinal = ReusableRegistroPersona.ingresarDatos();
			if(!msjFinal.isEmpty()) return;
			EFA.executeAction(Action.Back, null);
			Thread.sleep(500);
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONTINUE.getId()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			
			codigoVerificacion = ReusableRegistroPersona.ingresarCodigo();
			if (!codigoVerificacion.isEmpty())return;
			msjFinal = ReusableRegistroPersona.ingresarDatos();
			if(!msjFinal.isEmpty()) return;
			String codigoSMS = ReusableRegistroPersona.ingresarCodigoSMS();
			if (!codigoSMS.isEmpty())
				return;
			return;
		}
		msjFinal = ReusableRegistroPersona.ingresarCodigo();
		if (!msjFinal.isEmpty())
			return;
		msjFinal = ReusableRegistroPersona.ingresarDatos();//Ingresar Datos Segunda Pantalla
		if(!msjFinal.isEmpty()) return;
		 msjFinal = ReusableRegistroPersona.ingresarCodigoSMS();
		if (!msjFinal.isEmpty())
			return;
		if (!Boolean
				.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()))
						.toString())) {
			msjFinal = "No se logeo correctamente";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
		Thread.sleep(1000);
		if (GlobalData.getData("viAgregarTarjeta").equals("SI")) {
				if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(),EText.TV_TITLE.getValue())).toString().equals(Textos.TIPO_DE_TARJETA)){
					msjFinal = "No muestra pantalla Vincula tu tarjeta";
					return;
				}
				msjFinal = ReusableRegistroPersona.AgregarTarjeta();
				if(!msjFinal.isEmpty())return;
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_START.getId())).toString())){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_START.getId()));
				}
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE_LATER.getId())).toString())){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE_LATER.getId()));
				}
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId())).toString())){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId()));
				}
				while (Boolean.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
						.toString()))
					;
				msjFinal = Reutilizable.verificarMensajesError();
				if (!msjFinal.isEmpty())
					return;
				if(GlobalData.getData("viP2C").equals("SI")){
					if(!UtilPaymentCommerce.buscarComercio(GlobalData.getData("viComercio"))){
						msjFinal = "No se encontr� el comercio.";
						return;
					}
					if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_TUNKI.getValue())).toString())){
						msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_TUNKI.getValue())).toString();
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NOT_YET.getId()));
					}
					msjFinal = Reutilizable.verificarMensajesError();
					if(!msjFinal.isEmpty())return;
					msjFinal = ReusablePagoComercio.pagoComercio();
					if(!msjFinal.isEmpty())return;
				}
				if(GlobalData.getData("viPagoP2P").equals("SI")){
					if (!ReusableRegistroPersona.pagoP2P())return;	
				}
				if(GlobalData.getData("viReIngreso").toUpperCase().equals("LOGIN")){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
					EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
					TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
					x.press(45,829).moveTo(-6, -569).release().perform();
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SIGNOUT.getId()));
					while (Boolean.parseBoolean(EFA
							.executeAction(Action.IsElementPresent,
									Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
							.toString()))
						;
					msjFinal = Reutilizable.verificarMensajesError();
					if (!msjFinal.isEmpty())
						return;
					EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()));
					EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()),GlobalData.getData("viEmail2"));
					EFA.executeAction(Action.Back, null);
					EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
					EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()),GlobalData.getData("viPassword_2"));
					EFA.executeAction(Action.Back, null);
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getId()));
					while (Boolean.parseBoolean(EFA
							.executeAction(Action.IsElementPresent,
									Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
							.toString()))
						;
					msjFinal = Reutilizable.verificarMensajesError();
					if (!msjFinal.isEmpty())
						return;
					EFA.cs_getTestEvidence("Captura Login", 500);
				}
				return;
		}
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONTINUE.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())
			return;
		if (!Boolean
				.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_START.getId()))
						.toString())) {
			msjFinal = "No se realizo el registro";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_START.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())
			return;
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())
			return;
		if(GlobalData.getData("viPagoP2P").equals("SI")){
			if (!ReusableRegistroPersona.pagoP2P())return;	
		}
		if(GlobalData.getData("viReIngreso").toUpperCase().equals("LOGIN")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
			TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
			x.press(45,829).moveTo(-6, -569).release().perform();
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SIGNOUT.getId()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty())
				return;
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()),GlobalData.getData("viEmail2"));
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()),GlobalData.getData("viPassword_2"));
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getId()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty())
				return;
			EFA.cs_getTestEvidence("Captura Login", 500);
		}
		
	}
	
	public static String getResult() throws Exception { 
		if (!GlobalData.getData("viMensajeErrorRegistro").isEmpty()) {
			if (GlobalData.getData("viMensajeErrorRegistro").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}

		return msjFinal.isEmpty() ? Mensajes.SUCCESS : msjFinal;
	}
}
