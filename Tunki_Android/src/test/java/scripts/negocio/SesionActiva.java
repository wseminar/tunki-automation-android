package scripts.negocio;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class SesionActiva {
		
	private static String msjFinal = "";
	private static Element imgErrorAlert = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView");
	private static Element lblMensaje = new Element("id", "txt_message");
	private static Element bntRegresar = new Element ("xpath", "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.view.View/android.widget.ImageButton");
	
	public static void sesionActiva() throws Exception{
		
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		
		//inicio nuevo flujo
	
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));	
		
		//validaci�n de lista de clientes
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CUSTOMER.getId())).toString())){
			msjFinal = "Error: No se muestra lista de clientes";
			Thread.sleep(500);
			EFA.cs_getTestEvidence("No se muestran los clientes", 0);
			return;
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Men� M�s", 0);
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SETTING.getId()));
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Configuraci�n", 0);
		
		Boolean sgsesionActiva = Boolean.parseBoolean(EFA.executeAction(Action.GetAttribute, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.SW_SESSION_ALIVE.getValue()), "checked").toString());
		
		String indicadorSesion = "NO";
		if(GlobalData.getData("viIsMantenerSesion").equals("SI") || GlobalData.getData("viMantenerSesion").equals("SI"))indicadorSesion = "SI";
		
		if(sgsesionActiva && indicadorSesion.equals("NO")){
			msjFinal = "El indicador de sesi�n activa no tiene el mismo valor que en la base de datos";
			return;
			}
		else if(!sgsesionActiva && indicadorSesion.equals("SI")){
			msjFinal = "El indicador de sesi�n activa no tiene el mismo valor que en la base de datos";
			return;
		}
		if(GlobalData.getData("viCambiarEstado").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.SW_SESSION_ALIVE.getValue()));
		}
		
		EFA.executeAction(Action.Click, bntRegresar);
		
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Men� M�s", 0);
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SIGNOUT.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(GlobalData.getData("viReIngreso").equals("SI")) {
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()), GlobalData.getData("viPassword"));
			EFA.executeAction(Action.Back, null);
			
			if(GlobalData.getData("viReMantenerSesion").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CHKREMEMBER.getValue()));
			}
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getId()));
			
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));

			Thread.sleep(500);
			EFA.cs_getTestEvidence("Ingreso por segunda vez", 0);
			
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgErrorAlert).toString()))
			{
				msjFinal = EFA.executeAction(Action.GetText, lblMensaje).toString();
				
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));	
				
				Thread.sleep(500);
				
				EFA.cs_getTestEvidence("Pantalla Login", 0);
				
				return;
			}
			
			
			if(!msjFinal.isEmpty()) {
				return;
			}
		
			}
		
		boolean checkManSes = Boolean.parseBoolean(EFA.executeAction(Action.GetAttribute,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CHKREMEMBER.getValue()), "checked").toString());
		if(checkManSes){
			msjFinal = "Check de mantener sesi�n activa no debe estar marcado en la ventana de Login";
			return;		
		}
		}
	
	public static String getResult() throws Exception{
		if(msjFinal.isEmpty())
			return Mensajes.SUCCESS;
		return msjFinal;
	}
}

