package scripts.negocio;

import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class Devolucion {

	private static String msjFinal = "";
	private static Element lblerror = new Element("id", "textinput_error");
	private static Element imgErrorAlert = new Element("xpath","/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView");
	private static Element lblMensaje = new Element("id", "txt_message");
	private static float montoCobrado = -1;
	private static float propinaCobrado = -1;
	private static String fechaCobrado = "";
	
	//private static Element progress = new Element("xpath", "//*[@class='android.widget.ProgressBar']");
	
	public static void devolucion() throws Exception{
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";

		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MOVE_COMMERCE.getId())).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MOVE_COMMERCE.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				
		if(!validarClientesHistorial(GlobalData.getData("viClienteNombre").toUpperCase(),true,true)){
			msjFinal = "Error: No se visualiza el movimiento realizado";
			return;
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			
		Element btnAccept = new Element("id","btn_accept");
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnAccept).toString())){
			msjFinal = "Error: Validación incorrecta de movimiento";
			return;
		}	
		
		if(EFA.executeAction(Action.GetText, btnAccept).toString().contains(Mensajes.ENTENDIDO)){
			msjFinal = "Error: No se puede realizar Devolución";
			EFA.executeAction(Action.Click, btnAccept);
			return;
			}
		
		if(EFA.executeAction(Action.GetText, btnAccept).toString().contains(Mensajes.DEVOLVER)){
			EFA.executeAction(Action.Click, btnAccept);
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		}
		
		Element txtclave = new Element("id","tv_password");		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, txtclave).toString())){
			msjFinal = "Error: Problemas en Devolución";
			return;
		}
		
		EFA.executeAction(Action.Clear, txtclave);
		EFA.executeAction(Action.SendKeys, txtclave,GlobalData.getData("viPasswordDev"));
		
		Element btnAceptarDevolucion = new Element("id","btn_accept");
		EFA.executeAction(Action.Click, btnAceptarDevolucion);
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lblerror).toString())){
			msjFinal = EFA.executeAction(Action.GetText, lblerror).toString();
			return;
		}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgErrorAlert).toString()))
		{
			msjFinal = "No se pudo realizar la Devolución";
			EFA.executeAction(Action.Click, btnAccept);	
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Pantalla Password", 0);
			return;
		}
		
		if (msjFinal.equals("")){
			Element firstItem = new Element ("xpath","//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.view.View/android.support.v7.widget.RecyclerView/android.view.View[1]");
			EFA.executeAction(Action.Click, firstItem);
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Devolucion Realizada", 0);
			EFA.executeAction(Action.Click, btnAccept);
			
		}	
	}
	
	public static String getResult() throws Exception{
		
		if(!GlobalData.getData("viMensajeErrorDevolucion").isEmpty()){
			if(GlobalData.getData("viMensajeErrorDevolucion").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
				
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
		
	}
	
	public static boolean validarClientesHistorial(String cliente, boolean click, boolean valores) throws Exception{
		for (int i=0;i<7;i++){
		List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.view.View/android.support.v7.widget.RecyclerView/android.view.View["+i+"]"));		
		if(lstMovimientos.size() > 0){			
			for (WebElement ymovimiento : lstMovimientos){
						WebElement nombre = ymovimiento.findElement(By.id("tv_full_name"));
						String xcliente = nombre.getText().trim().toUpperCase();
						System.out.println(xcliente);
						if(xcliente.equals(cliente)){
							boolean continuar = true;
							
							if(!GlobalData.getData("viMontoCobro").equals("")){
								WebElement xxmontoCobrado = null;
								xxmontoCobrado = ymovimiento.findElement(By.id("tv_amount"));
								montoCobrado = Float.parseFloat(xxmontoCobrado.getText().substring(5).trim());
								System.out.println(montoCobrado);
								if(!GlobalData.getData("viMontoCobro").equals(xxmontoCobrado.getText().substring(5).trim())){
									continuar=false;
								}
							}
							
							if(continuar){
								if(valores){
									WebElement xmontoCobrado = null;
									xmontoCobrado = ymovimiento.findElement(By.id("tv_amount"));
									montoCobrado = Float.parseFloat(xmontoCobrado.getText().substring(5).trim());
									WebElement xfechaCobrado = ymovimiento.findElement(By.id("tv_date"));
									fechaCobrado = xfechaCobrado.getText().substring(0, 10);
									System.out.println(fechaCobrado);
									WebElement xpropinaCobrado = null; 
									try{
										xpropinaCobrado = ymovimiento.findElement(By.id("tv_tip"));
									}
									catch(Exception ex){
										xpropinaCobrado = null;
									}
									if(!(xpropinaCobrado == null))
										propinaCobrado = Float.parseFloat(xpropinaCobrado.getText().substring(5).trim());
								}					
								if(click)
									ymovimiento.click();
								return true;
							}
							
						}
					}
				}		
		}
		return false;
  }
}
