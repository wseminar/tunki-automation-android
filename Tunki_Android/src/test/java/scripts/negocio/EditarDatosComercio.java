package scripts.negocio;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class EditarDatosComercio {
	private static String msjFinal = "";

	public static void editarComercio() throws Exception {

		Login.login();
		msjFinal = Login.getResult();
		if (!msjFinal.equals(Mensajes.SUCCESS))
			return;
		msjFinal = "";

		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		if (!Boolean
				.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CUSTOMER.getId()))
						.toString())) {
			msjFinal = "Error: No se muestra lista de clientes";
			Thread.sleep(500);
			Reutilizable.tomarCapturaPantalla(300, "No se muestra los clientes");
			return;
		}

		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Men� M�s", 0);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.EDIT.getId()));
		if (GlobalData.getData("viModificarFoto").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.GALLERY.getId()));
			msjFinal = Reutilizable.galleryPhoto();
			if(!msjFinal.isEmpty())return;
			EFA.cs_getTestEvidence("Cambio foto", 500);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty()) {
				return;
			}
			Reutilizable.tomarCapturaPantalla(300, "MenuMas");
		}
		if (GlobalData.getData("viModificarLocal").equals("SI")) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.STORENAME.getId()));
			EFA.executeAction(Action.Clear,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.STORENAME.getId()));
			if(!GlobalData.getData("viNombreLocal").isEmpty()){
				EFA.executeAction(Action.SendKeys,
						Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.STORENAME.getId()),
						GlobalData.getData("viNombreLocal"));
			}
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty()) {
				return;
			}
			Reutilizable.tomarCapturaPantalla(300, "MenuMas");
		}
	}

	public static String getResult() throws Exception {
		if (!GlobalData.getData("viMensajeErrorEditar").isEmpty()) {
			if (GlobalData.getData("viMensajeErrorEditar").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}

		return msjFinal.isEmpty() ? Mensajes.SUCCESS : msjFinal;
	}

}
