package scripts.negocio;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import scripts.Util.UtilRedeemCode;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;

public class RedeemTicket {
	private static String msjFinal = "";
	
	public static void ticketCommerce() throws Exception{
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilRedeemCode.menuTicket();
		if(!msjFinal.isEmpty()) return;
		msjFinal = UtilRedeemCode.popUpRedeemCode();
		if(!msjFinal.isEmpty()) return;
		msjFinal = UtilRedeemCode.screenRedeemTicket();
		if(!msjFinal.isEmpty()) return;
	}
	
	public static String getResult(){
		String mensajeEsperado = GlobalData.getData("viMensajeErrorCanjearTicket");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}else{
				return "El mensaje no es igual: " + msjFinal;
			}
		}
		
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}	

}
