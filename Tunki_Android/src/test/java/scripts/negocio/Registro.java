package scripts.negocio;


import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;



//import java.util.List;

//import org.openqa.selenium.By;
//import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class Registro {
	
	private static String msjFinal = "";
	private static Element optDNI = new Element("xpath", "/hierarchy/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[1]");		
	private static Element optCE = new Element("xpath", "//android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]");
	private static Element optPasaporte = new Element("xpath", "//android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[3]");														 
	private static Element root_view = new Element("id","root_view");
	private static Element imgBack = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageButton");
	
	public static void registro() throws Exception{
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId())).toString())){
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_SIGN_IN.getId())).toString())){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_SIGN_IN.getId()));
			}
		}
		
		if(GlobalData.getData("viFlujo1").equals("SI")){
			msjFinal = "";
			RegistroM1();
			if(msjFinal.equals(Mensajes.SUCCESS)) msjFinal = "";
			if(!msjFinal.isEmpty()) return;
		}
		
		if(GlobalData.getData("viFlujo2").equals("SI")){
			msjFinal = "";
			RegistroM2();
			if(msjFinal.equals(Mensajes.SUCCESS)) msjFinal = "";
			if(!msjFinal.isEmpty()) return;
		}
			
		if(GlobalData.getData("viFlujo3").equals("SI")){
			msjFinal = "";
			RegistroM3();
			if(msjFinal.equals(Mensajes.SUCCESS)) msjFinal = "";
			if(!msjFinal.isEmpty()) return;
		}
	}
	
	public static String getResult() throws Exception {
		if(!GlobalData.getData("viMensajeErrorRegistro").isEmpty()) {
			if(GlobalData.getData("viMensajeErrorRegistro").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		
		return msjFinal.isEmpty() ? Mensajes.SUCCESS:msjFinal;
	}
	
	///
	/// Primera secci�n de validaci�n - Datos Generales de Registro
	///
	public static void RegistroM1() throws Exception{
		
		//Primera pantalla
		
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CREATE.getId())).toString()));
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CREATE.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(!GlobalData.getData("viComercio").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_COMMERCE_NAME.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_COMMERCE_NAME.getId()), GlobalData.getData("viComercio"));
		}
		
		if(!GlobalData.getData("viRazonSocial").isEmpty()) {
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_BUSINESS_NAME.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_BUSINESS_NAME.getId()), GlobalData.getData("viRazonSocial"));
		}
		
		if(!GlobalData.getData("viRuc").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_RUC_NUMBER.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_RUC_NUMBER.getId()), GlobalData.getData("viRuc"));
		}
		
		EFA.executeAction(Action.Back, null);
		
		try {
			int nroComercios = Integer.parseInt(GlobalData.getData("viNroLocal"));
			
			if(nroComercios > 1){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_MORE_STORES.getId()));
			}
		}
		catch(Exception er){
			msjFinal = "El n�mero de locales ingresados no es v�lido";
			return;
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		msjFinal = Reutilizable.verificarMensajesError();
		
		if(!msjFinal.isEmpty()) {
			return;
		}
		
		//Segunda Pantalla
		
		if(!GlobalData.getData("viNombre").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_NAME.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_NAME.getId()),GlobalData.getData("viNombre"));
		}
		
		if(!GlobalData.getData("viApellido").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_LAST_NAME.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_LAST_NAME.getId()),GlobalData.getData("viApellido"));
		}
		
		if(!GlobalData.getData("viEmail").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_EMAIL.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_EMAIL.getId()),GlobalData.getData("viEmail"));
		}
		
		if(!GlobalData.getData("viCelular").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_PHONE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_PHONE.getId()),GlobalData.getData("viCelular"));
		}
		
		EFA.executeAction(Action.Back, null);
			
		if(!GlobalData.getData("viTipoDocumento").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.SPINNER_DOCUMENT.getValue()));
				
			switch (GlobalData.getData("viTipoDocumento").toUpperCase()) {
			case "DNI":
				EFA.executeAction(Action.Click, optDNI);
				break;
			case "CE":
				EFA.executeAction(Action.Click, optCE);
				break;
			case "PASAPORTE":
				EFA.executeAction(Action.Click, optPasaporte);
				break;

			}
		}
			
		if(!GlobalData.getData("viDocIdentidad").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_DOC_NUMBER.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_DOC_NUMBER.getId()),GlobalData.getData("viDocIdentidad"));
			EFA.executeAction(Action.Back, null);
		}
		
		if(GlobalData.getData("viEntrarLink").equals("SI")){
				
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_TERMS_CONDITIONS.getId()));
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Tratamiento de datos", 0);
			EFA.executeAction(Action.Click, imgBack);
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Datos de Administrador", 0);
		}
			
		if(GlobalData.getData("vilsAutorizarDatosPersonales").equals("SI")){
				
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CB_COND_TERMS_USE.getValue()));
		}
			
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			
		msjFinal = Reutilizable.verificarMensajesError();
			
		if(!msjFinal.isEmpty()) {
			return;
		}

		//Tercera Confirmaci�n
		
		if(!ingresarCodigoVerificacion()) return;
		Thread.sleep(30000);
		//Pantalla despu�s del c�digo de verificaci�n
		
		
		if(!GlobalData.getData("viUsuario").isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()), GlobalData.getData("viUsuario"));
		}
		if(!GlobalData.getData("viPassword").isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()), GlobalData.getData("viPassword"));
		}
		if(!GlobalData.getData("viRePassword").isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getId()), GlobalData.getData("viRePassword"));
		}
		
		EFA.executeAction(Action.Back, null);
		
		if(GlobalData.getData("viEntrarLink").equals("SI")){
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TERMS_CONDITIONS.getId()));
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Terminos y condiciones", 0);
			EFA.executeAction(Action.Click, imgBack);
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Datos de Usuario", 0);
		}
		
		if(GlobalData.getData("viExpirarUsuario").equals("SI")){
			int tiempoExpirar = Integer.parseInt(GlobalData.getData("viTiempoExpirar"));
			Thread.sleep(tiempoExpirar*1000);
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			
		msjFinal = Reutilizable.verificarMensajesError();
		
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Pantalla Retorno", 0);
		
		if(!msjFinal.isEmpty()) {
			return;
		}
		
		if(GlobalData.getData("viIrInicioSesion").equals("SI")){
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue())).toString())){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IV_BACK_ARROW.getId()));
				Login.loginRegistro(Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue()));
				msjFinal = Login.getResult();
				if(!msjFinal.isEmpty()) {
					return;
				}
			}
		}
		else {
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue())).toString())) {
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Pantalla", 0);
				return;
				
			}
			else {
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Pantalla Error", 0);
				return;
			}
			
		}
	}
	
	private static boolean ingresarCodigoVerificacion() throws Exception {
			
		if(!GlobalData.getData("viCodigo").isEmpty()){
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CODE_1.getId()), GlobalData.getData("viCodigo"));
		}
		else
		{
			String codigoVerificacion = obtenerCodigoVerificacion();
			if(!codigoVerificacion.isEmpty()) {
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CODE_1.getId()), codigoVerificacion);
			}
			else {
				msjFinal = "No se pudo obtener el codigo de verificacion";
				return false;
			}
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		msjFinal = Reutilizable.verificarMensajesError();
				
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Pantalla Retorno", 0);
		
		if(GlobalData.getData("viReIngreso").equals("SI")) {
			if(msjFinal.equals("El c�digo de activaci�n ha expirado.")) {
				EFA.executeAction(Action.Back, null);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
				msjFinal = "";
				String codigo = obtenerCodigoVerificacion();
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CODE_1.getId()), codigo);
			}
			else{
				return false;
			}
		}
		else {
			if(!msjFinal.isEmpty()) {
				return false;
			}
		}
		
		return true;
	}

	///
	/// Segunda secci�n de validaci�n - Datos del Local
	///
	public static void RegistroM2() throws Exception{	
			
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD_LOCAL.getId())).toString())) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD_LOCAL.getId()));
		}
		else{
			Login.loginRegistro(Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue()));
			msjFinal = Login.getResult();
			if(!msjFinal.equals(Mensajes.SUCCESS)) return;
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD_LOCAL.getId()));
		}
		
		msjFinal = "";
		
		/// agregando Nombre al local		
		
		if(!GlobalData.getData("viNombreLocal").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.STORENAME.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.STORENAME.getId()), GlobalData.getData("viNombreLocal"));
			EFA.executeAction(Action.Back, null);
		}
		
		if(GlobalData.getData("viAgregarLogo").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.GALLERY.getId()));
			if(!Reutilizable.agregarImagen()) return;
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId()));

		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));			
		msjFinal = Reutilizable.verificarMensajesError();
		
		if(!msjFinal.isEmpty()) {
			return;
		}
		
		/// agregando direcci�n
		
		if(!GlobalData.getData("viDireccionLocal").isEmpty()) {
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_STORE_ADDRESS.getId()), GlobalData.getData("viDireccionLocal"));
			if(GlobalData.getData("viDireccionLocal").equals("U")){
				EFA.executeAction(Action.Back, null);
				EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId()));			
				msjFinal = Reutilizable.verificarMensajesError();
				if(!msjFinal.isEmpty()) {
					return;
				}
			}
			Thread.sleep(600);
			EFA.executeAction(Action.Back,null);
		}
		EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));			
		msjFinal = Reutilizable.verificarMensajesError();
		
		if(!msjFinal.isEmpty()) {
			return;
		}
		
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Mapa", 0);
				
		///siguiente al mapa
		EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));			
		msjFinal = Reutilizable.verificarMensajesError();
		
		if(!msjFinal.isEmpty()) {
			return;
		}
		
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, root_view).toString())){
			
			if(GlobalData.getData("viPropina").equals("SI")){
				EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_AGREED.getId()));
			}
			else{
				EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_DECLINE.getId()));
			}
			
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));			
			msjFinal = Reutilizable.verificarMensajesError();
			
			if(!msjFinal.isEmpty()) {
				return;
			}			
		}
		
		if(GlobalData.getData("viIrInicioSesion").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IV_BACK_ARROW.getId()));
			Login.loginRegistro(Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue()));
			msjFinal = Login.getResult();
			
			if(!msjFinal.isEmpty()) {
				return;
			}
		}
		else {
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue())).toString())) {
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Pantalla", 0);	
				return;
			}
			else {
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Pantalla Error", 0);
				return;
			}			
		}
		
	}
	
	private static boolean agregarImagen() throws Exception {
		Element btnImagenes = new Element("xpath", "//android.widget.TextView[@text='Im�genes']");
		Element crop = new Element("id", "crop_image_menu_crop");
		Element foto = new Element("xpath", "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.FrameLayout[1]/android.widget.LinearLayout");
		Element img = new Element("xpath","	//android.widget.ImageButton[@content-desc='M�s opciones']");
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnImagenes).toString())){
			//EFA.executeAction(Action.Click, btnImagenes);
			EFA.executeAction(Action.Click, img);
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, foto).toString())){
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, foto).toString())){
				EFA.executeAction(Action.Click, foto);
			}
		}
		else{
			msjFinal = "No hay ninguna foto";
			return false;
		}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, crop).toString())){
			EFA.executeAction(Action.Click, crop);
		}
		else{
			msjFinal = "No se encontro el boton aceptar";
			return false;
		}
		return true;
	}

	///
	/// Tercera secci�n de validaci�n - Datos de cuenta bancaria
	///
	public static void RegistroM3() throws Exception{
				
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue())).toString())) {
			Login.loginRegistro(Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue()));
			msjFinal = Login.getResult();
			if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		}
		
		msjFinal = "";
				
		///pantalla para asociar o crear
		
		switch (GlobalData.getData("viAccionCuentaNegocio").toUpperCase()) {
		case "ABRIR":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OPEN_ACCOUNT.getId()));
			Thread.sleep(10000);
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OPEN_ACCOUNT.getId())).toString())){
				EFA.cs_getTestEvidence("Navegador", 0);
			}
			else {
				EFA.cs_getTestEvidence("Error Click Asociar", 0);
				msjFinal = "No aparecio la pagina de interbank";
			}
			return;
		case "ASOCIAR":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK_ACCOUNT.getId()));
			break;
			
		default:
			msjFinal = "No se reconoce la acci�n ingresada (" + GlobalData.getData("viAccionCuenta") + ").";
			return;
		}
		
		if(!GlobalData.getData("viCuentaBancaria").isEmpty()){
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ACCOUNT_NUMBER.getId()), GlobalData.getData("viCuentaBancaria"));
		}
		
		EFA.executeAction(Action.Back,null);
		
		switch (GlobalData.getData("viTipoCuentaBancaria").toUpperCase()) {
		case "AHORROS":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SAVINGS_ACCOUNT.getId()));
			break;

		case "CORRIENTE":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHECKING_ACCOUNT.getId()));
			break;
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_AGREE.getId()));		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));		
		
		msjFinal = Reutilizable.verificarMensajesError();
		
		if(msjFinal.equals("N�mero de cuenta no v�lido."))
		{
			if(!GlobalData.getData("viReCuenta").isEmpty())
			{
				msjFinal = "";
				EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ACCOUNT_NUMBER.getId()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ACCOUNT_NUMBER.getId()), GlobalData.getData("viReCuenta"));
				EFA.executeAction(Action.Back,null);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_AGREED.getId()));
				
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));		
				
				msjFinal = Reutilizable.verificarMensajesError();
			}
		}
		
		if(!msjFinal.isEmpty()) {
			return;
		}
					
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue())).toString())) {
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Pantalla", 0);
			
		}
		else {
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Pantalla Error", 0);
			return;
		}
		
		if(GlobalData.getData("viEmpezar").toUpperCase().equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_START.getId()));
			
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.AHORANO.getId())).toString())){
				Reutilizable.tomarCapturaPantalla(500, "PopUp Sesi�n Activa");
				if(GlobalData.getData("viIsMantenerSesion").equals("SI")) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
				}
				else {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.AHORANO.getId()));
				}
			}			
			else{
				Reutilizable.tomarCapturaPantalla(500, "Error PopUp Sesi�n Activa");
				msjFinal = "El PopUp de sesi�n activa no se muestra";
				return;
			}
			
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			
			msjFinal = Reutilizable.verificarMensajesError();
			
			if(!msjFinal.isEmpty()) {
				return;
			}
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
			
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Pantalla M�s", 0);
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SETTING.getId()));
			
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Men� Configuraci�n", 0);
			
			/*Boolean valorSesionActiva = Boolean.parseBoolean(EFA.executeAction(Action.GetAttribute, sw_session_alive, "checked").toString());
			String cValorSesion = valorSesionActiva?"SI":"NO";
			if(!GlobalData.getData("viIsMantenerSesion").toUpperCase().equals(cValorSesion)) {
				msjFinal = "El valor de la sesi�n activa no es igual a la ingresada";
				return;
			}*/
		}
			
	}

	public static String obtenerCodigoVerificacion() throws Exception {
		String codigo = "";
		String primerDato = "";

		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i < 2; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "[0-9][0-9][0-9][0-9][0-9][0-9]";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
	
}
