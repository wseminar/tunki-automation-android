package scripts.negocio;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class Cancelacion {
	private static String msjFinal = "";
	
	public static void cancelacion() throws Exception{
		msjFinal = "";		
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		String fullName = GlobalData.getData("viClienteNombre").toLowerCase() + GlobalData.getData("viClienteApellido").toLowerCase();
		
		if(!Cobro.buscarClientes(fullName,true)){
			msjFinal = "Error: No se encontr� el cliente.";
			return;
		}			
		
		if(!Cobro.EnvioCobro()){
			msjFinal = "No se realizo el cobro correctamente";
			return;
		}
			
		String snroVeces = GlobalData.getData("viVecesCancelar");
		int nroVeces = Integer.parseInt(snroVeces);
		
		while(nroVeces>0){
		
			if(!GlobalData.getData("viTiempoEspera").isEmpty()){
				int segundosEspera = Integer.parseInt(GlobalData.getData("viTiempoEspera"));
				Thread.sleep(segundosEspera * 1000);
			}
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CANCEL.getId()));			
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));			
				
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					
			/*if(GlobalData.getData("viOtroCliente").equals("SI"))
				//mismo cliente
				fullName = GlobalData.getData("viClienteNombre").toLowerCase() + GlobalData.getData("viClienteApellido").toLowerCase();	*/	
			
			if((nroVeces-1)==0){
				if(GlobalData.getData("viContinuarFlujo").equals("SI")){
					if(!Cobro.buscarClientes(fullName,true)){
						msjFinal = "Error: No se encontr� el cliente.";
						return;
					}
				
					if(!Cobro.EnvioCobro()){
						msjFinal = "No se realizo el cobro correctamente";
						return;
					}
				}
				else{
					if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE_COMMERCE.getId())).toString())){
						msjFinal = "No se muestra pantalla de inicio";
						return;
					}
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE_COMMERCE.getId()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					Element lblMovimientos = new Element("xpath", "//*[@text='Historial de movimientos']");
					if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lblMovimientos).toString())){
						Reutilizable.tomarCapturaPantalla(500, "Movimientos");
					}
					else{
						Reutilizable.tomarCapturaPantalla(500, "Error Movimientos");
						msjFinal = "Error al entrar al historial";
					}
					return;
				}
			}
			else{
				if(!Cobro.buscarClientes(fullName,true)){
					msjFinal = "Error: No se encontr� el cliente.";
					return;
				}
			
				if(!Cobro.EnvioCobro()){
					msjFinal = "No se realizo el cobro correctamente";
					return;
				}
			}
			
			
			nroVeces = nroVeces - 1;
		}				
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NAME_COMMERCE.getId())).toString()));
				
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.lblTITULO.getValue())).toString())
			&& Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId())).toString())){
			msjFinal = "Error: No sale mensaje esperado en pantalla";
			return;
		}
					
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE_COMMERCE.getId())).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE_COMMERCE.getId()));
				
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				
		if(!validarClientesHistorial(fullName)){
			msjFinal = "Error: No se visualiza el �ltimo movimiento realizado";
			return;
		}
		
		Element imgRegresar = new Element("xpath", "//*[@class='android.widget.ImageButton'][1]");
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgRegresar).toString())){
			msjFinal = "Error: No sale mensaje esperado en pantalla";
			return;
		}
		EFA.executeAction(Action.Click, imgRegresar);
	}
	
	
	public static String getResult() throws Exception {
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}
		
	public static boolean validarClientesHistorial(String cliente) throws Exception{
		List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.id("tv_fullname"));
		if(lstMovimientos.size() > 0){
			String clientePrimero = lstMovimientos.get(0).getText().trim();
			clientePrimero = clientePrimero.replace(" ", "").toLowerCase();
			if(cliente.equals(clientePrimero)){
				lstMovimientos.get(0).click();
				return true;
			}
		}		
		return false;
		
	}
}
