package scripts.negocio;

import java.util.Date;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;

public class Login {
	private static String msjFinal = "";
	
	private static Element lblFormatoInvalido = new Element("id", "textinput_error");	
	private static Element imgErrorAlert = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView");
	private static Element lblMensaje = new Element("id", "txt_message");
	
	public static void login() throws Exception{
		
		msjFinal = "";
		
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId())).toString())){
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_SIGN_IN.getId())).toString())){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_SIGN_IN.getId()));
			}
			
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RETRY.getId())).toString())){
				msjFinal = "No hay conexion a internet";
				return;
			}
		}
		EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
		EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
		EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()), GlobalData.getData("viUsuario"));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXTPASWORD.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXTPASWORD.getId()), GlobalData.getData("viPassword"));
		EFA.executeAction(Action.Back, null);
		
		if(GlobalData.getData("viMantenerSesion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CHKREMEMBER.getValue()));
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getId()));	
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty()) return;
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.AHORANO.getId())).toString()))
		{
			if(GlobalData.getData("viIsMantenerSesion").equals("SI"))
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			else
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.AHORANO.getId()));
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		Thread.sleep(500);
		
		EFA.cs_getTestEvidence("Pantalla", 0);
		
		//validarSesionBD();
		
	}
	
	public static String getResult() throws Exception{
				
		if(!GlobalData.getData("viMensajeError").isEmpty()){
			if(GlobalData.getData("viMensajeError").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
				
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
		
	}
		
	public static void loginRegistro(Element validacion) throws Exception{
		msjFinal = "";
		int tries = 0;
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId())).toString())){
			if(tries>5) {
				msjFinal = "No se abrio el login";
				EFA.cs_getTestEvidence("Error Login", 0);
				return;
			}
			Thread.sleep(2000);
			tries++;
		}
		
		EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
		EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
		EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()), GlobalData.getData("viUsuario"));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()), GlobalData.getData("viPassword"));
		EFA.executeAction(Action.Back, null);
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getId()));	
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		msjFinal = Reutilizable.verificarMensajesError();
				
		if(!msjFinal.isEmpty()) {
			return;
		}
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, validacion).toString())) {
			msjFinal = "No se visualiza la pantalla esperada";
		}
		
		EFA.cs_getTestEvidence("Pantalla", 0);
	}
	
}
