package scripts.negocio;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import java.util.Calendar;
import java.util.Date;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class Historial {
	private static String msjFinal = "";
	public static void historial() throws Exception {
		
		msjFinal = "";
		
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";
	
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE_COMMERCE.getId())).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;			
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE_COMMERCE.getId()));		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		msjFinal = Reutilizable.verificarMensajesError();
		
		if(!msjFinal.isEmpty()) return;
		
		Element lbltotaxdia = new Element("id", "tv_total_per_day");
		String totalDia = EFA.executeAction(Action.GetText, lbltotaxdia).toString();
		totalDia = totalDia.split(" S/ ")[1];
		float totalxdia = Float.parseFloat(totalDia.trim());
		//aqui
		
		if(GlobalData.getData("viValidarMovimientos").equals("SI")) {
			if(!validarMovimientos(totalxdia)) return;
		}
		
		if(GlobalData.getData("viEnviarReporte").equals("SI")){
			if (!enviarReporte()){
				return;
			}
		}
			
	}

	public static String getResult() throws Exception {
		if(!GlobalData.getData("viMensajeErrorHistorial").isEmpty()) {
			if(GlobalData.getData("viMensajeErrorHistorial").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		
		return msjFinal.isEmpty() ? Mensajes.SUCCESS:msjFinal;
	}
	
	public static boolean validarMovimientos(float montoTotal) throws Exception{
		
		Date actual = Calendar.getInstance().getTime();
		@SuppressWarnings("deprecation")
		String fActual = actual.getDate() + "/"+((actual.getMonth()+1)<10?("0"+(actual.getMonth()+1)):(actual.getMonth()+1))+"/"+(actual.getYear()+1900);
		List<WebElement> filas = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.widget.LinearLayout"));
		float montoAcumulado = 0;
		boolean Bandera = false;
		Element montoE = new Element("id", "tv_total");
		if(filas.size() > 0){
			for (int i = 0; i < filas.size(); i++) {
				String fecha = filas.get(i).findElement(By.xpath(".//android.widget.LinearLayout[1]/android.widget.TextView[2]")).getText();
				String signo = filas.get(i).findElement(By.xpath(".//android.widget.LinearLayout[2]/android.widget.TextView[1]")).getText();
				if(fecha.contains(fActual)){
					filas.get(0).click();
					Reutilizable.tomarCapturaPantalla(500, "Detalle Movimiento");
					Bandera = true;
					String monto = EFA.executeAction(Action.GetText, montoE).toString();
					if(signo.contains("+")){
						montoAcumulado += Float.parseFloat(monto.replace("S/ ", "").trim());
					}
					else {
						montoAcumulado -= Float.parseFloat(monto.replace("S/ ", "").trim());
					}
					
					EFA.executeAction(Action.Back, null);
					Reutilizable.tomarCapturaPantalla(500, "Movimientos");
					
					if(i==(filas.size()-1)){
						hacerSwipe();
						filas = EFA.cv_driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout"));
						i = devolverIndiceRegistro(fecha, filas);
						if(i==(filas.size()-1)) break;
					}
				}
				else{
					break;
				}
				
				
			}
		}
		else
		{
			msjFinal = "No se muestran movimientos";
			return false;
		}
		
		if(Bandera){
			if(GlobalData.getData("viValidarMontos").equals("SI")) {
				montoAcumulado = (float)Math.round(montoAcumulado * 100f) / 100f;
				if(montoAcumulado != montoTotal)
				{
					msjFinal = "Los montos no coinciden";
					return false;
				}
			}
		}
		else{
			msjFinal = "No hay movimientos para el d�a de hoy";
			return false;
		}
		
		return true;
	}
	
	public static void hacerSwipe() throws Exception {
		Dimension dimensions = EFA.cv_driver.manage().window().getSize();
		double dblStart,dblEnd;
		dblStart=0.9;
		dblEnd=0.3;
		Double screenHeightStart = dimensions.getHeight() *dblStart;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * dblEnd;
		int scrollEnd = screenHeightEnd.intValue();
		Double tempStart = scrollStart*dblStart;
		Double tempEnd = scrollEnd*dblEnd;
		String[] Arreglo = new String[] {"0",tempStart.intValue()+"","0",tempEnd.intValue()+"","2200"};
		//
		EFA.executeAction(Action.Swipe, Arreglo);
	}

	public static int devolverIndiceRegistro(String identificador, List<WebElement> lista){
		int indice = 0;
		for (int i = 0; i < lista.size(); i++) {
			String fecha = lista.get(i).findElement(By.xpath(".//android.widget.LinearLayout[1]/android.widget.TextView[2]")).getText();
			if(fecha.equals(identificador)) return i;
		}
		return indice;
	}

	public static boolean enviarReporte() throws Exception{

		Element btnEnviarCorreo = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ImageView");
		Element txtEmail = new Element("id", "et_email");
		Element lblErrorEnvioEmail = new Element("id", "txt_cancel");
		Element btnEnviarEmail = new Element("id", "btn_accept");
		Element btnCancelarEmail = new Element("id", "txt_cancel");
		Element imgSendAlert = new Element("xpath", "//XCUIElementTypeImage[@name='sendAlert']");
		Element imgUpsAlert = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView");
		Element lblUpsError = new Element("id", "txt_message");
		
		EFA.executeAction(Action.Click, btnEnviarCorreo);
		
		EFA.executeAction(Action.Click, txtEmail);
		EFA.executeAction(Action.SendKeys, txtEmail, GlobalData.getData("viMail"));
		
		if(GlobalData.getData("viCancelarEnvio").equals("SI")) {
			EFA.executeAction(Action.Click, btnCancelarEmail);
			
			Reutilizable.tomarCapturaPantalla(500, "Movimientos");
			
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnCancelarEmail).toString())) {
				msjFinal = "No se muestra el historial despu�s de cancelar el envio del reporte";
				return false;
			}
			else
				return true;
		}

		EFA.executeAction(Action.Click, btnEnviarEmail);	
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lblErrorEnvioEmail).toString())) {
			String mensajeErrorObj = EFA.executeAction(Action.GetText, lblErrorEnvioEmail).toString();
			msjFinal = mensajeErrorObj.toString();
			EFA.executeAction(Action.Click, btnCancelarEmail);
			Reutilizable.tomarCapturaPantalla(500, "Movimientos");
			return false;
		}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgSendAlert).toString())) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			return true;
		}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgUpsAlert).toString())) {
			msjFinal = EFA.executeAction(Action.GetAttribute, lblUpsError, "label").toString();
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			return false;
		}
		
		return false;
	}
	
}
