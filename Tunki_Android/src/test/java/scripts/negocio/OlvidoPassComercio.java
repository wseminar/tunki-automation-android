package scripts.negocio;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;

public class OlvidoPassComercio {
	private static String msjFinal = "";
	public static void PassComercio() throws Exception{
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId())).toString())){
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_SIGN_IN.getId())).toString())){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_SIGN_IN.getId()));
			}
			
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RETRY.getId())).toString())){
				msjFinal = "No hay conexion a internet";
				return;
			}
		}
		if(GlobalData.getData("viMantenerSesion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CHKREMEMBER.getValue()));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_FORGOT_PASSWORD.getId()));
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BTN_SEND.getId())).toString())){
				msjFinal = "No es la pantalla correcta";
				return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()),GlobalData.getData("viUsuario"));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()),GlobalData.getData("viEmail"));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SEND.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty()) return;
		if(GlobalData.getData("viEmail").equals("correoinvalido@gmail.com")){
			EFA.cs_getTestEvidence("Envio de correo", 800);
			return;
		}
		String codigoVerificacion = Reutilizable.obtenerCodigoVerificacionAlfa();
		System.out.println("El codigo es "+codigoVerificacion);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId())).toString())){
			msjFinal = "No es la pantalla correcta";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId()));
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId())).toString())){
			msjFinal = "No muestra la pantalla de Login";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()),GlobalData.getData("viUsuario"));
		EFA.executeAction(Action.Back, null);
		if(GlobalData.getData("viMantenerSesion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.CHKREMEMBER.getValue()));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()),codigoVerificacion);
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BTN_SIGN_IN.getId()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty()) return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD_CONFIRMATION.getId())).toString())){
			msjFinal = "No muesta pantalla Cambia tu contraseņa temporal";
			return;
		}
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTPASWORD.getId()),GlobalData.getData("viPass1"));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD_CONFIRMATION.getId()),GlobalData.getData("viPass2"));
		EFA.executeAction(Action.Back, null);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONFIRM.getId()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty()) return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId())).toString())){
			msjFinal = "No muestra la pantalla de cambio de contraseņa correctamente.";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getId()));
		msjFinal=Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty()) return;
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.AHORANO.getId())).toString())) {
			if(GlobalData.getData("viIsMantenerSesion").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
			}else {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.AHORANO.getId()));
			}
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();	
		if(!msjFinal.isEmpty()) return;	
		if(GlobalData.getData("viFlujosRecuperarPass").toUpperCase().equals("PASS TEMPORAL")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SIGNOUT.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.verificarMensajesError();	
			EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getId()), GlobalData.getData("viUsuario"));
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXTPASWORD.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXTPASWORD.getId()),codigoVerificacion);
			EFA.executeAction(Action.Back, null);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getId()));	
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty()) return;
		}
	}
	
	public static String getResult(){
		String mensajeEsperado = GlobalData.getData("viMensajeErrorPass");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}else{
				return "El mensaje no es igual: " + msjFinal;
			}
		}
		
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}	

}
