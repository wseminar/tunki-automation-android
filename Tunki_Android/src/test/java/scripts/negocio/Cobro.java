package scripts.negocio;

import java.util.List;

import org.bouncycastle.jcajce.provider.asymmetric.EC;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusableDevolucionComercio;
import scripts.helpers.Reutilizable;

public class Cobro {
	
	private static String msjFinal = "";
	
	public static void cobro() throws Exception{
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
				
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			
		String fullName = GlobalData.getData("viClienteNombre");
		
		int tries = 0;
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SEARCH.getId())).toString())){
			if(tries>5)
			{
				msjFinal = "Ningun cliente hizo checkin en el comercio";
				return;
			}
			Thread.sleep(15000);
			tries++;
		}
		
		if(!buscarClientes(fullName,true)){
			msjFinal = "Error: No se encontr� el cliente.";
			return;
		}
		
		if(GlobalData.getData("viCobros").equals("NO")){
			while(!buscarClientes(fullName,false));
			return;
		}			
		
		if(!EnvioCobro()) return;
				
		if(GlobalData.getData("viCancelarCobro").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CANCEL.getId()));			
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));			
			
			msjFinal = Reutilizable.verificarMensajesError();
			
			if(!msjFinal.equals("")) return;
			
			if(GlobalData.getData("viReCobro").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
								
				if(!buscarClientes(fullName,true)){
					msjFinal = "Error: No se encontr� el cliente.";
					return;
				}
				
				if(!EnvioCobro()) return;
				
				
			}
		}

		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGESPERA.getValue())).toString()));
		
		Element txtcobromensaje = new Element("id","tv_ups_detail");
		Element btnEntendido = new Element("id","btn_accept");
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, txtcobromensaje).toString())){
			msjFinal = EFA.executeAction(Action.GetText,txtcobromensaje).toString();
			return;
		}
		
		if(GlobalData.getData("viConfirmacionCobro").equals("NO")) return;		

		EFA.executeAction(Action.Click, btnEntendido);
			
		if(GlobalData.getData("viHistorialCobros").equals("NO")) return;
		EFA.executeAction(Action.Back, null);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE_COMMERCE.getId())).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE_COMMERCE.getId()));
				
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(!validarClientesHistorial(GlobalData.getData("viClienteNombre")+ " " + GlobalData.getData("viClienteApellido"))){
			msjFinal = "Error: No se visualiza el �ltimo movimiento realizado";
			return;
		}
		Reutilizable.tomarCapturaPantalla(300, "constancia");
		if(GlobalData.getData("viRealizarDevolucion").toUpperCase().equals("SI")){
			msjFinal = ReusableDevolucionComercio.Devolucion();
			if(!msjFinal.isEmpty())return;
		}
		
	}
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeErrorCobro").isEmpty()) {
			if(GlobalData.getData("viMensajeErrorCobro").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		
		return msjFinal.isEmpty() ? Mensajes.SUCCESS:msjFinal;
	}
	
	public static boolean EnvioCobro() throws Exception{
		Element txtMonto = new Element("id", "et_amount");
		EFA.executeAction(Action.Clear, txtMonto);
		EFA.executeAction(Action.SendKeys, txtMonto, GlobalData.getData("viMontoCobro"));
		EFA.executeAction(Action.Back, null);
		Element btnEnviarSolicitudPago = new Element("id", "btn_send_collect");
		EFA.executeAction(Action.Click, btnEnviarSolicitudPago);
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.equals("")) return false;
		return true;
	}
	
	public static boolean buscarClientes(String cliente,boolean clic) throws Exception{
		
		Element txtBuscar = new Element("id", "search_src_text");
		Element recicleView = new Element("id", "recycler_view");
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, recicleView).toString())){
			WebElement contenedor = EFA.cv_driver.findElement(By.id("recycler_view"));
			List<WebElement> filas = contenedor.findElements(By.className("android.widget.RelativeLayout"));
			
			int contador = 5;
			
			for(int i=0 ;i<contador;i++){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SEARCH.getId()));
				EFA.executeAction(Action.SendKeys, txtBuscar, cliente);
				
				for (WebElement item : filas) {
					WebElement nombre = item.findElement(By.id("txt_name"));
					String fullName = nombre.getText().trim();
					if(fullName.toUpperCase().equals(cliente.toUpperCase())){
						item.click();
						return true;
					}
				}
			}
			
		}
		else
			msjFinal = "No se visualiza ninguna persona";
		return false;
	}
	
	public static boolean validarClientesHistorial(String cliente) throws Exception{
		List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.xpath("//*[@text='"+cliente+"']"));
		if(lstMovimientos.size() > 0){
			String clientePrimero = lstMovimientos.get(0).getText().trim();
			if(cliente.toUpperCase().equals(clientePrimero.toUpperCase())){
				lstMovimientos.get(0).click();
				return true;
			}
		}		
		return false;
		
	}
	
	public static boolean validarListaClientes() throws Exception{
		Element btnCerrarBusqueda = new Element("id", "search_close_btn");
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		EFA.executeAction(Action.Click, btnCerrarBusqueda);
		int nroClientes = EFA.cv_driver.findElements(By.id("txt_name")).size();
		if(nroClientes <= 0){
			return false;
		}
		return true;
	}
}
