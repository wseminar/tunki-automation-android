package scripts.negocio;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;

import scripts.helpers.Mensajes;

public class Logout {
	private static String msjFinal = "";
	
	private static Element progress = new Element("xpath", "//*[@class='android.widget.ProgressBar']");	
	
	public static void logout() throws Exception{
		
		Element menuMas = new Element("id", "menu_more");
		EFA.executeAction(Action.Click, menuMas);
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, progress).toString()));
		
		Element item_signout = new Element("id", "item_signout");
		EFA.executeAction(Action.Click, item_signout);
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, progress).toString()));
		
		Element txtEmail = new Element("id", "et_email");
		Element chkMantenerSesion = new Element("id", "chk_remember");
		Element txtPassword = new Element("id", "et_password");
		Element btnLogin = new Element("id", "btn_sign_in");
		
		if(!(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, txtEmail).toString())
			&& Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, chkMantenerSesion).toString())
			&& Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, txtPassword).toString())
			&& Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnLogin).toString()))){
			msjFinal = "Error: No es la pantalla de logueo";
			return;
		}
	}
	
	public static String getResult() throws Exception{
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}

}
