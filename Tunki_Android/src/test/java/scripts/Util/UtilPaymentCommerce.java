package scripts.Util;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import script.textosTunki.Textos;
import scripts.helpers.ReusableRegistroPersona;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class UtilPaymentCommerce {
	
	public static String paymentCommerceTicket() throws Exception{
		String msjFinal = "";
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if(!msjFinal.isEmpty())return msjFinal;
		EFA.cs_getTestEvidence("Pantalla Pago Ticket", 800);
		Reutilizable.swipeDown(611,460,6,-245);
		msjFinal = choicePaymentMethod();
		if(!msjFinal.isEmpty())return msjFinal;
		switch (GlobalData.getData("viTipoIngresoMonto")) {
		case "TECLADO":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_QUANTITY.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_QUANTITY.getId()),GlobalData.getData("viMontoCobro"));
			EFA.executeAction(Action.Back, null);
			break;
		case "BOTONES":
			for(int i=1;i<=Integer.parseInt(GlobalData.getData("viMontoCobro"));i++){
				EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PLUS.getId()));
				Thread.sleep(800);
			}
		default:
			break;
		}
		EFA.cs_getTestEvidence("Pantalla ticket", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE.getId())).toString())){
			if(GlobalData.getData("viValidarAhora").toUpperCase().equals("PAGO")){
				msjFinal = UtilPaymentCommerce.validateTarjeta();
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId())).toString())){
					msjFinal = "";
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId()));
					EFA.cs_getTestEvidence("pantalla pago ticket", 1500);
					EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
					msjFinal = Reutilizable.verificarMensajesError();
					if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_TICKET_GLOBE.getValue())).toString())){
						msjFinal = "";
					}
					if(!msjFinal.isEmpty())return msjFinal;
					if(GlobalData.getData("viCanjeTicket").toUpperCase().equals("USAR AHORA")){
						msjFinal = getCodeTicket();
						if(!msjFinal.isEmpty())return msjFinal;
					}
					return msjFinal;
				}
			}else{
				return msjFinal;
			}
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_TICKET_GLOBE.getValue())).toString())){
			msjFinal = "";
		}
		if(!msjFinal.isEmpty())return msjFinal;
		if(GlobalData.getData("viCanjeTicket").toUpperCase().equals("USAR AHORA")){
			msjFinal = getCodeTicket();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(GlobalData.getData("viFlujoTicket").toUpperCase().equals("NO USAR")){
			
		}
		return msjFinal;
	}
	
	private static String getCodeTicket() throws Exception {
		String msjFinal = "";
		EFA.cs_getTestEvidence("Pop UP Ganaste Ticket", 800);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_TICKET_GLOBE.getValue())).toString())){
			return msjFinal = "No muestra pop up Ganaste Ticket";
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GET_CODE.getId()));
		Thread.sleep(3000);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_CODE.getValue())).toString())){
			return msjFinal = "No muestra pantalla QR generado";
		}
		EFA.cs_getTestEvidence("Generacion QR", 800);
		EFA.executeAction(Action.Back, null);
		EFA.cs_getTestEvidence("Constancia", 1000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getId()));
		return msjFinal;
	}

	public static String choicePaymentMethod() throws Exception{
		String msjFinal = "";
		String Tarjeta = "";
		Element xTarjeta = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.view.View/android.widget.Spinner/android.widget.TextView");
		Element xXTarjeta = new Element("xpath", "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout/android.widget.ScrollView/android.view.View/android.widget.Spinner/android.widget.TextView");
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,xTarjeta).toString())){
			Tarjeta = EFA.executeAction(Action.GetText,xTarjeta).toString().trim();
			System.out.println(Tarjeta);
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, xXTarjeta).toString())){
			Tarjeta = EFA.executeAction(Action.GetText,xXTarjeta).toString().trim();
			System.out.println(Tarjeta);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SPINNER_PAYMENT_METHOD.getId()));
		if(!GlobalData.getData("viElegirMetodoPago").isEmpty()){
			EFA.cs_getTestEvidence("Eleccion metodo de pago", 800);
			switch (GlobalData.getData("viElegirMetodoPago").toUpperCase()) {
			case "CREDITO":
				if(!UtilPaymentCommerce.EleccionTarjetaCredito()){
					return msjFinal = "no se realizo el cambio";
				}
				break;
			case "DEBITO":
				if(!UtilPaymentCommerce.EleccionTarjetaDebito(Tarjeta)){
					return msjFinal = "no se realizo el cambio";
				}
				break;
			default:
				break;
			}
		}else{
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.SELECT_DIALOG_LISTVIEW.getValue())).toString())){
				EFA.executeAction(Action.Back, null);
			}
		}
		EFA.cs_getTestEvidence("Pantalla Pago", 800);
		return msjFinal;
	}
	
	public static boolean buscarComercio(String comercio) throws Exception{
		EFA.cs_getTestEvidence("Listado Comercio", 800);
		Element btnLupa = new Element("id", "search_view");
		Element txtBuscar = new Element("id", "search_view");
				
		EFA.executeAction(Action.Click, btnLupa);
		EFA.executeAction(Action.SendKeys, txtBuscar, comercio);
		Thread.sleep(3000);
		EFA.cf_getTestEvidenceWithStep("Comercio Buscado", 800);
		List<WebElement> lista = EFA.cv_driver.findElements(By.id("txt_name"));
		for (WebElement item : lista) {
			if(item.getText().equals(comercio)){
				if(GlobalData.getData("viDetalleComercio").equals("SI")){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.MOREINFO.getValue()));
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PAGARAQUI.getId()));
					Thread.sleep(10000);
					return true;
				}
				item.click();
				Thread.sleep(5000);
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD.getId())).toString())){
					if(GlobalData.getData("viPagoSinTarjeta").toUpperCase().equals("SI")){
						return true;
					}
					return false;
				}
				return true;
			}
		}
		return false;
	}

	public static boolean EleccionTarjetaCredito() {
		List<WebElement> listaElementoxClases = EFA.cv_driver.findElementsByClassName("android.widget.TextView");
		for (WebElement webElement : listaElementoxClases) {
			String tempTexto = webElement.getText();
			System.out.println(tempTexto);
			if(tempTexto.contains("*")) {
				webElement.click();
				return true;
			}
		}
		return false;
	}
	
	public static boolean EleccionTarjetaDebito(String tarjeta) throws Exception {
		System.out.println(tarjeta);
		List<WebElement> listaElementoxClases = EFA.cv_driver.findElementsByClassName("android.widget.TextView");
		for (WebElement webElement : listaElementoxClases) {
			String tempTexto = webElement.getText();
			System.out.println(tempTexto);
			if(!tempTexto.equals("Elige una cuenta")){
				if(!tempTexto.equals(tarjeta)){
					webElement.click();
					return true;
				}
			}
		}
		return false;
	}
	
	public static String payNoCard() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD.getId()));
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXTCARDNUMBER.getId())).toString())){
			msjFinal = "No muestra pantalla Vincula tu tarjeta de debito";
		}
		msjFinal = ReusableRegistroPersona.AgregarTarjeta();
		if(!msjFinal.isEmpty())return msjFinal;
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId())).toString())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE_LATER.getId())).toString())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE_LATER.getId()));
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId())).toString())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId()));
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NAME.getId())).toString())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NAME.getId()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.verificarMensajesError();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PAY.getId())).toString())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PAY.getId()));
		}
		return msjFinal;
	}
	
	public static String menuTicket() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getId()));
		EFA.cf_getTestEvidenceWithStep("Menu mas", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_TICKET.getId()));
		EFA.cs_getTestEvidence("Ticket", 800);
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_EMPTY.getValue())).toString())){
			if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_EMPTY.getValue())).toString().toUpperCase().equals(Textos.NO_TIENES_TICKECTS.toUpperCase())){
				return msjFinal = "Texto: "+Textos.NO_TIENES_TICKECTS+" diferente";
			}
		}
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GET_CODE.getId())).toString())){
			msjFinal = "";
		}
		if (!msjFinal.isEmpty())return msjFinal;
		EFA.cs_getTestEvidence("Menu Ticket", 1000);
		EFA.executeAction(Action.Back, null);
		Thread.sleep(800);
		EFA.cs_getTestEvidence("Menu Mas", 800);
		return msjFinal;
	}
	
	public static String menuMove() throws Exception{
		String msjFinal= "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVE.getId()));
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		EFA.cs_getTestEvidence("Menu mas", 1000);
		return msjFinal;
	}
	
	public static String validateTarjeta() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDATE.getId()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.STAR_BUTTON.getId()));
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		String codigoValidacion = ValidacionBD.obtenerCodigoValidacionTC(GlobalData.getData("viEmail").toUpperCase());
		System.out.println(codigoValidacion);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.EDIT_TEXT.getId()));
		if(!GlobalData.getData("viCodigoTC").isEmpty()){
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EDIT_TEXT.getId()),GlobalData.getData("viCodigoTC"));
			if(GlobalData.getData("viCodigoTC").length()<6){
				EFA.executeAction(Action.Back, null);
			}
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VALIDATE_BUTTON.getId()));
			if(GlobalData.getData("viBloquearUsuario").equals("VALIDACION TD")){
				for(int i=0;i<2;i++){
					while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GOT_IT.getId()));
					Thread.sleep(3000);
					EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EDIT_TEXT.getId()),GlobalData.getData("viCodigoTC"));
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VALIDATE_BUTTON.getId()));
				}
				msjFinal = UtilLoginCustomer.loginPersona();
			  	String personId = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
			  	System.out.println(personId);
			  	ValidacionBD.desbloquearUsuario(personId);
			  	EFA.cs_getTestEvidence("cuenta bloqueada", 1400);
			  	if(!msjFinal.isEmpty())return msjFinal;
			}
			msjFinal = Reutilizable.verificarMensajesError();
			if (!msjFinal.isEmpty())return msjFinal;
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.FINISH_BUTTON.getId())).toString())){
				return msjFinal = "no muestra pantalla tarjeta validada";
			}
		}
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.EDIT_TEXT.getId()),codigoValidacion);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VALIDATE_BUTTON.getId()));
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String[] ArrayListofCommerce(){
		String[] xdistancia = new String[6] ;
		List<WebElement> listaElemtoxId = EFA.cv_driver.findElementsById("txt_distance");
		for(int i= 0;i<xdistancia.length;i++){
			for(WebElement webElement : listaElemtoxId){
				String distancia = webElement.getText();
					xdistancia[i] = distancia;
			}
		}
		return xdistancia;
	}
}
