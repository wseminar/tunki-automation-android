package scripts.Util;

import com.everis.GlobalData;

import scripts.helpers.ValidacionBD;

public class UtilBD {
	
	public static void borrarTarjeta() throws Exception{
		String dato = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
		System.out.println(dato);
		if(!dato.equals("DataNotFound")){
			ValidacionBD.eliminarTarjetaTblPMethod(dato);
			ValidacionBD.eliminarTarjetaTblPUser(dato);
			System.out.println("se borro tarjeta");
		}
	}
	
	public static void quitarValidacionTD() throws Exception{
		String personId = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
		System.out.println(personId);
		String paymentMethodId = ValidacionBD.obtenerPaymentMethodId(personId);
		System.out.println(paymentMethodId);
		String status = ValidacionBD.obtenerStatusTblPca(paymentMethodId);
		System.out.println(status);
		if(paymentMethodId.equals("DataNotFound") || !status.equals("0")){
			ValidacionBD.cambiarStatusTblPcaTD(paymentMethodId);
		}
	}
	
	
}
