package scripts.Util;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import script.textosTunki.Textos;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class UtilRedeemCode {
	
	public static String textPopUpRedeemCode() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_TITLE.getValue())).toString().toUpperCase().equals(Textos.INGRESA_EL_CODIGO_TICKET.toUpperCase())){
			msjFinal = "Texto: "+Textos.INGRESA_EL_CODIGO_TICKET+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue())).toString().toUpperCase().equals(Textos.DETALLO_INGRESA_EL_CODIGO_TICKET.toUpperCase())){
			msjFinal = "Texto: "+Textos.DETALLO_INGRESA_EL_CODIGO_TICKET+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VERIFY.getId())).toString().toUpperCase().equals(Textos.VERIFICAR.toUpperCase())){
			msjFinal = "Texto: "+Textos.VERIFICAR+" diferente";
		}
		return msjFinal;
	}
	
	public static String menuTicket() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MORE.getId()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_TICKETS.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String popUpRedeemCode() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IV_QR.getId()));
		msjFinal = textPopUpRedeemCode();
		if(!msjFinal.isEmpty()) return msjFinal;
		String codigoTicket = codeTicket();
		if(!GlobalData.getData("viCodigoTicket").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_QR_CODE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_QR_CODE.getId()),GlobalData.getData("viCodigoTicket"));
		}else{
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_QR_CODE.getId()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_QR_CODE.getId()),codigoTicket);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VERIFY.getId()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String screenRedeemTicket(){
		String msjFinal = "";
		EFA.cs_getTestEvidence("Pantalla Canje de Ticket", 1000);
		return msjFinal;
	}
	
	public static String codeTicket() throws Exception{
		String msjFinal = "";		
		String userId = ValidacionBD.obtenerUserIdComercio(GlobalData.getData("viUsuario"));
		System.out.println(userId);
		if(userId.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		String storeId = ValidacionBD.obtenerStoreIdComercio(userId);
		System.out.println(storeId);
		if(storeId.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		String commerceId = ValidacionBD.obtenerCommerceIdComercio(storeId);
		System.out.println(commerceId);
		if(commerceId.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		String productId = ValidacionBD.obtenerProductIdComercio(commerceId);
		System.out.println(productId);
		if(productId.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		String codigoTicket = ValidacionBD.obtenerCodigoTicket(productId,GlobalData.getData("viEmail"));
		System.out.println(codigoTicket);
		if(codigoTicket.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		msjFinal = codigoTicket;
		return msjFinal;
	}
	
}
