package scripts.Util;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;


import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class UtilLoginCustomer {

	public static String loadingScreenLogin() throws Exception {
		String msjFinal = "";
		while (!Boolean
				.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_UP.getId()))
						.toString())) {
			if (Boolean
					.parseBoolean(EFA
							.executeAction(Action.IsElementPresent,
									Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RETRY.getId()))
							.toString())) {
				return msjFinal = "No hay conexion a internet";
			}
		}
		return msjFinal;
	}

	public static String enterYourUserName() throws Exception {
		String msjFinal = "";
		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TXT_CHANGE_ACCOUNT.getId()));
		Thread.sleep(1000);
		if (!GlobalData.getData("viEmail").isEmpty()) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()));
			EFA.executeAction(Action.SendKeys,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getId()),
					GlobalData.getData("viEmail"));
			EFA.executeAction(Action.Back, null);
		}

		EFA.executeAction(Action.Click,
				Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_AGREED.getId()));
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())
			return msjFinal;
		return msjFinal;
	}

	public static String loginPersona() throws Exception {
		String msjFinal = "";
		int n = 0;
		if (GlobalData.getData("viFlujosLoginPersona").toUpperCase().equals("LOGIN DOS VECES")) {
			for (int i = 0; i < 2; i++) {
				n = 0;
				for (int j = 0; j < GlobalData.getData("viPassword_2").length(); j++) {
					msjFinal = captureTextLogin(GlobalData.getData("viPassword_2").substring(n, n + 1));
					if (!msjFinal.isEmpty())
						return msjFinal;
					EFA.cs_getTestEvidence("Login", 500);
					n++;
				}
				while (!Boolean.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BUTTON_0.getId()))
						.toString()))
					;
			}
		}
		Thread.sleep(1000);
		n = 0;
		for (int i = 0; i < GlobalData.getData("viPassword").length(); i++) {
			msjFinal = captureTextLogin(GlobalData.getData("viPassword").substring(n, n + 1));
			if (!msjFinal.isEmpty())
				return msjFinal;
			EFA.cs_getTestEvidence("Login", 500);
			n++;
		}
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = Reutilizable.verificarMensajesError();
		if (!msjFinal.isEmpty())
			return msjFinal;
		if (!Boolean
				.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.CONTACT.getId()))
						.toString())) {
			return msjFinal = "No muestra listado de comercio";
		}
		return msjFinal;
	}

	public static String captureTextLogin(String texto) {
		String msjFinal = "";
		List<WebElement> listaTecladoNumerico = EFA.cv_driver.findElementsByClassName("android.widget.Button");
		for (WebElement webElement : listaTecladoNumerico) {
			String Teclanumero = webElement.getText();
			if (Teclanumero.equals(texto)) {
				webElement.click();
				return msjFinal;
			}
		}
		return msjFinal = "Error captura texto";

	}
}
